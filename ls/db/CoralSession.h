/*
 *  CoralSession.h
 *  ls package
 *
 *  Created by Raul Murillo-Garcia on 8.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#ifndef LS_CORALSESSION_H_
#define LS_CORALSESSION_H_

#include <string>
#include <memory>                          // unique_ptr
#include <RelationalAccess/AccessMode.h>

#include "ls/Exceptions.h"


namespace coral {
  class ISessionProxy;
  class ConnectionService;
}

namespace daq {
   namespace ls {
      namespace CoralItems {
         
         /**
          * \class Session "ls/db/Session.h"
          *
          * \brief Wraps around a CORAL session, configuring it as appropriate and 
          *        loading all the required services, including, the XML authentitication 
          *        and the connection. 
          */                        
         class Session {
            typedef coral::AccessMode SessionMode;

            public:
               /**
                 * \brief Constructor; does most of the job.
                 * 
                 * \throw ls::SessionCreate Thrown if a CORAL exception occurs or
                 *                          dynamic memory could not be allocated.                          
                 */ 
               Session(const std::string& connectionString, 
                       SessionMode mode = coral::Update);

               /**
                 * \brief Destructor; frees the dynamically allocated memory.
                 */
               ~Session();

               /**
                 * \brief Returns the CORAL session proxy.
                 * NOTE: this should be fixed since releasing the ownership of
                 *       the pointer like this might lead to issues.
                 * 
                 * \return The session proxy.  
                 */                       
               coral::ISessionProxy* getProxy();

            protected:

            private:
               Session(const Session& session);
               Session& operator =(const Session& session); 
                                 
               std::unique_ptr<coral::ISessionProxy>     m_proxy;             ///< CORAL proxy session.
               std::unique_ptr<coral::ConnectionService> m_connectionService; /// < CORAL connection service
         };          
      } // CoralItems
   } // ls
} //daq       


   
                     

#endif //LS_CORALSESSION_H_
    

