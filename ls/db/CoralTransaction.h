/*
 *  CoralTransaction.h
 *  ls package
 *
 *  Created by Raul Murillo-Garcia on 17.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#ifndef LS_CORALTRANSACTION_H_
#define LS_CORALTRANSACTION_H_

#include <sstream>
#include <string>

#include "ls/Exceptions.h"


namespace daq {
   namespace ls {
      namespace CoralItems {
         // Forward declarations
         class Session;
                     
         /**
          * \class Transaction "ls/db/Transaction.h"
          *
          * \brief This class wraps around a CORAL transaction. Offers methods to 
          *        start, commit, etc. Any action on a table (create, insert,
          *        delete, etc.) need to be done within an active Transaction.
          */                             
         class Transaction {
            public:
               /**
                * \brief The contructor: keeps a Sesssion reference.
                * 
                * \param session Session to have the transaction in.
                */                  
               Transaction(Session &session);

               /**
                * \brief The destructor: commits a transaction in case it is 
                *        still active. This gurantees that all transaction are 
                *        correctly closed.
                */               
               ~Transaction();

               /**
                * \brief Starts a transaction.
                * 
                * \param readOnly If \c TRUE, only read operations are allowd. If
                *                 \c FALSE (default), read/write operations are allowd.
                *    
                * \throw ls::TransactionFailed Thrown if a CORAL exception occurs.
                */
               void Start(bool readOnly = false);

               /**
                * \brief Commits the transaction.
                *    
                * \throw ls::TransactionFailed Thrown if a CORAL exception occurs.
                */                   
               void Commit();

               /**
                * \brief Checks if the transaction is still active, that is strarted
                *        and not yet commited.
                * 
                * \return \c TRUE if active, \c FALSE otherwise.   
                *    
                * \throw ls::TransactionFailed Thrown if a CORAL exception occurs.
                */
               bool Active();
                
            protected:
                
            private:                                     
               Transaction(const Transaction& Transaction);
               Transaction& operator =(const Transaction& Transaction);

               Session& m_session; ///< Session to use this transactino in.                             
         };
      } // CoralItems
   } // ls
} //daq       

#endif //LS_CORALTRANSACTION_H_
