/*
 *  CoralTable.h
 *  ls package
 *
 *  Created by Raul Murillo-Garcia on 8.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#ifndef LS_CORALTABLE_H_
#define LS_CORALTABLE_H_

#include <string>
#include <iostream>

#include <CoralBase/AttributeList.h>


namespace daq {
   namespace ls {
      namespace CoralItems {
         // Forward declarations
         class Session;

         /**
          * \class Table "ls/db/Table.h"
          *
          * \brief This is the base class to handle Log Service tables. It
          *        wraps around a CORAL table. Offers methods to do most
          *        general purpouse operations, such as doing a bulk INSERT,
          *        SELECT or DELETE with bind variables, dropping a table,
          *        getting its size (number of rows) and checking if it exists.
          *        More specialized functions such as creating the table or
          *        inserting a single row need to be implemented in the derivied
          *        class. This class works around a Session, which needs to be
          *        passed as an argument in the Constructor.
          */
         class Table {
            public:
               /**
                * \struct OneRow "ls/db/Table.h"
                *
                * \brief Dummy message.
                */
               struct OneRow {
                  OneRow() {;};
                  virtual ~OneRow() {;};
               };

               /**
                * \brief The contructor: keeps a Sesssion reference.
                *
                * \param session Session to use the table in.
                * \param name Table name.
                */
               explicit Table(Session &session,  const std::string& name = "");

               /**
                * \brief The destructor: empty.
                */
               virtual ~Table();

               /**
                * \brief Pure virtual method. Creates the table in the database.
                * \throw ls::TableFailed
                */
               virtual void create() = 0;

               /**
                * \brief Pure virtual method. Inserts a single entry in the table.
                *
                * \param oneRow Entry to insert in the table. The derivied class
                *               has to also derive the OneRow structure with the
                *               specialized entry.
                * \throw ls::TableFailed Thrown if a CORAL exception occurs.
                */
               virtual void insertRow(OneRow& oneRow) = 0;

               /**
                * \brief Removes entries that match the bind variables and condition
                *        passed as arguments.
                *
                * \param conditionData Set of parameters that are evaluated in the
                *                      DELETE query.
                * \param condition Condition logic use to evaluate in the DELETE query.
                *
                * \return Number of entires removed from the database.
                *
                * \throw ls::TableFailed Thrown if a CORAL exception occurs.
                */
               virtual unsigned int removeRow(const coral::AttributeList& conditionData,
                                              const std::string& condition);

               /**
                * \brief Retrieves all the entries in a table.
                *
                * \param stream Output stream to dump the entries to. std::cout by default.
                *
                * \return Number of entires retrieved from the database.
                *
                * \throw ls::TableFailed Thrown if a CORAL exception occurs.
                */
               virtual unsigned int select(std::ostream& stream = std::cout);

               /**
                * \brief Retrieves entries that match the bind variables and condition
                *        passed as arguments.
                *
                * \param conditionData Set of parameters that are evaluated in the
                *                      SELECT query.
                * \param condition Condition logic use to evaluate in the SELECT query.
                * \param orderList Logic for the ORDER BY part of the SQL query.
                * \param groupBy Logic for the GROUP BY part of the SQL query.
                * \param maxRows Logic for the LIMIT part of the SQL query; number
                *                of entries to retrieve. By default 1000. If 0, all
                *                the entries are dumped.
                * \param offset Logic for the LIMIT part of the SQL query; offset to
                *               retrieves the entries from. By default 0.
                * \param stream Output stream to dump the entries to. std::cout by default.
                *
                * \return Number of entires retrieved from the database.
                *
                * \throw ls::TableFailed Thrown if a CORAL exception occurs.
                */
               virtual unsigned int select(const coral::AttributeList& conditionData,
                                           const std::string& condition,
                                           const std::string& colList,
                                           const std::string& orderList,
                                           const std::string& groupBy,
                                           unsigned int maxRows = 1000,
                                           unsigned int offset = 0,
                                           std::ostream& stream = std::cout);

               /**
                * \brief Checks if the table exists on the database.
                *
                * \return \c TRUE if it exists, \c FALSE otherwise.
                *
                * \throw ls::TableFailed Thrown if a CORAL exception occurs.
                */
               virtual bool exists();

               /**
                * \brief Removes the table from the database.
                *
                * \throw ls::TableFailed Thrown if a CORAL exception occurs.
                */
               virtual void drop();

               /**
                * \brief Removes all the entries in the table;
                *
                * \throw ls::TableFailed Thrown if a CORAL exception occurs.
                */
               virtual void clean();

               /**
                * \brief Returns the number of entires in the database.
                *
                * \return Number of entires in the database.
                *
                * \throw ls::TableFailed Thrown if a CORAL exception occurs or
                *                        if nothing is returned from the DB.
                */
               virtual double size();

               /**
                * \brief Returns the number of entires that match a certain criteria.
                *
                * \param conditionData Set of parameters that are evaluated in the
                *                      SELECT query.
                * \param condition Condition logic use to evaluate in the SELECT query.
                *
                * \return Number of entires in the database.
                *
                * \throw ls::TableFailed Thrown if a CORAL exception occurs or
                *                        if nothing is returned from the DB.
                */
               virtual double size(const coral::AttributeList& conditionData,
                                   const std::string& condition);

            protected:
               Session& getSession() const { return m_session; }
               std::string getName() const { return m_name;    }

               /**
                * \brief Prints the SQL query based. Need to be implemetned properly!!!!!!
                *
                * \param action SQL action, SELECT, DELETE, etc.
                * \param conditionData Set of parameters that make up the SQL query.
                * \param condition Condition logic used in the SQL query.
                */
               void printSQL(const std::string& action,
                             const coral::AttributeList& conditionData,
                             const std::string& condition) const;

            private:
               Table(const Table& table);
               Table& operator =(const Table& table);

               Session& m_session; ///< Session to use this table with.
               std::string m_name; ///< Name of the table;
         };
      } // CoralItems
   } // ls
} //daq

#endif //LS_CORALTABLE_H_
