/*
 *  incldue.h
 *  ls package
 *
 *  Created by Raul Murillo-Garcia on 8.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#ifndef LS_INCLUDE_H_
#define LS_INCLUDE_H_

#include "ls/db/CoralSession.h"
#include "ls/db/CoralTable.h"
#include "ls/db/CoralTransaction.h"    

#endif //LS_INCLUDE_H_
