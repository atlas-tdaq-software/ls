/*
 *  Exceptions.h
 *  ls package
 *
 *  Created by Raul Murillo-Garcia on 12.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#ifndef LS_EXCEPTIONS_H_
#define LS_EXCEPTIONS_H_


#include <ers/ers.h>


/**
 * \namespace daq
 *
 * \brief This is a wrapping namespace for all the Log Service exceptions.
 */

namespace daq {

  /**
   * \class ls::Exception
   *
   * \brief  This is a base class for all Log Servicer exceptions.
   */
   ERS_DECLARE_ISSUE(ls,
                     Exception,
                     ERS_EMPTY,
                     ERS_EMPTY
                    )


  /*************************/
  /**   DATABASE ISSUES   **/
  /*************************/

     /**
      * \class ls::TestLS
      *
      * \brief Exception to test functionality of LS.
      */
     ERS_DECLARE_ISSUE_BASE(ls,
			    TestLS,
			    ls::Exception,
			    txt,
			    ERS_EMPTY,
			    ((const char *)txt)
                        )


  /**
   * \class ls::SessionCreate
   *
   * \brief Exception raised when the Log Receiver constructor fails.
   */
  ERS_DECLARE_ISSUE_BASE(ls,
                         SessionCreate,
                         ls::Exception,
                         "CORAL session could not be created: " << reason << ".",
                         ERS_EMPTY,
                         ((const char *)reason)
                        )

  /**
   * \class ls::TableFailed
   *
   * \brief Exception raised if an error occurrs when doing an action on a table.the Log Receiver constructor fails.
   */
  ERS_DECLARE_ISSUE_BASE(ls,
                         TableFailed,
                         ls::Exception,
                         "Action '" << action << "' failed on table '" << table << "'. Reason: " << reason,
                         ERS_EMPTY,
                         ((const char *)action)
                         ((const char *)table)
                         ((const char *)reason)
                        )


  /**
   * \class ls::TransactionFailed
   *
   * \brief Exception raised if an error occurrs during a transaction.
   */
  ERS_DECLARE_ISSUE_BASE(ls,
                         TransactionFailed,
                         ls::Exception,
                         "Transaction failed on action '" << action << "'. Reason: " << reason,
                         ERS_EMPTY,
                         ((const char *)action)
                         ((const char *)reason)
                        )

  /**
   * \class ls::TableDrop
   *
   * \brief Exception raised when a database table removal fails.
   */
  ERS_DECLARE_ISSUE_BASE(ls,
                         TableDrop,
                         ls::Exception,
                         "Database table cound not be removed: " << reason << ".",
                         ERS_EMPTY,
                         ((const char *)reason)
                        )


  /***************************/
  /**   LOG BUFFER ISSUES   **/
  /***************************/
  /**
   * \class ls::LogBufferFailed
   *
   * \brief Exception raised if an error occurs when accessing the buffer:
   *        buffer empty, buffer full, etc.
   */
  ERS_DECLARE_ISSUE_BASE(ls,
                         LogBufferFailed,
                         ls::Exception,
                         "Log buffer error: " << reason << ".",
                         ERS_EMPTY,
                         ((const char *)reason)
                        )


  /****************************/
  /**   LOGGER ISSUES   **/
  /****************************/

  /**
   * \class ls::LogReceiverConstructor
   *
   * \brief Exception raised if the Log Receiver constructor failes.
   */
  ERS_DECLARE_ISSUE_BASE(ls,
                         LoggerFailed,
                         ls::Exception,
                         "Logger failed: " << reason << ".",
                         ERS_EMPTY,
                         ((const char *)reason)
                        )

  /*****************************/
  /**   LOG RECIEVER ISSUES   **/
  /*****************************/

  /**
   * \class ls::ParseFailed
   *
   * \brief Exception raised when the command arguments failed.
   */
  ERS_DECLARE_ISSUE_BASE(ls,
                         ParseFailed,
                         ls::Exception,
                         "Parsing of commandline options failed",
                         ERS_EMPTY,
                         ((const char *) dummy)
                        )

  /************************/
  /**   TESTING ISSUES   **/
  /************************/

  /**
   * \class ls::TestUnitFailed
   *
   * \brief Exception raised when a test unit failed.
   */
  ERS_DECLARE_ISSUE_BASE(ls,
                         TestUnitFailed,
                         ls::Exception,
                         "Exception raised when running the test unit '" << testUnit << "'. Reason: " << reason,
                         ERS_EMPTY,
                         ((const char *) testUnit)
                         ((const char *) reason)
                       )

}

#endif //LS_EXCEPTIONS_H_

