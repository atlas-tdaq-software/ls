//----------------------------------------------------------------------------------------
// Title         : Trigger condition
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogTriggerCondition.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 20/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogTriggerCondition
// Description   : This class wraps around a boost::condition and is used to signal
//                 when the buffered has reached the logging threahold (500 by default)
//                 and it is time for the msgs to be copied onto the database..
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 20/Jun/2007: created.
//----------------------------------------------------------------------------------------

#ifndef LOG_TRIGGERCONDITION_H_
#define LOG_TRIGGERCONDITION_H_

#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>


namespace daq {
   namespace ls {
      /**
       * \class LogTriggerCondition "ls/lib/LogTriggerCondition .h"
       * \brief This class wraps around a boost::condition and is used to signal
       *        when the buffered has reached the logging threahold (500 by default)
       *        and it is time for the msgs to be copied onto the database.
       */
      class LogTriggerCondition: private boost::noncopyable
      {
         public:
            LogTriggerCondition() { ; }
            virtual ~LogTriggerCondition() { ; }

            /* \brief Blocks the thread until the condition is fulfilled. */
            void wait();

            /* \brief Releases the waiting status of the conditin. Threads that
             *        were blocked anre released.
             */
            void release();

         private:
            boost::condition m_logTrigger; // Condition variable triggered when a certain number of msgs are in the buffer.
            boost::mutex   m_logTrigMutex; // Mutex for the condition variable.
        };

   } // ls
} // daq

#endif //LOG_TRIGGERCONDITION_H_
