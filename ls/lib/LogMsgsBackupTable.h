//----------------------------------------------------------------------------------------
// Title         : Log message backup table
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogMsgsBackupTable.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 2/Apr/2012
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogMsgsBackupTable
// Description   : Provides access to the Log Message backup table.
//----------------------------------------------------------------------------------------
// Copyright (c) 2012 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
//  2/Apr/2012: created.
//----------------------------------------------------------------------------------------

#ifndef _LS_LOG_MSGS_BACKUP_TABLE_H
#define _LS_LOG_MSGS_BACKUP_TABLE_H

#include <boost/noncopyable.hpp>

#include <ers/Severity.h> 

#include "ls/db/include.h"


// Forward declarations
class AttributeList;

namespace daq {
   namespace ls {
      // Forward declarations
      class LogBuffer;

      class LogMsgsBackupTable: public CoralItems::Table, private boost::noncopyable
      {
         public:
            /* \brief The contructor: fills in the session and name data members.
             * \param session Session to use the table in.
             * \param user User name. If an empty string is passed, this
             *                  parameter is not taken into account.
             * \param session Session ID. If an empty string is passed, this
             *                  parameter is not taken into account.
             */
            LogMsgsBackupTable(CoralItems::Session &session,
                               std::string const& userName,
                               long session_id);

             /**
             * \brief Always throws an exception since this table cannot be crated via CORAL.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            void create();

            /* \brief Inserts a single entry in the Log Msgs table.
             * \param oneRow Entry to insert in the Log Msgs table. An object
             *               of type LogMessage should be passed instead.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            void insertRow(CoralItems::Table::OneRow& onemsg);

            /* \brief Bulk insert. This method should be the preferable to insert logs
             *        in a table. It gets all the messages from the LogBuffer and does a
             *        a bulk insert. This is the most efficient way to write onto the database.
             * \param logBuffer Log buffer with the messages received via the ERS interface.
             * \return Number of entires inserted in the database.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            unsigned int insertRowBulk(LogBuffer& logBuffer);

            /* \brief Bulk select.
             * \return Number of entires selected.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            unsigned int selectForRunNumber(unsigned int runNumber,
                                            unsigned int maxRows,
                                            unsigned int offset,
                                            LogBuffer& logBuffer);

            /* \brief Gets the parameters for a Log Msgs table and constructs the
             *        bind variables and condition string. That is, it constructs a
             *        criteria to work with the table.
             * \param machine Machine name. If an empty string is passed, this parameter
             *              is not taken into account.
             * \param app Application name. If an empty string is passed, this parameter
             *             is not taken into account.
             * \param whenLow Lower threshold time in UTC format. If -1, is passed, this
             *                parameter is not taken into account.
             * \param whenUp Upper threshold time in UTC format. If -1, is passed, this
             *                parameter is not taken into account.
             * \param severity Severity level as an integer. If -1, is passed, this
             *                parameter is not taken into account.
             * \param msgID Message name. If an empty string is passed, this
             *                  parameter is not taken into account.
             * \param msgTxt Message body. If an empty string is passed, this parameter
             *            is not taken into account.
             * \param msgParam Message parameters. If an empty string is passed, this
             *              parameter is not taken into account.
             * \param msgOptParam Message optional parameters. If an empty string is passed, this
             *              parameter is not taken into account.
             * \param msgQual Message qualifiers. If an empty string is passed, this
             *              parameter is not taken into account.
             * \param conditionData Reference to an Attribute list object where the
             *                      table parameters are appended.
             * \param condition Reference to a string where the query condition is
             *                  built in.
             */
            void parseCondition(long runnumber, const std::string& machine, const std::string& app,
                                long whenLow, long whenUp, short severity, const std::string& msgID,
                                const std::string& msgTxt, const std::string& msgParam,
                                const std::string& msgOptParam, const std::string& msgQual,
                                coral::AttributeList& conditionData, std::string& condition);

         private:
            std::string m_userName;
            long        m_session;
            coral::AttributeList m_tableSchema;
      };
   } // ls
} //daq

#endif //_LS_LOG_MSGS_BACKUP_TABLE_H
