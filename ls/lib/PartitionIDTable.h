//----------------------------------------------------------------------------------------
// Title         : Partition ID table.
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : PartitionIDTable.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 20/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : PartitionIDTable
// Description   : Class to access the table mapping Partition Names with its associatied
//                 Partition ID.
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 31/Oct/2007: created.
//----------------------------------------------------------------------------------------


#ifndef LS_PARTITIONIDTABLE_H
#define LS_PARTITIONIDTABLE_H

#include <string>
#include <ostream>
#include <boost/noncopyable.hpp>

#include "ls/db/include.h"


// Forward declarations
class AttributeList;

namespace daq {
   namespace ls {

      /**
       * \class PartitionIDTable "ls/lib/PartitionIDTable.h"
       * \brief Class mapping Partition Names with its associatied Partition ID.
       */
      class PartitionIDTable : public CoralItems::Table, private boost::noncopyable
      {
         public:
            /**
             * \struct PartitionIDRow "ls/lib/PartitionIDTable.h"
             * \brief Structure that encapsulates an entry in the PartitionID table;
             */
            struct PartitionIDRow : public CoralItems::Table::OneRow
            {
               PartitionIDRow() {;};
               PartitionIDRow(long long partitionID,
                              const std::string& tdaqRelease,
                              const std::string& partitionName)
                            : m_tableID(partitionID),
                              m_tdaqRelease(tdaqRelease),
                              m_partitionName(partitionName) {;};

              ~PartitionIDRow() {;};

               long long     m_tableID;
               std::string   m_tdaqRelease;
               std::string   m_partitionName;
            };

            /**
             * \brief The contructor: fills in the name and ID.
             * \param session Session to use the table in.
             */
            PartitionIDTable(CoralItems::Session  &session);

            /* \brief The destructor: does nothing. */
            ~PartitionIDTable() { ; }

            /**
             * \brief Creates the Metadata table.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            void create();

            /**
             * \brief Inserts a single entry in the Metadata table.
             * \param oneRow Entry to insert in the Metadata table. An object
             *               of type MetadataRow should be passed instead.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            void insertRow(CoralItems::Table::OneRow& onemsg);

            /**
             * \brief Gets the parameters for a Metadata table and constructs the
             *        bind variables and condition string. That is, it constructs a
             *        criteria to work with the table.
             * \param partitionID ID associated to a given partition.
             * \param tdaqRelease   TDAQ release. If an empty string is passed, this
             *                      parameter is not taken into account.
             * \param partitionName Partition name. If an empty string is passed, this
             *                      parameter is not taken into account.
             * \param conditionData Reference to an Attribute list object where the
             *                      table parameters are appended.
             * \param condition Reference to a string where the query condition is
             *                  built in.
             */
            void parseCondition(long long partitionID,
                                const std::string& tdaqRelease,
                                const std::string& partitionName,
                                coral::AttributeList& conditionData, std::string& condition);

            /**
             * \brief Returns the unique ID associated to a given partition, or -1 if none
             *        is associated to that partition.
             * \param tdaqRelease   TDAQ release.
             * \param partitionName whose ID is to be fetched.
             * \return ID associated to this partition.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            long long getPartID(const std::string& tdaqRelease,
                                const std::string& partitionName);

            /**
             * \brief Returns the highest partition ID.
             * \return Highest partition ID.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            long long getLastPartID();
      };

   } // ls
} //daq

#endif //LS_PARTITIONIDTABLE_H
