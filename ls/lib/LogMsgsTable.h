//----------------------------------------------------------------------------------------
// Title         : Log message table
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogMsgsTable.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 8/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogMsgsTable
// Description   : Provides access to the Log Message. Access is always for a partition.
//                 A unique ID per message is not used when inserting them  in the database.
//                 The likelyhood of having two messages with exactly the same values in all
//                 the fields is very small. Nevertheless, if that case occurred there is no
//                 gain in having two identical entries.
// NOTE: the table cannot be created for the primary database (Oracle) since CORAL does not
//       provide the sufficient interface to define all aspects (Oracle paritioning, etc.).
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
//  8/Jun/2007: created.
//----------------------------------------------------------------------------------------

#ifndef _LS_LOG_MSGS_TABLE_H
#define _LS_LOG_MSGS_TABLE_H

#include <ostream>
#include <memory>
#include <boost/noncopyable.hpp>

#include "ls/db/include.h"
#include "ers/Severity.h"

// Forward declarations
class AttributeList;

namespace daq {
   namespace ls {
      // Forward declarations
      class LogBuffer;

      class LogMsgsTable: public CoralItems::Table, private boost::noncopyable
      {
         public:
             //  Structure to define a single log message
            struct LogMessage : public CoralItems::Table::OneRow
            {
               LogMessage() {;};
               LogMessage(const std::string& user,
                          long               session,
                          long               runnumber,
                          const std::string& id,
                          const std::string& machine,
                          const std::string& application,
                          time_t             dateTime,
                          ers::severity      severity,
                          const std::string& text,
                          const std::string& parameters,
                          const std::string& optParam,
                          const std::string& qualifiers,
                          const std::string& chainedMsgs)
                          : m_userName(user),
                            m_session(session),
                            m_runnumber(runnumber),
                            m_id(id),
                            m_machine(machine),
                            m_application(application),
                            m_dateTime(dateTime),
                            m_severity(severity),
                            m_text(text),
                            m_parameters(parameters),
                            m_optParam(optParam),
                            m_qualifiers(qualifiers),
                            m_chainedMsgs(chainedMsgs) {;};

               ~LogMessage() { ; };
               void dump(std::ostream& stream = std::cout) const;

               std::string m_userName;
               long long   m_session;
               long long   m_runnumber;
               std::string m_id;
               std::string m_machine;
               std::string m_application;
               time_t      m_dateTime;
	       ers::severity    m_severity;
               std::string m_text;
               std::string m_parameters;
               std::string m_optParam;
               std::string m_qualifiers;
               std::string m_chainedMsgs;
            };

         public:
            /* \brief The contructor: fills in the session and name data members.
             * \param session Session to use the table in.
             * \param partID ID Associated to the running partition.
             * \param user User name. If an empty string is passed, this
             *                  parameter is not taken into account.
             * \param session Session ID. If an empty string is passed, this
             *                  parameter is not taken into account.
             */
            LogMsgsTable(CoralItems::Session &session,
                         long partID,
                         std::string const& userName,
                         long session_id);

             /**
             * \brief Always throws an exception since this table cannot be crated via CORAL.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            void create();

            /* \brief Inserts a single entry in the Log Msgs table.
             * \param oneRow Entry to insert in the Log Msgs table. An object
             *               of type LogMessage should be passed instead.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            void insertRow(CoralItems::Table::OneRow& onemsg);

            /* \brief Bulk insert. This method should be the preferable to insert logs
             *        in a table. It gets all the messages from the LogBuffer and does a
             *        a bulk insert. This is the most efficient way to write onto the database.
             * \param logBuffer Log buffer with the messages received via the MTS interface.
             * \return Number of entires inserted in the database.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            unsigned int insertRowBulk(LogBuffer& logBuffer);

            /* \brief Gets the parameters for a Log Msgs table and constructs the
             *        bind variables and condition string. That is, it constructs a
             *        criteria to work with the table.
             * \param machine Machine name. If an empty string is passed, this parameter
             *              is not taken into account.
             * \param app Application name. If an empty string is passed, this parameter
             *             is not taken into account.
             * \param whenLow Lower threshold time in UTC format. If -1, is passed, this
             *                parameter is not taken into account.
             * \param whenUp Upper threshold time in UTC format. If -1, is passed, this
             *                parameter is not taken into account.
             * \param severity Severity level as an integer. If -1, is passed, this
             *                parameter is not taken into account.
             * \param msgID Message name. If an empty string is passed, this
             *                  parameter is not taken into account.
             * \param msgTxt Message body. If an empty string is passed, this parameter
             *            is not taken into account.
             * \param msgParam Message parameters. If an empty string is passed, this
             *              parameter is not taken into account.
             * \param msgOptParam Message optional parameters. If an empty string is passed, this
             *              parameter is not taken into account.
             * \param msgQual Message qualifiers. If an empty string is passed, this
             *              parameter is not taken into account.
             * \param conditionData Reference to an Attribute list object where the
             *                      table parameters are appended.
             * \param condition Reference to a string where the query condition is
             *                  built in.
             */
            void parseCondition(long runnumber, const std::string& machine, const std::string& app,
                                long whenLow, long whenUp, short severity, const std::string& msgID,
                                const std::string& msgTxt, const std::string& msgParam,
                                const std::string& msgOptParam, const std::string& msgQual,
                                coral::AttributeList& conditionData, std::string& condition);

         private:
            long        m_partID;
            std::string m_userName;
            long        m_session;
            coral::AttributeList m_tableSchema;
      };
   } // ls
} //daq

#endif //_LS_LOG_MSGS_TABLE_H
