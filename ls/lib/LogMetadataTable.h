//----------------------------------------------------------------------------------------
// Title         : Metadata table
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogMetadataTable.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 8/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogMetadataTable
// Description   : Class representing the Metadata table. This table is used to map the
//                 table name and the partition name. In addition, it stores the user that
//                 started the partition and the session value, that is, the data and time
//                 when the table was created. Only one table is kept and created if it
//                 does not exist when a partition is started.
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
//  8/Jun/2007: created.
//----------------------------------------------------------------------------------------

#ifndef LS_METADATATABLE_H
#define LS_METADATATABLE_H

#include <string>
#include <ostream>
#include <boost/noncopyable.hpp>

#include "ls/db/include.h"


// Forward declarations
class AttributeList;

namespace daq {
   namespace ls {
      /**
       * \class LogMetadataTable "ls/lib/LogMetadataTable.h"
       * \brief Class representing the Metadata table. This table is used to map
       *        map the table name and the partition name. In addition, it stores
       *        the user that started the partition and the session value, that is,
       *        the date and time when the table was created. Only one table is kept
       *        and created if it does not exist when a partition is started.
       */
      class LogMetadataTable : public CoralItems::Table, private boost::noncopyable
      {
         public:
            /**
             * \struct MetadataRow "ls/lib/LogMetadataTable.h"
             * \brief Structure that encapsulates an entry in the Metadata table;
             */
            struct MetadataRow : public CoralItems::Table::OneRow
            {
               MetadataRow() {;};
               MetadataRow(long long          partitionID,
                           const std::string& user,
                           long long          runNumber)
                           : m_tableID(partitionID),
                             m_user(user),
                             m_runNumber(runNumber) {;};

              ~MetadataRow() {;};

               long long     m_tableID;
               std::string   m_user;
               long long     m_runNumber;
            };

            /* \brief The contructor: fills in the sessino and name data members.
             * \param session Session to use the table in.
             */
            LogMetadataTable(CoralItems::Session& session);

            /**
             * \brief Creates the Metadata table.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            void create();

            /**
             * \brief Inserts a single entry in the Metadata table.
             * \param oneRow Entry to insert in the Metadata table. An object
             *               of type MetadataRow should be passed instead.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            void insertRow(CoralItems::Table::OneRow& onemsg);

            /**
             * \brief Inserts a single entry in the Metadata table.
             * \param oneRow Entry to insert in the Metadata table. An object
             *               of type MetadataRow should be passed instead.
             * \return Number of entires inserted in the database.
             * \throw ls::TableFailed Thrown if a CORAL exception occurs.
             */
            unsigned int addMetadataInfo(CoralItems::Table::OneRow& onemsg);

            /**
             * \brief Gets the parameters for a Metadata table and constructs the
             *        bind variables and condition string. That is, it constructs a
             *        criteria to work with the table.
             * \param partitionID ID associated to a given partition.
             * \param user User name. If an empty string is passed, this parameter
             *             is not taken into account.
             * \param session Session ID or UTC time when the partition started. If
             *                -1 is passed, this parameter is not taken into account.
             * \param conditionData Reference to an Attribute list object where the
             *                      table parameters are appended.
             * \param condition Reference to a string where the query condition is
             *                  built in.
             */
            void parseCondition(long long partitionID,
                                const std::string& user,
                                long long runNumber,
                                coral::AttributeList& conditionData,
                                std::string& condition);
      };

   } // ls
} //daq

#endif //LS_METADATATABLE_H
