//----------------------------------------------------------------------------------------
// Title         : Log Buffer
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogBuffer.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 12/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogBuffer
// Description   : This class implements a buffer for log messages. When (by default)
//                 500 msgs are in the buffer a bulk insert in the database is performed.
//                 This increases database access performance. If the buffer is full,
//                 messages are discarded and the statistics updated accordingly. All
//                 methods in this class are thread safe
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 12/Jun/2007: created.
//----------------------------------------------------------------------------------------

#ifndef _LS_LOG_BUFFER_H_
#define _LS_LOG_BUFFER_H_

#include <boost/noncopyable.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/thread/mutex.hpp>

#include "ls/lib/LogMsgsTable.h"


namespace daq {
   namespace ls {

      class LogBuffer: private boost::noncopyable
      {
         public:
            LogBuffer(unsigned int size, std::string const& userName);
            LogBuffer();

            // Inserts an ERS issue.
            void insert(ers::Issue const& issue, long runNumber);
            // Inserts a log message.
            void insert(LogMsgsTable::LogMessage* msg);
            // Retrieves message at index.
            // Throws LogBufferFailed if index is out of bounds.
            LogMsgsTable::LogMessage const& at(unsigned int indx) const;
            // Throws LogBufferFailed if index is out of bounds.
            LogMsgsTable::LogMessage& at(unsigned int indx);
            // Swaps this buffer with other.
            void swap(LogBuffer& other);
            // Transfers the ownership of other buffer to this one.
            void transfer(LogBuffer& other);

            // Returns the number of messages buffered.
            size_t size();
            // Returns true if the buffer is full. False otherswise.
            bool isFull();
            // Returns the capacity of the Log Buffer.
            unsigned int capacity() const { return m_buffer.capacity(); }
            // Returns the number of messages buffered that trigger a bulk insert.
            unsigned int threshold() const { return m_threshold; }

            std::string userName() const;

         private:
            typedef boost::ptr_vector<LogMsgsTable::LogMessage> Buffer;

            static const unsigned int StrMaxLength  = 4000; // Maximum length for columns of type string
            static const unsigned int CapacityRatio =   20; // Ratio of the Log buffer to dump the messages onto the database.

         private:
            void m_buildChainedMessage(LogMsgsTable::LogMessage* msg, ers::Issue const* issue) const;
            std::string m_trimField(std::string const& field) const;

         private:
            Buffer            m_buffer;
            unsigned int      m_threshold;
            std::string const m_userName;
            mutable boost::mutex m_bufMutex;
      };
   }
}

#endif //_LS_LOG_BUFFER_H_
