//----------------------------------------------------------------------------------------
// Title         : Database access
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : BackupDatabase.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 26/Mar/2012
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : BackupDatabase
// Description   : This class encapsulates the access to the backup database: SQLite.
//----------------------------------------------------------------------------------------
// Copyright (c) 2012 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 26/Mar/2012: created.
//----------------------------------------------------------------------------------------

#ifndef _LS_BACKUP_DATABASE_H_
#define _LS_BACKUP_DATABASE_H_

#include <memory>                      // unique_ptr
#include <boost/noncopyable.hpp>

#include "ls/db/include.h"


namespace daq {
    namespace ls {
       // Forward declarations
       class LogBuffer;

        /**
         * \class LogBuffer "ls/lib/BackupDatabase.h"
         * \brief This class encapsulates the access to the backup database: SQLite.
         */
        class BackupDatabase: private boost::noncopyable
        {
           public:
               BackupDatabase(std::string const& conString, std::string const& userName);

               // Initializes the access to the primary database.
               bool initialize();
               // Stores the buffer into the database.
               // throw TableFailed if accessing the database failed.
               unsigned int insertBuffer(LogBuffer& logBuffer);
               // Retrieves a list of run numbers.
               // throw TableFailed if accessing the database failed.
               void getListRunNumbers(std::vector<unsigned int>& runNumbers) const;
               // Retrieve 500 logs for a given run number.
               // throw TableFailed if accessing the database failed.
               unsigned int getBulkSelect(unsigned int runNumber,
                                          unsigned int numMsgs,
                                          unsigned int offset,
                                          LogBuffer& logBuffer) const;
               // Removes all messages for a given run number.
               // throw TableFailed if accessing the database failed.
               void removeRunNumberMsgs(unsigned int runNumber) const;

            private:
               std::unique_ptr<CoralItems::Session> m_session;
               std::string const m_conString;
               std::string const m_userName;
        };
    }
}

#endif //_LS_BACKUP_DATABASE_H_
