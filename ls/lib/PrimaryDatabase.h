//----------------------------------------------------------------------------------------
// Title         : Database access
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : PrimaryDatabase.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 26/Mar/2012
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : PrimaryDatabase
// Description   : This class encapsulates the access to the primary database: ORACLE.
//----------------------------------------------------------------------------------------
// Copyright (c) 2012 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 26/Mar/2012: created.
//----------------------------------------------------------------------------------------

#ifndef _LS_PRIMARY_DATABASE_H_
#define _LS_PRIMARY_DATABASE_H_

#include <memory>
#include <boost/noncopyable.hpp>

#include "ls/db/include.h"


namespace daq {
    namespace ls {
       // Forward declarations
       class LogBuffer;

        /**
         * \class LogBuffer "ls/lib/PrimaryDatabase.h"
         * \brief This class encapsulates the access to the primary database: ORACLE.
         */
        class PrimaryDatabase: private boost::noncopyable
        {
           public:
              PrimaryDatabase(std::string const& conString,
                              std::string const& tdaqRelease,
                              std::string const& partitionName,
                              std::string const& userName);

               // Initializes the access to the primary database.
               bool initialize();
               // Retrieves the Partition ID associated to the partition name.
               // throw TableFailed if accessing the database failed.
               unsigned int getPartitionId() const;
               // Retrieves the last run number recorded in the database.
               // throw TableFailed if accessing the database failed.
               unsigned int getLastRunNumber(unsigned int partitionId) const;
               // Stores the buffer into the database.
               // throw TableFailed if accessing the database failed.
               unsigned int insertBuffer(LogBuffer& logBuffer, unsigned int partitionId);
               // Inserts a new run number in the metadata table.
               // throw TableFailed if accessing the database failed.
               void insertNewRunNumber(unsigned int partitionId, unsigned int runNumber) const;
               // Ping the database to avoid disconnection due to timeout expiration.
               void pingDatabase() const;

            private:
               // Oracle idle timeout is 200 minutes. Ping the database
               // if there is no activity during 120 minutes.
               static const int ConnectionTimeout = 60 * 120;

            private:
               std::unique_ptr<CoralItems::Session> m_session;

               std::string const m_conString;
               std::string const m_tdaqRelease;
               std::string const m_partitionName;
               std::string const m_userName;
               mutable time_t    m_lastConnection; // Time of the last connection to the database.
        };
    }
}


#endif //_LS_PRIMARY_DATABASE_H_
