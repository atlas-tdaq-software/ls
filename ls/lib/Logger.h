//----------------------------------------------------------------------------------------
// Title         : Logger agent
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : Logger.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 26/Mar/2102
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : Logger
// Description   : This class provides the interface to run the logger agent of the
//                 Log Service, that is, it subscribes to ERS and stores the messages to
//                 an ORACLE database via the CORAL interface. If this database is not
//                 accessible, it switches to a backup alternative: SQLite.
//----------------------------------------------------------------------------------------
// Copyright (c) 2012 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 26/Mar/2102: created.
//----------------------------------------------------------------------------------------

#ifndef _LS_LOGGER_H_
#define _LS_LOGGER_H_

#include <boost/noncopyable.hpp>
#include <ers/ers.h>

#include "ls/lib/LogBuffer.h"
#include "ls/lib/LogServiceStats.h"
#include "ls/lib/LogServiceInfoNamed.h"
#include "ls/lib/LogTriggerCondition.h"
#include "ls/lib/PrimaryDatabase.h"
#include "ls/lib/BackupDatabase.h"

#include <mutex>

namespace daq {
   namespace ls {

      class Logger: public ers::IssueReceiver, private boost::noncopyable
      {
         public:
            Logger(std::string const& tdaqRelease,
                   std::string const& partitionName,
                   std::string const& userName,
                   std::string const& isSrvName,
                   std::string const& ersTransportName,
                   std::string const& primaryConString,
                   std::string const& backupConString,
                   unsigned int bufferSize);

            // Starts listening for issues.
            // Throws LogServiceFailed if writing to the backup database failed.
            void run(std::string const& expression);
            // Initializes the Logger
            // Throws LogServiceFailed if neither of the database could be initialized.
            void initialize(bool restarted);
            // Stops the logger.
            void stop();
            // Switch to using the primary database.
            void restorePrimaryDb();

         private:
            static const unsigned int TaskPeriod =  10; // Period task period.

            //@TODO use this timeout static const int IsPrimaryDbUpPeriod = 300; // If the primary DB is down, check every 5 min. if it is up.
            static const int IsPrimaryDbUpPeriod = 10; // If the primary DB is down, check every 5 min. if it is up.

            enum DatabaseType { DB_PRIMARY, DB_BACKUP };

         private:
            //  ERS callback function.
            void receive(ers::Issue const& issue);
            // Periodic task to publish the Log Receiver statistics.
            static bool m_periodicTask(void * parameter);
            // Get run number from IS.
            unsigned int m_getRunNumberOnIs() const;
            // Checks if a the issue signals a new run number.
            void m_isNewRunNumber(ers::Issue const& issue);
            // Inserts the current run number in the metadata table. This is not done
            // in m_isNewRunNumber() to return from receive() as soon as possible.
            void m_insertRunNumber() const;
            // Move messages from the backup database to the primary database.
            // Throws LogServiceFailed if neither of the database could be initialized.
            void m_moveMessages(unsigned int lastRunNumber);
            // Publishes the log services statistical info on IS.
            void m_publish() const;
            // Retuns a human readable name for the active database.
            std::string m_dbActive() const { return (DB_PRIMARY == m_dbType ? "Primary" : "Backup"); }

         private:
            PrimaryDatabase m_db;     // Primary database
            BackupDatabase m_bckpDb;  // Backup datbase.
            volatile DatabaseType m_dbType; // Database type: primary or backup.

            std::string  m_tdaqRelease;   // TDAQ release.
            std::string  m_partitionName; // Partition name.
            unsigned int m_partitionID;   // Partition ID associated to a partition name.
            std::string  m_userName;      // User name.
            unsigned int m_runNumber;     // Run Number value

            LogBuffer m_logBuffer;          // Buffer with the log messages.
            LogTriggerCondition m_trigCond; // Condition variable used to trigger the logging of buffered msgs.

            std::unique_ptr<LogServiceInfoNamed> m_ISinfo; // IS structure with the Log Receiver statistics.
            LogServiceStats m_stats; // Log Receiver statistics.

            volatile bool m_stopFlag; // When set to true, it stops the log receiver.
            time_t m_lastDbUpCheck;   // Time of the last connection to the database.
	        std::string m_ersTransportName;
	        mutable std::mutex m_mutex;
      };
   }  // namespace ls
}  // namespace daq


#endif //_LS_LOGGER_H_

