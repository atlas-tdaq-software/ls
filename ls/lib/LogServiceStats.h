//----------------------------------------------------------------------------------------
// Title         : Log service statistics
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogServiceStats.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 11/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogServiceStats
// Description   : Encapsualtes the statistical paramters for the Log Service, that is, the
//                 number of messages received via the MTS interface, the number of messages
//                 successfully logged onto the database, the number of messages discarded
//                 because the buffer was full and whether the buffer is currently full.
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 11/Jun/2007: created.
//----------------------------------------------------------------------------------------

#ifndef LS_LOGSERVICESTATS_H_
#define LS_LOGSERVICESTATS_H_

#include <boost/noncopyable.hpp>


namespace daq {
   namespace ls {
      /**
       * \class StatsEntry "ls/lib/LogServiceStats .h"
       * \brief Template class that implements the required access methods of
       *        the statistical parameters for the Log Service.
       */
      template <typename T> class StatsEntry: private boost::noncopyable
      {
         public:
            StatsEntry() { ; };
            StatsEntry(T& v) : value(v) { ; };
            ~StatsEntry() { ; };

            inline const T get(void) const { return value; } ;
            inline void set(T v) { value = v; };
            inline void inc(T v) { value += v; };
            inline void dec(T v) { value -= v; };
            inline void operator++() { ++value; };
            inline void operator--() { --value; };

         private:
            T value; // The value stored.
      };

      /**
       * \class LogServiceStats "ls/lib/LogServiceStats .h"
       * \brief Encapsualtes the statistical paramters for the Log Service, that is,
       *        the number of messages received via the MTS interface, the number of
       *        messages successfully logged onto the database, the number of messages
       *        discarded because the buffer was full and whether the buffer is currently
       *        full.
       */
      class LogServiceStats: private boost::noncopyable
      {
         public:
            LogServiceStats()
            {
               m_msgsReceived.set(0);
               m_msgsLogged.set(0);
               m_insertRate.set(0);
               m_msgsDiscarded.set(0);
               m_bufferFull.set(false);
            }
            ~LogServiceStats() { ; };

         private:
            friend class Logger;

            StatsEntry< unsigned long > m_msgsReceived;  // Log messages received via the MTS interface.
            StatsEntry< unsigned long > m_msgsLogged;    // Number of log messages inserted.
            StatsEntry< unsigned long > m_insertRate;    // Log messages received via the MTS interface.
            StatsEntry< unsigned long > m_msgsDiscarded; // Log messages discarded and lost. Messages are discarded if the log msgs buffer overflows.
            StatsEntry< bool >          m_bufferFull;    // If TRUE, buffer is full and messages being discarded.
      };
   }
}

#endif //LS_LOGSERVICESTATS_H_
