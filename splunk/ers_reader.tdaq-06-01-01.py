#!/usr/bin/env tdaq_python

import cx_Oracle
import os
import sys
from time import localtime,strftime
import time
from datetime import date
from datetime import datetime
import binascii
import xml.etree.ElementTree as et


def parse_ERS_chainedmsgstr(S):
    """
    "MsgStart: ID:[X]Host:[X]App:[X]Time:[X]Sev:[X]Txt:[X]Param:[X]Opt:[X]Qual:[X]EndMsg "
    """

    ERS_PARSER_FIELDS = set(["Param", "Time", "Txt", "App", "Opt", "Host", "ID", "Sev", "Qual"])
    ERS_PARSER_KEY_SUFFIX = ":["
    #ERS_PARSER_KEY_PREFIX = "]"

    result = []
    for p in S.split(']EndMsg '):
        m = {}

        idxs = []
        for y in [p.find(x + ERS_PARSER_KEY_SUFFIX) for x in ERS_PARSER_FIELDS]:
            if y >= 0:
                idxs.append(y)
        idxs.sort()

        idxs2 = []
        for i, x in enumerate(idxs):
            if i < (len(idxs) - 1):
                idxs2.append((x, idxs[i+1]))
            else:
                idxs2.append((x, len(p)))

        for i, e in enumerate(idxs2):
            x, y = e
            ek, ev = p[x:y].split(':', 1)
            if i < (len(idxs2) - 1):
                m[ek] = ev[1:-1]
            else:
                m[ek] = ev[1:] # The last one does not contain ']'

        if len(m):
            if "Qual" not in m:
                m["Qual"] = ""

            if set(m.keys()) >= ERS_PARSER_FIELDS:
                result.append(m)
            else:
                raise Exception("Parse error: missing fields in message " + repr(m))

    return result

def get_coral_connect_string():
	logical_name = "LOG_MANAGER"

	try:
		tree = et.parse(os.environ['CORAL_DBLOOKUP_PATH'] + "/dblookup.xml")
	except KeyError:
		sys.stderr.write('Error: CORAL_DBLOOKUP_PATH must be defined\n')
        	sys.exit(2)
		
	logserv = tree.findall("./logicalservice[@name='" + logical_name + "']")
	coral_connect = logserv[0].findall("./service")[0].attrib['name']
	parts = coral_connect.split('/')
	service_name = parts[2]

	try:
		filename = os.environ['CORAL_AUTH_PATH']
	except KeyError:
		sys.stderr.write('Error: CORAL_AUTH_PATH must be defined\n')
        	sys.exit(2)
		
	tree = et.parse(filename + "/authentication.xml")
	conn = tree.findall("./connection[@name='" + coral_connect + "']")
	user = conn[0].findall("./parameter[@name='user']")[0].attrib['value']
	pwd = conn[0].findall("./parameter[@name='password']")[0].attrib['value']

	return user + '/' + pwd + '@' + service_name


last_eventid_filepath = "/data/splunk/ers/.lastid" # user supplies correct path
#last_eventid_filepath = "/opt/splunk/etc/apps/ers/bin/.lastid.tdaq-06-01-00"

datafile_dir = "/data/4splunk"
datafile_path = datafile_dir + "/ers."
now = date.today()
datafile_path += now.isoformat()

############# NUMBER OF ROW LIMIT
max_number_of_rows = 100000000

# Open file containing the last event ID and get the last record read
last_eventtime = 0;
if os.path.isfile(last_eventid_filepath):
    try:
        last_eventid_file = open(last_eventid_filepath,'r')
        last_eventtime = int(last_eventid_file.readline())
        last_eventid_file.close()

    # Catch the exception. Real exception handler would be more robust    
    except IOError:
        sys.stderr.write('Error: failed to read last_eventid file, ' + last_eventid_filepath + '\n')
        sys.exit(2)
else:
    sys.stderr.write('Error: ' + last_eventid_filepath + ' file not found! Starting from zero. \n')

# Fetch 1000 rows starting from the last event read
# SELECT TOP 1000 eventID, transactionID, transactionStatus FROM table WHERE eventID > lastEventID ORDER BY eventID
#sql_query = #"SELECT * FROM log_messages WHERE rownum<=10"

if last_eventtime == 0:
    last_eventtime = int(time.time() - 3600*24) #Last day of messages

#from 6Feb to 31July 2012
from_rn=196007
to_rn=207809

#print "Last event %s" % (last_eventtime)

sql_query= 'SELECT log_messages.issued_when, log_messages.user_name, log_messages.MSG_ID, log_messages.machine_name, log_messages.application_name, log_messages.severity, log_messages.msg_text, log_messages.param, log_messages.opt_param, log_messages.run_number,log_messages.qualifiers, partition_ids.partition_name, log_messages.chained_msgs FROM log_messages, partition_ids, metadata WHERE log_messages.run_number = metadata.run_number AND metadata.partition_id = partition_ids.partition_id AND partition_ids.tdaq_release = \'tdaq-06-01-01\' AND log_messages.part_id = partition_ids.partition_id AND metadata.timestamp > ' + str(last_eventtime-3600*24*7*4) + ' AND issued_when > ' + str(last_eventtime)  + ' AND issued_when < ' + str(time.time() - 120) + ' ORDER BY log_messages.issued_when'

#print sql_query

# ERS, tdaq-05
sev2str = ["DEBUG",  "LOG", "INFO", "WARNING", "ERROR", "FATAL", "UNDEFINED"]

try:
    connection = cx_Oracle.connect(get_coral_connect_string())
    cursor = connection.cursor()
    result = cursor.execute(sql_query)
    r = cursor.fetchall()
    if len(r) == 0:
    	#print "nothinbg to do"
        sys.exit(0)
    ret = ""
    # timestamp the returned data
	#TODO escape new line
    try:
        for row in r:
		chained=row[12]
		salt=str(datetime.now().microsecond)
		if chained == None:
	    		ret += "t=%s, rn=%s, part=%s, uname=%s, msgID=%s, host=%s, app=%s, sev=%s, text=\"%s\", context=\"%s\", params=\"%s\", quals=\"%s\", chained=0" % (row[0], row[9], row[11], row[1], row[2], row[3], row[4], sev2str[row[5]], row[6], row[7], row[8], row[10])
			ret += ", gh=%s\n" % abs((binascii.crc32(salt + row[3] + row[4] + str(row[6]) + row[7])))

		else:
			for chned in reversed(parse_ERS_chainedmsgstr(row[12])):
				ret += "t=%s, rn=%s, part=%s, uname=%s, msgID=%s, host=%s, app=%s, sev=%s, text=\"%s\", context=\"%s\", params=\"%s\", quals=\"%s\", chained=2" % (row[0], row[9], row[11], row[1], chned['ID'], chned['Host'], chned['App'], sev2str[row[5]], chned['Txt'], chned['Param'], chned['Opt'], chned['Qual'])
				ret += ", gh=%s\n" % abs((binascii.crc32(salt + row[3] + row[4] + str(row[6]) + row[7])))


			ret += "t=%s, rn=%s, part=%s, uname=%s, msgID=%s, host=%s, app=%s, sev=%s, text=\"%s\", context=\"%s\", params=\"%s\", quals=\"%s\", chained=1" % (row[0], row[9], row[11], row[1], row[2], row[3], row[4], sev2str[row[5]], row[6], row[7], row[8], row[10])			
			ret += ", gh=%s\n" % abs((binascii.crc32(salt + row[3] + row[4] + str(row[6]) + row[7])))


    	    #print "%s" % (row[0])
        #print row;
        last_eventtime = r[len(r)-1][0]
    except IndexError:
       # sys.stderr.write("No new messages from last check %s - %s\n" % (last_eventtime, time.strftime("%a, %d %b %Y %H:%M:%S +0000", 
        #                time.localtime(last_eventtime))))
	print "Error in indexing"
        connection.cancel()
        sys.exit(2)

#    print "Last event %s" % (last_eventtime)

# Catch the exception. Real exception handler would be more robust    
except  cx_Oracle.DatabaseError, exception:
    error, = exception.args
    print ("Database connection error: ", error.code, error.offset, error.message)
#Cancel long running transaction
connection.cancel()

# this is for splunk    
print ret

if os.path.exists(datafile_dir):
	try:
		print ("Writing to file ", datafile_path)
   		f = open(datafile_path, 'a')
   		f.write(ret)
   		f.close()
	except IOError:
     		sys.stderr.write('Error writing file: ' + datafile_path + '\n')
     		sys.exit(2)


if last_eventtime > 0:
   try:
     last_eventid_file = open(last_eventid_filepath,'w')
     last_eventid_file.write(str(last_eventtime))
     last_eventid_file.close()
   # Catch the exception. Real exception handler would be more robust    
   except IOError:
     sys.stderr.write('Error writing last_eventid to file: ' + last_eventid_filepath + '\n')
     sys.exit(2)

