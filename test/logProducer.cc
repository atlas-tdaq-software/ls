/*
 *  LogProducer.cc
 *  logService
 *
 *  Created by Raul Murillo-Garcia on 21.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */
 /** \file 
  * Just sends ers issues.
  */

#include <stdlib.h>

#include <ers/ers.h>
#include <ipc/core.h>

#include "ls/Exceptions.h"

using namespace daq::ls;

int main(int argc, char ** argv)
{
   // Do all the IPc and initialization
   try {
      IPCCore::init(argc, argv);
   }
   catch (daq::ipc::CannotInitialize& issue) {
      ers::error(issue);
      exit(-1);
   }
   catch (daq::ipc::AlreadyInitialized& issue) {
      ers::warning(issue);
   }

   TestUnitFailed issue1(ERS_HERE, "log producer msg 1", " dummy 1");
   ers::fatal(issue1);
   
//   sleep(4);   
   TestUnitFailed issue2(ERS_HERE, "log producer msg 2", " dummy 2");
   ers::error(issue2);   
   
   TestUnitFailed issue3(ERS_HERE, "log producer msg 3", " dummy 3");
   ers::warning(issue3);

 //  sleep(4);      
   TestUnitFailed issue4(ERS_HERE, "log producer msg 4", " dummy 4");
//   ers::error(issue4);   

   TestUnitFailed issue5(ERS_HERE, "log producer msg 5", " issue 5", issue4);
   ers::info(issue5);   
               
 //  TestUnitFailed issue5(ERS_HERE, "log producer msg 6", " issue 6");
   //ers::success(issue6);   


   return 0;

}
