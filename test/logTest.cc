/*
 *  logTest.cc
 *  logService
 *
 *  Created by Raul Murillo-Garcia on 05.07.07
 *  Copyright 2007 CERN. All rights reserved.
 *
 */

#include <stdlib.h>
#include <unistd.h>

#include <cmdl/cmdargs.h>

#include <ers/ers.h>

#include <ipc/core.h>

#include "ls/lib/LogMsgsTable.h"
#include "ls/lib/LogMetadataTable.h"
#include "ls/lib/PartitionIDTable.h"
#include "ls/db/CoralSession.h"
#include "ls/Exceptions.h"
#include "tmgr/tmresult.h"

using namespace daq::ls;


int complexTest(const std::string& connectionString,
                const std::string& tdaqRelease,
                const std::string& partitionName) throw();

inline const std::string  str(const char * word)
{
   return word ? word : "";
}


/** Tests the functionality of the Log Service and that the infrastructure is working.
  * First it sends 4 msgs per severity level via the ERS service. The Log Receiver should
  * capture these messasges and store them onto the DB. Next step is to retrieve them and
  * finally to remove them.
  * \param partitionName: partition name
  */
int complexTest(const std::string& connectionString,
                const std::string& tdaqRelease,
                const std::string& partitionName)
                throw()
{
  int retVal = daq::tmgr::TmFail;

   try {
      // Log one messages for each severity level

      const std::string msgTxt  = "Message added during the test procedure";
      TestLS t(ERS_HERE, msgTxt.c_str());
      TestLS tf(ERS_HERE, "LS_COMMAND_FLUSH");
      ers::info(t);
      ers::warning(t);
      ers::error(t);
      ers::fatal(tf);

      // Open session
      daq::ls::CoralItems::Session session(connectionString);

      // Table with the mapping of partition name and partition id.
      PartitionIDTable partitionIDsTable(session);
      long partID = partitionIDsTable.getPartID(tdaqRelease, partitionName);
      if (-1 == partID) {
         std::string warnStr("The TDAQ release " + tdaqRelease + " and partition name " + partitionName + " pair has no PARTITION_ID associated to it.");
         ers::warning(TableFailed(ERS_HERE, "read" , "PartitionID", warnStr.c_str()));
         return retVal;
      }
      ERS_DEBUG(0, "Partition ID " << partID << " is associated to TDAQ release " << tdaqRelease <<
                   " and partition " << partitionName << ".");

      // Get the messages from the log message table
      daq::ls::LogMsgsTable lsTable(session, partID, "", -1);

      coral::AttributeList condtionData;
      std::string condition;

      lsTable.parseCondition(-1, "", "", -1, -1, -1, "ls::TestLS", "", "", "", "", condtionData, condition);

      unsigned int msgsGot = 0;
      for (int i = 0; i < 3; i++) {
         msgsGot += lsTable.select(condtionData, condition, "", "", "");
         if (msgsGot >= 4) {
            break;
         }
         sleep(1);
      }

      if (4 == msgsGot) {
         ERS_DEBUG(2, msgsGot << " messages successfully retrieved.");
      }
      else {
         ERS_DEBUG(1, "An unexpected number of messages was retrieved: " << msgsGot << ", when expected 6.");
         retVal = daq::tmgr::TmUndef;
      }

      // Remove the test messages from the table
      msgsGot = lsTable.removeRow(condtionData, condition);
      if (4 == msgsGot) {
         ERS_DEBUG(2, msgsGot << " messages successfully removed.");
         retVal = daq::tmgr::TmPass;
      }
      else {
         ERS_DEBUG(1, "An unexpected number of messages was removed: " << msgsGot << ", when expected 6.");
         retVal = daq::tmgr::TmUndef;
      }

   }
   catch (daq::ls::Exception &e)  {
      ers::fatal(e);
   }
   catch(ers::Issue &e) {
      ers::fatal(e);
   }
   catch (...) {
      ERS_DEBUG(0, "Unknown error thrown.");
   }

   return retVal;
}

/** If level of complexity is 1 then it only opens and closes a connection.
  * If level is 2 it does further tests (see explanation for doComplextest())
 */
int main(int argc, char ** argv)
{
   int retVal = daq::tmgr::TmPass;

   IPCCore::init(argc,argv);
   // Declare arguments
   CmdArgStr connectionString('c', "connectionString", "connect-string", "Database connection string.", CmdArg::isREQ);
   CmdArgStr tdaqRelease('t', "TDAQrelease", "TDAQ-release", "TDAQ release.", CmdArg::isREQ);
   CmdArgStr partitionName('p', "partition", "partition-name", "partition name.", CmdArg::isOPT);
   CmdArgInt complexityLevel('l', "level", "complexity-level", "Level of Complexity of the test [1: open/close - 2: tests the Log Service Infrastructure].", CmdArg::isOPT);

   tdaqRelease     = "";
   partitionName   = std::getenv("TDAQ_PARTITION");
   complexityLevel = 1;


   // Declare command object and its argument-iterator
   CmdLine cmd(*argv, &connectionString, &tdaqRelease, &partitionName, &complexityLevel, NULL);
   cmd.description("Test binary for the Log Receiver application.");

   CmdArgvIter arg_iter(--argc, ++argv);


   // Parse arguments
   cmd.parse(arg_iter);

   // Make sure the complexity level is correct

   if (complexityLevel < 1)
     complexityLevel = 1;

   if (complexityLevel > 2)
     complexityLevel=2;

   try {
      if (complexityLevel == 1) {
         // Just create a session, this will open a connection and
         // when it leaves this scope it will close the connection.
         ERS_DEBUG(0, "Testing the Database connection.");
         CoralItems::Session session(str(connectionString));
      }
      else if (complexityLevel == 2) {
         ERS_DEBUG(0, "Testing the Log Service infrastructure.");
         retVal = complexTest(str(connectionString),
                              str(tdaqRelease),
                              str(partitionName));
      }
      else {
         ERS_DEBUG(2, "You should have never got here...");
         return daq::tmgr::TmUndef;
      }
   }
   catch (daq::ls::LoggerFailed& e) {
      retVal = daq::tmgr::TmFail;
      ers::fatal(e);
   }
   catch (daq::ls::Exception& e) {
      retVal = daq::tmgr::TmFail;
      ers::fatal(e);
   }
   catch (...) {
      retVal = daq::tmgr::TmFail;
      ERS_DEBUG(0, "Unknown error thrown.");
   }

   if (retVal == daq::tmgr::TmPass) {
      ERS_DEBUG(1, "The Log Service test was successful.");
   }
   else {
      ERS_DEBUG(1, "The Log Service test failed.");
   }

   return retVal;
}

