/*
 *  logProducer.cc
 *  logService
 *
 *  Created by Raul Murillo-Garcia on 11.06.07
 *  Copyright 2007 CERN. All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include <ipc/core.h>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>

#include <pmg/pmg_initSync.h>

#include "ls/Exceptions.h"


volatile bool stopRunning;


void signalHandler(int )
{
	stopRunning = true;
}


int main(int argc, char ** argv)
{
	// Do all the IPc and initialization
	try {
		IPCCore::init(argc, argv);
	}
	catch (daq::ipc::CannotInitialize& issue) {
		ers::error(issue);
		exit(-1);
	}
	catch (daq::ipc::AlreadyInitialized& issue) {
		ers::warning(issue);
	}

	// Declare arguments
	CmdArgInt  period('t', "period", "period", "Period in ms between burst of messages", CmdArg::isOPT);

	// Default values
	period = 1000;

	// Declare command object and its argument-iterator
	CmdLine cmd(*argv, &period, NULL);
	cmd.description("This is a log producer, sending messages at a user defined rate.");

	// Parse arguments
	CmdArgvIter argvIter(--argc, ++argv);
	if ( cmd.parse(argvIter) ) {
		printf("\nCommand parse failed\n");
		exit(-1);
	}

	useconds_t uperiod = period;
	uperiod *= 1000; // in usecs

	// Install signals handler
	struct sigaction saint;
	sigemptyset(&saint.sa_mask);
	sigaddset(&saint.sa_mask, SIGINT);
	sigaddset(&saint.sa_mask, SIGTERM);
	saint.sa_flags = 0;
	saint.sa_handler = signalHandler;
	sigaction(SIGINT, &saint, NULL);
	sigaction(SIGTERM, &saint, NULL);

	stopRunning = false;

    struct timeval tv;
    gettimeofday(&tv, NULL);
    srand(tv.tv_sec * tv.tv_usec);

    int starttimeout = 10 + rand() % 10;
    std::cout << "\nSleeping for " << starttimeout << " seconds.\n";
    sleep(starttimeout);

    int offset = uperiod * 20 / 100; // +/- 20%
    uperiod   -= offset/2; // ~ -10%

    // Tell PMG we are up and running
    pmg_initSync();

	while (false == stopRunning) {

		int nummsgs = rand() % 6;

		switch (nummsgs) { // fall through on all
		   case 5:
		   {
			   daq::ls::ParseFailed issueF(ERS_HERE, "scalability", "");
			   ers::fatal(issueF);
		   }
		   case 4:
		   {
			   daq::ls::ParseFailed issueE(ERS_HERE, "scalability", "error   with a little text");
			   ers::error(issueE);
		   }
		   case 3:
		   {
			   daq::ls::ParseFailed issueW(ERS_HERE, "scalability", "warning with a little text here and ther just to send more ch");
			   ers::warning(issueW);
		   }
		   case 2:
		   {
			   daq::ls::ParseFailed issueD(ERS_HERE, "scalability", "debug   with a little text here and ther just to send more ch and what can I say just this and that bla bla bla");
			   ers::debug(issueD);
		   }
		   case 1:
		   {
			   daq::ls::ParseFailed issueI(ERS_HERE, "scalability", "information with a little text here and ther just to send more ch and what can I say just this and that bla bla bla and more bla bla bla bla and yet more bla bla bla bla bla                     l                  o                         n                             g                          e                      s                    t                            .");
			   ers::info(issueI);
		   }
		   case 0:
			   // no messages
			   break;
		   default:
			   break;
		}

		usleep(uperiod + rand() % offset); // +/- 20%
	}

	return 0;
}

