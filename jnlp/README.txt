Whenever a new version of logManager.jar is created, the file has to be certified to be used with Java Web Technologies. Same has to be done with the ojdbc jar, that you can take from /afs/cern.ch/project/oracle/jdbc/). 
To do so, run the following commands (you may to update path depending on your java installation):

/usr/lib/jvm/java-1.7.0-oracle-1.7.0.76.x86_64/jre/bin/keytool -genkey -alias lskey -dname "OU=ATLAS-TDAQ, O=CERN, L=Geneve, ST=Geneve, C=Switzerland" -storepass lspass -storetype JKS -keystore lmKeystore -keyalg RSA -keypass lspass
/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.75.x86_64/bin/jarsigner -keystore lmKeystore ojdbc6.jar lskey
/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.75.x86_64/bin/jarsigner -keystore lmKeystore logManager.jar lskey

The utility jarsigner will ask the user to enter the password specified in keytool (lspass). Once this is done, the resulting lmKeystore and the jar files must be copied to
atlasdaq:/var/www/html/logmanager/logmanager.

