/*
 *  logDrop.cc
 *  logService
 *
 *  Created by Raul Murillo-Garcia on 29.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */
#include <iterator>
#include <set>

#include <RelationalAccess/SchemaException.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITransaction.h>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <ers/ers.h>

#include "ls/lib/LogMsgsTable.h"
#include "ls/lib/LogMetadataTable.h"
#include "ls/lib/PartitionIDTable.h"
#include "ls/db/CoralSession.h"
#include "ls/Exceptions.h"

inline const std::string  str(const char * word)
{
   return word ? word : "";
} 


using namespace daq::ls;


int main(int argc, char ** argv)
{
   // Do all the IPc initialization
   try {
      IPCCore::init(argc, argv);
   }
   catch (daq::ipc::CannotInitialize& issue) {
      ers::error(issue);
      exit(-1);
   }
   catch (daq::ipc::AlreadyInitialized& issue) {
      ers::warning(issue);
   }

   // Declare arguments
   CmdArgStr  connectionString('c', "connectionString", "connect-string", "Database connection string.", CmdArg::isREQ);           

   // Declare command object and its argument-iterator
   CmdLine cmd(*argv, &connectionString, NULL);
   cmd.description("Application to clean the database by removing all the existing tables.");

   // Parse arguments
   CmdArgvIter argvIter(--argc, ++argv);
   if ( cmd.parse(argvIter) ) {
      daq::ls::ParseFailed issue(ERS_HERE, "");
      ers::fatal(issue);
      return EXIT_FAILURE;
   }

   bool done = false;
   char c;
   do {
      std::cout << "Are you sure you want to clean all the Log Service Database (Y/N)? ";      
      std::cin >> c;
      if (c == 'Y' || c == 'y') {
         done = true;
      } 
      else if (c == 'N' || c == 'n') {
         exit(0);
      }
      else {
         std::cout << "Wrong input, please choose either 'Y' or 'N'.\n";      
      }      
   } while (false == done); 
 
   try {
      // Open session
      daq::ls::CoralItems::Session session(str(connectionString));
      CoralItems::Transaction transaction(session);                

      // Clean the log messages table
      daq::ls::LogMsgsTable lsTable(session, -1, "", -1);
      lsTable.clean();
      
      // Clean the partition ID table
      PartitionIDTable partitionIDsTable(session);         
      partitionIDsTable.clean();      

      // Clean the metadata table.
      LogMetadataTable metaTable(session);
      metaTable.clean();


      ERS_DEBUG(0, "Log Service database cleaned.");      

      // Commit transaction
      transaction.Commit();        
   }
   catch (daq::ls::Exception& issue) {
      ers::fatal(issue);
   }
   catch (coral::Exception &e) {
      ERS_DEBUG(0, e.what());
   }    
   catch (...) {
      ERS_DEBUG(0, "Unknown error thrown."); 
   }   

   return 0;

}
