/*
 *  logDelete.cc
 *  logService
 *
 *  Created by Raul Murillo-Garcia on 11.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#include <limits.h>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <ers/ers.h>

#include "ls/lib/LogMsgsTable.h"
#include "ls/lib/LogMetadataTable.h"
#include "ls/lib/PartitionIDTable.h"
#include "ls/db/CoralSession.h"
#include "ls/Exceptions.h"

inline const std::string  str(const char * word)
{
   return word ? word : "";
}


using namespace daq::ls;


int main(int argc, char ** argv)
{
   // Do all the IPC initialization
   try {
      IPCCore::init(argc, argv);
   }
   catch (daq::ipc::CannotInitialize& issue) {
      ers::error(issue);
      exit(-1);
   }
   catch (daq::ipc::AlreadyInitialized& issue) {
      ers::warning(issue);
   }

   // Declare arguments
   CmdArgStr  connectionString('c', "connectionString", "connect-string", "Database connection string.", CmdArg::isREQ);
   CmdArgStr  partitionName('p', "partition", "partition-name", "Partition name.", CmdArg::isREQ);
   CmdArgStr  tdaqRelease('t', "TDAQrelease", "TDAQ-release", "TDAQ release.", CmdArg::isREQ);

   // Initialize arguments to appropiate default values
   tdaqRelease   = "";
   partitionName = "";

   // Declare command object and its argument-iterator
   CmdLine cmd(*argv, &connectionString, &tdaqRelease, &partitionName, NULL);
   cmd.description("Application to remove log messages for a given partition according to the command arguments.");

   // Parse arguments
   CmdArgvIter argvIter(--argc, ++argv);
   if ( cmd.parse(argvIter) ) {
      daq::ls::ParseFailed issue(ERS_HERE, "");
      ers::fatal(issue);
      return EXIT_FAILURE;
   }

   std::string partname = str(partitionName);
   std::string release  = str(tdaqRelease);

   bool done = false;
   char c;
   do {
      std::cout << "Are you sure you want to remove the logs from TDAQ release '" << release << "' and partition '" << partitionName << "' (Y/N)? ";
      std::cin >> c;
      if (c == 'Y' || c == 'y') {
         done = true;
      }
      else if (c == 'N' || c == 'n') {
         exit(0);
      }
      else {
         std::cout << "Wrong input, please choose either 'Y' or 'N'.\n";
      }
   } while (false == done);

   try {
      // Open session
      daq::ls::CoralItems::Session session(str(connectionString));

      // Table with the mapping of partition name and partition id.
      PartitionIDTable partitionIDsTable(session);
      long partID = partitionIDsTable.getPartID(release, partname);
      if (-1 == partID) {
         std::string warnStr("The TDAQ release " + release + " and partition name " + partname + " pair has no PARTITION_ID associated to it.");
         ers::warning(TableFailed(ERS_HERE, "fetch" , "PartitionID", warnStr.c_str()));
         return -1;
      }

      ERS_DEBUG(0, "Partition ID " << partID << " is associated to TDAQ release " << release <<
                   " and partition " << partname << ".");

      // Remove entries in the partition ID table. Removeing a row from this table triggers
      // a rule on the Oracle side to drop the Oracle segment associated to that PartID. It
      // is therefore not needed to explicitly remove the messages in the LOG_MESSAGES table.
      coral::AttributeList conditionDataPartID;
      std::string conditionPartID;
      partitionIDsTable.parseCondition(partID, "", "", conditionDataPartID, conditionPartID);
      partitionIDsTable.removeRow(conditionDataPartID, conditionPartID);

      // Remove entries in the metadata table
      LogMetadataTable metaTable(session);
      coral::AttributeList conditionDataMetadata;
      std::string conditionMetadata;
      metaTable.parseCondition(partID, "", -1, conditionDataMetadata, conditionMetadata);
      metaTable.removeRow(conditionDataMetadata, conditionMetadata);
   }
   catch (daq::ls::Exception& issue) {
      ers::fatal(issue);
   }
   catch (...) {
      ERS_DEBUG(0, "Unknown error thrown.");
   }

   return 0;
}
