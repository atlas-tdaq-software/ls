/*
 *  logSelect.cc
 *  logService
 *
 *  Created by Raul Murillo-Garcia on 26.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#include <limits.h>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <ers/ers.h>

#include "ls/lib/LogMsgsTable.h"
#include "ls/lib/LogMetadataTable.h"
#include "ls/lib/PartitionIDTable.h"
#include "ls/db/CoralSession.h"
#include "ls/Exceptions.h"

using namespace daq::ls;


inline const std::string  str(const char * word)
{
   return word ? word : "";
}


int main(int argc, char ** argv)
{
   // Do all the IPC initialization
   try {
      IPCCore::init(argc, argv);
   }
   catch (daq::ipc::CannotInitialize& issue) {
      ers::error(issue);
      exit(-1);
   }
   catch (daq::ipc::AlreadyInitialized& issue) {
      ers::warning(issue);
   }

   // Declare arguments
   CmdArgStr  connectionString('c', "connectionString", "connect-string", "Database connection string.", CmdArg::isREQ);

   // Declare command object and its argument-iterator
   CmdLine cmd(*argv, &connectionString, NULL);
   cmd.description("Application to retrieve the list of partition names.");

   // Parse arguments
   CmdArgvIter argvIter(--argc, ++argv);
   if ( cmd.parse(argvIter) ) {
      ers::fatal(ParseFailed(ERS_HERE, ""));
      return EXIT_FAILURE;
   }

   try {
      // Open session
      daq::ls::CoralItems::Session session(str(connectionString));

      // Get partitions from the PartitionID table
      PartitionIDTable partitionIDsTable(session);

      coral::AttributeList conditionData;
      std::string condition;

      partitionIDsTable.parseCondition(-1, "", "", conditionData, condition);
#ifndef ERS_NO_DEBUG
      int numPartitions =
#endif
      partitionIDsTable.select(conditionData, condition, "", "", "", 1000, 0);
      ERS_DEBUG(0, "Number of partitions dumped: " << numPartitions);
   }
   catch (daq::ls::Exception& issue) {
      ers::fatal(issue);
   }
   catch (...) {
      ERS_DEBUG(0, "Unknown error thrown.");
   }

   return 0;
}
