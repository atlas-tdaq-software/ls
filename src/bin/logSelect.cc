/*
 *  logSelect.cc
 *  logService
 *
 *  Created by Raul Murillo-Garcia on 26.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#include <limits.h>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <ers/ers.h>

#include "ls/lib/LogMsgsTable.h"
#include "ls/lib/LogMetadataTable.h"
#include "ls/lib/PartitionIDTable.h"
#include "ls/db/CoralSession.h"
#include "ls/Exceptions.h"

using namespace daq::ls;


inline const std::string  str(const char * word)
{
   return word ? word : "";
}


int main(int argc, char ** argv)
{
   // Do all the IPC initialization
   try {
      IPCCore::init(argc, argv);
   }
   catch (daq::ipc::CannotInitialize& issue) {
      ers::error(issue);
      exit(-1);
   }
   catch (daq::ipc::AlreadyInitialized& issue) {
      ers::warning(issue);
   }

   // Declare arguments
   CmdArgStr  connectionString('c', "connectionString", "connect-string", "Database connection string.", CmdArg::isREQ);
   CmdArgStr  tdaqRelease('t', "TDAQrelease", "TDAQ-release", "TDAQ release.", CmdArg::isREQ);
   CmdArgStr  partitionName('p', "partition", "partition-name", "Partition name.", CmdArg::isREQ);
   CmdArgStr  userName('u', "user", "user-name", "User name.", CmdArg::isOPT);
   CmdArgInt  runNumber('n', "runNumber", "run-number", "Run number.", CmdArg::isOPT);
   CmdArgStr  msgName('i', "messageName", "message-name", "Message name or ID.", CmdArg::isOPT);
   CmdArgStr  machName('m', "machineName", "machine-name", "Machine name where the message was issued.", CmdArg::isOPT);
   CmdArgStr  appName('a', "applicationName", "application-name", "Application name where the message was issued.", CmdArg::isOPT);
   CmdArgInt  timeLow('L', "timeLow", "time-low", "Lower time threshold (in UTC time).", CmdArg::isOPT);
   CmdArgInt  timeUp('U', "timeUp", "time-up", "Upper time threshold (in UTC time).", CmdArg::isOPT);
   CmdArgInt  severity('s', "severity", "severity", "Message severity: \n\t0 - DEBUG\n\t1 - LOG\n\t2 - INFORMATION\n\t3 - WARNING\n\t4 - ERROR\n\t5 - FATAL", CmdArg::isOPT);
   CmdArgStr  text('x', "text", "text", "Text in the message body.", CmdArg::isOPT);
   CmdArgStr  param('r', "parameters", "parameters", "Message parameters.", CmdArg::isOPT);
   CmdArgStr  optParam('o', "optionalParameters", "parameters", "Message optional parameters.", CmdArg::isOPT);
   CmdArgStr  qualifiers('q', "qualifiers", "qualifiers", "Message qualifiers.", CmdArg::isOPT);
   CmdArgStr  orderList('d', "orderList", "order-list", "Parameter to sort the messages by (MSG_ID, MACHINE_NAME, APPLICATION_NAME, ISSUED_WHEN, SEVERITY, MSG_TEXT, PARAMETERS, RUN_NUMBER).", CmdArg::isOPT);
   CmdArgInt  maxRows('e', "maxRows", "max-rows", "Maximum number of rows to retrieve from the database; 100 by default. If 0, all entries are retrieved.", CmdArg::isOPT);
   CmdArgInt  offsetRow('f', "offsetRow", "offset-row", "Offset in the table to retrieve the messages from.", CmdArg::isOPT);

   // Initialize arguments to appropiate default values
   tdaqRelease   = "";
   partitionName = std::getenv("TDAQ_PARTITION");
   userName      = "";
   runNumber     = -1;
   msgName       = "";
   machName      = "";
   appName       = "";
   timeLow       = -1;
   timeUp        = -1;
   severity      = -1;
   text          = "";
   param         = "";
   optParam      = "";
   qualifiers    = "";
   orderList     = "";
   maxRows       = 100;
   offsetRow     = 0;

   // Declare command object and its argument-iterator
   CmdLine cmd(*argv, &connectionString, &tdaqRelease, &partitionName, &userName, &runNumber,
               &msgName, &machName, &appName, &timeLow, &timeUp, &severity, &text, &param,
               &optParam, &qualifiers, &orderList, &maxRows, &offsetRow, NULL);
   cmd.description("Application to retrieve log messages for a given partition according to the command arguments. By default, messages are dumped on std::cout.");

   // Parse arguments
   CmdArgvIter argvIter(--argc, ++argv);
   if ( cmd.parse(argvIter) ) {
      ers::fatal(ParseFailed(ERS_HERE, ""));
      return EXIT_FAILURE;
   }

   try {
      // Open session
      daq::ls::CoralItems::Session session(str(connectionString));

      // Table with the mapping of partition name and partition id.
      std::string partname = str(partitionName);
      std::string release  = str(tdaqRelease);

      PartitionIDTable partitionIDsTable(session);
      long partID = partitionIDsTable.getPartID(release, partname);
      if (-1 == partID) {
         std::string warnStr("The TDAQ release " + release + " and partition name " + partname + " pair has no PARTITION_ID associated to it.");
         ers::warning(TableFailed(ERS_HERE, "read" , "PartitionID", warnStr.c_str()));
         return -1;
      }

      ERS_DEBUG(0, "Partition ID " << partID << " is associated to TDAQ release " << release <<
                   " and partition " << partname << ".");

      // Get messages from the log message table
      daq::ls::LogMsgsTable lsTable(session, partID, str(userName), -1);

      coral::AttributeList conditionData;
      std::string condition;

      lsTable.parseCondition(runNumber, str(machName), str(appName), timeLow, timeUp, severity,
                             str(msgName), str(text), str(param), str(optParam), str(qualifiers),
                             conditionData, condition);

      lsTable.select(conditionData, condition, "", str(orderList), "", maxRows, offsetRow);
   }
   catch (daq::ls::Exception& issue) {
      ers::fatal(issue);
   }
   catch (...) {
      ERS_DEBUG(0, "Unknown error thrown.");
   }

   return 0;
}
