/*
 *  logReciever.cc
 *  logService
 *
 *  Created by Raul Murillo-Garcia on 11.06.07
 *  Copyright 2007 CERN. All rights reserved.
 *
 */

#include <memory>
#include <regex>
#include <stdlib.h>
#include <pwd.h>
#include <signal.h>

#include <cmdl/cmdargs.h>

#include <ers/ers.h>
#include <ipc/core.h>
#include <is/infodictionary.h>

#include <pmg/pmg_initSync.h>

#include "ls/lib/Logger.h"
#include "ls/Exceptions.h"


inline const std::string  str(const char * word)
{
   return word ? word : "";
}

namespace daq {
   namespace ls {
      /**
       * \brief Handler for SIGTERM and SIGINT. Stops the logger.
       */
      void signalHandler(int signum);

      Logger* extlogger; // Instantiate log receiver
   }
}

void daq::ls::signalHandler(int )
{
   if (NULL != daq::ls::extlogger) {
       daq::ls::extlogger->stop();
   }
}


int main(int argc, char ** argv)
{
   daq::ls::extlogger = NULL;

   // Do all the IPC initialization
   try {
      IPCCore::init(argc, argv);
   }
   catch (daq::ipc::CannotInitialize& issue) {
      ers::error(issue);
      exit(-1);
   }
   catch (daq::ipc::AlreadyInitialized& issue) {
      ers::warning(issue);
   }

   // Declare arguments
   CmdArgStr  connectionString('c', "connectionString", "connection-string", "Database connection string.", CmdArg::isREQ);
   CmdArgStr  backupConnectString('b', "backupConnectionString", "backup-connection-string", "Backup database connection string.", CmdArg::isOPT);
   CmdArgStr  tdaqRelease('t', "TDAQrelease", "TDAQ-release", "TDAQ release.", CmdArg::isREQ);
   CmdArgStr  partitionName('p', "partition", "partition-name", "partition name.", CmdArg::isOPT);
   CmdArgStr  userName('u', "user", "user-name", "user name.", CmdArg::isOPT);
   CmdArgStr  ISserverName('n', "ISserverName", "IS-server-name", "Name of the Information Service to publish the message rate into.", CmdArg::isOPT);
   CmdArgStr  ERStransportName('e', "ERStransportName", "ERS-transport-name", "Name of the transport protocol for ERS.", CmdArg::isOPT);
   CmdArgInt  thresholdSize('s', "thresholdSize", "threshold-size", "Maximum number of log messages buffered before flushing them onto the database; 1000 by default.", CmdArg::isOPT);
   CmdArgStr  expr('S', "subscribe", "subscribe-expression", "ERS subscribe expression.", CmdArg::isOPT);
   CmdArgBool restarted('r', "restart", "Enable only if the logger is restarted.", CmdArg::isOPT);

   // Initialize arguments to appropiate default values
   backupConnectString = "sqlite://;schema=/tmp/sqlbkp.db;dbname=ATLAS_LOG_MESSG";
   if(std::getenv("TDAQ_PARTITION")) {
     partitionName = std::getenv("TDAQ_PARTITION");
   } else {
     partitionName = "initial";
   }
   if(std::getenv("TDAQ_VERSION")) {
     tdaqRelease = getenv("TDAQ_VERSION");
   } else {
     tdaqRelease = "nightly";
   }
   ISserverName  = "RunCtrlStatistics";
   ERStransportName = "mts";
   thresholdSize = 1000;
   expr          = "*";
   restarted     = false;

   // Declare command object and its argument-iterator
   CmdLine cmd(*argv, &tdaqRelease, &partitionName, &userName, &ISserverName, &ERStransportName, &thresholdSize,
                      &connectionString, &backupConnectString, &expr, &restarted, NULL);
   cmd.description("This application subscribes to the ERS service to receive and log on a database messages produced by TDAQ applications.");

   // Parse arguments
   CmdArgvIter argvIter(--argc, ++argv);
   if ( cmd.parse(argvIter) ) {
      daq::ls::ParseFailed issue(ERS_HERE, "");
      ers::fatal(issue);
      return EXIT_FAILURE;
   }

   // If user name is not defined get local, otherwise use 'unknown'
   if (userName.isNULL())  {
      ERS_DEBUG(1, "User name not provided in the parameter list. Retrieving user name with getpwuid().");
      struct passwd *psw = getpwuid( getuid() );
      if (psw != NULL)  {
         userName = psw->pw_name;
      }
      else  {
         ERS_DEBUG(1, "The user name could not be retrieved. 'unknown' will be used.");
         userName = "unknown";
      }
   }



   // Install signals handler
   struct sigaction saint;
   sigemptyset(&saint.sa_mask);
   sigaddset(&saint.sa_mask, SIGINT);
   sigaddset(&saint.sa_mask, SIGTERM);
   saint.sa_flags = 0;
   saint.sa_handler = daq::ls::signalHandler;
   sigaction(SIGINT, &saint, NULL);
   sigaction(SIGTERM, &saint, NULL);

   // Tell PMG we are up and running
   pmg_initSync();

   try {
      std::unique_ptr<daq::ls::Logger> logger(new daq::ls::Logger(str(tdaqRelease),
                                                                  str(partitionName),
                                                                  str(userName),
                                                                  str(ISserverName),
                                                                  str(ERStransportName),
                                                                  std::regex_replace(str(connectionString), std::regex("\\$\\(USER\\)"), str(userName)), // See ADTCC-245
                                                                  std::regex_replace(str(backupConnectString), std::regex("\\$\\(USER\\)"), str(userName)), // See ADTCC-245
                                                                  thresholdSize));
      daq::ls::extlogger = logger.get();
      logger->initialize(restarted);
      logger->run(str(expr));
   }
   catch (daq::ls::LoggerFailed& e) {
      ers::fatal(e);
   }

   ERS_DEBUG(0, "Exiting the log receiver.");

   return 0;
}
