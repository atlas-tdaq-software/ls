/*
 *  CoralTable.cc
 *  ls package
 *
 *  Created by Raul Murillo-Garcia on 8.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 *  Revisions:
 *
 */

#include "ls/db/CoralTable.h"
#include "ls/db/CoralTransaction.h"
#include "ls/db/CoralSession.h"

#include <memory>

#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/SchemaException.h>
#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/ISessionProxy.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ICursor.h>

#include <CoralBase/Attribute.h>

#include <ers/ers.h>

using namespace daq::ls::CoralItems;


Table::Table(Session &session, const std::string &name)
      : m_session(session),
        m_name(name)
{
   ;
}


Table::~Table()
{
   ;
}


unsigned int Table::removeRow(const coral::AttributeList& conditionData,
                              const std::string& condition)
{
   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table                 = schema.tableHandle(getName());
      coral::ITableDataEditor& tableEditor = table.dataEditor();

      // Remove rows
      unsigned int numRowsDeleted = tableEditor.deleteRows(condition, conditionData);
      ERS_DEBUG(0, "Number of rows removed from table '" << getName() << "': " << numRowsDeleted);

      // Commit transaction
      transaction.Commit();

      return numRowsDeleted;
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "delete" , getName().c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "delete" , getName().c_str(), "unknown");
   }
}


unsigned int Table::select(const coral::AttributeList& conditionData,
                           const std::string& condition,
                           const std::string& colList,
                           const std::string& orderList,
                           const std::string& groupBy,
                           unsigned int maxRows,
                           unsigned int offset,
                           std::ostream& stream)
{
   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table = schema.tableHandle(getName());

      // Get all messages from the table
      std::unique_ptr<coral::IQuery> query(table.newQuery());

      query->setRowCacheSize(100);
      query->setCondition(condition, conditionData);
      if (!colList.empty()) {
         query->addToOutputList(colList);
      }
      if (!orderList.empty()) {
         query->addToOrderList(orderList);
      }
      if (!groupBy.empty()) {
         query->groupBy(groupBy);
      }

      // If 0, do not set limits, just retrieve everything.
      if (maxRows > 0) {
         query->limitReturnedRows(maxRows, offset);
      }

      coral::ICursor& cursor = query->execute();
      unsigned int nRows = 0;
      while ( cursor.next() ) {
         stream << nRows << ": ";
         cursor.currentRow().toOutputStream( stream ) << std::endl;
         nRows++;
      }
      stream << "\n **** Number of rows output: " << nRows << " **** \n" << std::endl;

      // Commit transaction
      transaction.Commit();

      return nRows;
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "select" , getName().c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "select" , getName().c_str(), "unknown");
   }
}


unsigned int Table::select(std::ostream& stream)
{
   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table = schema.tableHandle(getName());

      // Get all messages from the table
      std::unique_ptr<coral::IQuery> query(table.newQuery());
      query->setRowCacheSize(100);
      coral::ICursor& cursor = query->execute();
      int nRows = 0;
      while ( cursor.next() ) {
         stream << nRows << ": ";
         cursor.currentRow().toOutputStream( stream ) << std::endl;
         nRows++;
      }
      stream << "\n **** Number of rows output: " << nRows << " **** \n" << std::endl;

      // Commit transaction
      transaction.Commit();

      return nRows;
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "select" , getName().c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "select" , getName().c_str(), "unknown");
   }
}


bool Table::exists()
{
   bool tableExists = false;
   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start the transaction
      transaction.Start();

      // Get table exists
      tableExists = schema.existsTable(m_name);

      // End transaction
      transaction.Commit();
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "exists" , m_name.c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "exists" , m_name.c_str(), "unknown");
   }

   return tableExists;
}


void Table::drop()
{
    try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start(true);

      // Drop table if it exists
      schema.dropIfExistsTable(m_name);

      // End transaction
      transaction.Commit();

      ERS_DEBUG(0, "Table " << m_name << "  successfully removed from database.");
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "drop" , m_name.c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "drop" , m_name.c_str(), "unknown");
   }
}


void Table::clean()
{
   try {

      coral::AttributeList conditionData;
      std::string condition;

#ifndef ERS_NO_DEBUG
      unsigned int numRowsDeleted =
#endif
      removeRow(conditionData, condition);

      ERS_DEBUG(0, "Number of rows removed: " << numRowsDeleted);
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "clean" , getName().c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "clean" , getName().c_str(), "unknown");
   }
}



double Table::size(const coral::AttributeList& conditionData,
                   const std::string& condition)
{
   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table = schema.tableHandle(getName());

      // Get the number of entries in a table
      std::unique_ptr<coral::IQuery> query(table.newQuery());
      query->addToOutputList("COUNT(*)");
      query->setCondition(condition, conditionData);

      coral::ICursor& cursor = query->execute();
      if (false == cursor.next()) {
         throw TableFailed(ERS_HERE, "size", getName().c_str(), " no result was returned when quering for the table size.");
      }

      const coral::AttributeList attrlist = cursor.currentRow();
      double nRows = attrlist["COUNT(*)"].data<double>();
      ERS_DEBUG(0, "Number of rows in table " << getName() << ": " << nRows);

      // Close cursor
      cursor.close();

      // Commit transaction
      transaction.Commit();

      return nRows;
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "size" , getName().c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "size" , getName().c_str(), "unknown");
   }
}


double Table::size()
{
   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table = schema.tableHandle(getName());

      // Get the number of entries in a table
      std::unique_ptr<coral::IQuery> query(table.newQuery());
      query->addToOutputList("COUNT(*)");
      coral::ICursor& cursor = query->execute();
      if (false == cursor.next()) {
         throw TableFailed(ERS_HERE, "size", getName().c_str(), " no result was returned when quering for the table size.");
      }

      const coral::AttributeList attrlist = cursor.currentRow();
      double nRows = attrlist["COUNT(*)"].data<double>();
      ERS_DEBUG(0, "Number of rows in table " << getName() << ": " << nRows);

      // Close cursor
      cursor.close();

      // Commit transaction
      transaction.Commit();

      return nRows;
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "size" , getName().c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "size" , getName().c_str(), "unknown");
   }
}


void Table::printSQL(const std::string& action,
                     const coral::AttributeList& conditionData,
                     const std::string& condition) const
{
   std::cout << action << " " << condition;
   for (coral::AttributeList::const_iterator iBindVariable = conditionData.begin(); iBindVariable != conditionData.end(); ++iBindVariable )
   {
       std::cout << iBindVariable->specification().name() << " = " << iBindVariable->data<std::string>() << std::endl;
   }
}

