/*
 *  CoralSession.cc
 *  ls package
 *
 *  Created by Raul Murillo-Garcia on 8.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 *  Revisions: 
 * 
 */

#include "ls/db/CoralSession.h"    

//#include "RelationalAccess/ConnectionService.h"

#include <CoralKernel/Context.h>
#include <RelationalAccess/ConnectionService.h>
#include <RelationalAccess/ISessionProxy.h>
#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/IConnectionServiceConfiguration.h>
#include <RelationalAccess/SchemaException.h>

#include <ers/ers.h>


using namespace daq::ls::CoralItems;

 
Session::Session(const std::string& connectionString, SessionMode mode)
        : m_proxy()
{   
   try {        
      coral::Context& context = coral::Context::instance();
   
      ERS_DEBUG(4, "Loading CORAL/Services/XMLAuthenticationService.");
      context.loadComponent( "CORAL/Services/XMLAuthenticationService" );
   
      ERS_DEBUG(4, "Loading CORAL/Services/RelationalService.");
      context.loadComponent( "CORAL/Services/RelationalService" );
   
      m_connectionService.reset(new coral::ConnectionService());
   
      ERS_DEBUG(3, "Connecting and opening new session.");
      m_proxy.reset(m_connectionService->connect(connectionString, static_cast<coral::AccessMode>(mode)));   
      if( !m_proxy.get() ) {
         std::string error = "could not connect to " + connectionString;
         throw SessionCreate(ERS_HERE, error.c_str());        
      }
   
      ERS_DEBUG(3, "Starting a new transaction.");
      m_proxy->transaction().start(mode == coral::ReadOnly);   
   }
   catch (coral::Exception &e) {
      std::string error = std::string("CORAL exceptions: ") + e.what(); 
      throw SessionCreate(ERS_HERE, error.c_str());
   }   
   catch (ers::Issue &e) {
      std::string error = std::string("General exception: ") + e.what();
      throw SessionCreate(ERS_HERE, error.c_str());
   }   
}


Session::~Session()
{    
   ;
}


coral::ISessionProxy* daq::ls::CoralItems::Session::getProxy()
{ 
   ERS_ASSERT(m_proxy.get());
   return m_proxy.get();
}        

