/*
 *  CoralTransaction.cc
 *  ls package
 *
 *  Created by Raul Murillo-Garcia on 17.06.07
 *  Copyright 2005 CERN. All rights reserved.
 *
 *  Revisions: 
 * 
 */

#include "ls/db/CoralTransaction.h"    
#include "ls/db/CoralSession.h"

#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/SchemaException.h>
#include <RelationalAccess/ISessionProxy.h>

#include <ers/ers.h>

using namespace daq::ls::CoralItems;


  
Transaction::Transaction(Session &session)
            : m_session(session)
{
   ;    
}

Transaction::~Transaction()
{
   try {
      // Commit if the transaction is still active;
      if (m_session.getProxy()->transaction().isActive()) {
         m_session.getProxy()->transaction().commit();
      }
   }
   catch (coral::Exception &e) {        
      ers::error(TransactionFailed( ERS_HERE, "commit", e.what() ));
   }   
}


void Transaction::Start(bool readOnly)
{
   try {
      m_session.getProxy()->transaction().start(readOnly);
   }
   catch (coral::Exception &e) {   
      throw TransactionFailed(ERS_HERE, "start", e.what());      
   }
}


void Transaction::Commit()
{
   try {
      m_session.getProxy()->transaction().commit();
   }
   catch (coral::Exception &e) {   
      throw TransactionFailed(ERS_HERE, "commit", e.what());      
   }
} 
 
bool Transaction::Active()
{
   try {
      return m_session.getProxy()->transaction().isActive();
   }
   catch (coral::Exception &e) {   
      throw TransactionFailed(ERS_HERE, "active", e.what());      
   }
}
