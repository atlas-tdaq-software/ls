//----------------------------------------------------------------------------------------
// Title         : Database access
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : PrimaryDatabase.cc
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 26/Mar/2012
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : PrimaryDatabase
// Description   : This class encapsulates the access to the primary database: ORACLE.
//----------------------------------------------------------------------------------------
// Copyright (c) 2012 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 26/Mar/2012: created.
//----------------------------------------------------------------------------------------

#include <string>
#include <time.h>

#include "ls/lib/PrimaryDatabase.h"
#include "ls/lib/LogBuffer.h"
#include "ls/lib/LogMsgsTable.h"
#include "ls/lib/LogMetadataTable.h"
#include "ls/lib/PartitionIDTable.h"
#include "ls/Exceptions.h"

using namespace daq::ls;


PrimaryDatabase::PrimaryDatabase(std::string const& conString,
                                 std::string const& tdaqRelease,
                                 std::string const& partitionName,
                                 std::string const& userName)
                : m_conString(conString),
                  m_tdaqRelease(tdaqRelease),
                  m_partitionName(partitionName),
                  m_userName(userName),
                  m_lastConnection(time(NULL))
{
   ;
}


bool PrimaryDatabase::initialize()
{
   try {
      m_session.reset(new CoralItems::Session(m_conString));
      ERS_DEBUG(0, "Primary database initialized.");
   }
   catch (SessionCreate& e) {
      ers::error(e);
      return false;
   }

   return true;
}


unsigned int PrimaryDatabase::getPartitionId() const
{
   PartitionIDTable partitionIDsTable(*m_session);
   // Might throw TableFailed.
   long partId = partitionIDsTable.getPartID(m_tdaqRelease, m_partitionName);

   if (-1 == partId) {
      // The TDAQ release/partition pair are not in the table. Add a new entry.
      // If two instantes of the logService running at the same time read the
      // same latest PartitionID value, they will attempt to write back the same
      // incremented-by-one PartitionID value. since the column ParititionID
      // in the PartitionIDs table is indexed, two entries with the same value
      // cannot occur, one of them will fail. To account for this, the function
      // attempts to insert a new entry in the PartitionID table three times.
      int tries = 3;
      while (tries--) {
         try {
            // Get the highest ID in the table
            partId = partitionIDsTable.getLastPartID();
            // If the table is empty start with ID = 0
            if (-1 == partId) {
               partId = 0;
            }  else {
               ++partId;
            }
            PartitionIDTable::PartitionIDRow row(partId, m_tdaqRelease, m_partitionName);
            partitionIDsTable.insertRow(row);
            // If no exception was thrown, the insert operation succeeded. Exit the loop.
            break;
         }
         catch (daq::ls::TableFailed& ex) {
            // If this is the third attempt throw exception.
            if (0 == tries) { throw;  }
         }
      }
   }

   // Reset timeout.
   m_lastConnection = time(NULL);

   return partId;
}


unsigned int PrimaryDatabase::getLastRunNumber(unsigned int partitionId) const
{
   unsigned int lastRunNumber = 0;

   // Check if there is no run-number associated with this partition ID and user name
   // NOTE!!!!! The table is not locked and therefore is not protected. If the same user runs the
   //           same partition at this moment the run-number may not be correct.
   LogMetadataTable metaTable(*m_session);
   coral::AttributeList conditionData;
   std::string condition;
   std::stringstream result;

   metaTable.parseCondition(partitionId, m_userName, -1, conditionData, condition);
   // Might throw TableFailed.
   unsigned int numRows = metaTable.select(conditionData, condition, "RUN_NUMBER", "RUN_NUMBER DESC", "", 1, 0, result);
   if (0 != numRows) {
      // Retrieve the latest run number.
      // Format:  0: [PARTITION_ID (long long) : 95], [USER_NAME (string) : rmurillo], [RUN_NUMBER (long long) : 0], [TIMESTAMP (long long) : 1217335552]
      std::string rows(result.str());
      std::string str1("[RUN_NUMBER (long long) : ");
      size_t pos1 = rows.find(str1);
      if (std::string::npos != pos1) {
         std::string str2("]");
         size_t pos2 = rows.find(str2, pos1);
         if (std::string::npos != pos2) {
            std::stringstream tmp;
            pos1 += str1.length();
            tmp << rows.substr(pos1, pos2-pos1);
            tmp >> lastRunNumber;
         }
      }
   }
   else {
      // Insert run number 0.
      insertNewRunNumber(partitionId, lastRunNumber);
   }

   // Reset timeout.
   m_lastConnection = time(NULL);

   return lastRunNumber;
}


unsigned int PrimaryDatabase::insertBuffer(LogBuffer& logBuffer, unsigned int partitionId)
{
   unsigned int logsInserted = 0;

   // Might throw TableFailed.
   LogMsgsTable logTable(*m_session, partitionId, m_userName, 0);
   logsInserted = logTable.insertRowBulk(logBuffer);

   ERS_DEBUG(2, "Messages inserted into the primary database: " << logsInserted << ".");

   // Reset timeout.
   m_lastConnection = time(NULL);

   return logsInserted;
}


void PrimaryDatabase::insertNewRunNumber(unsigned int partitionId, unsigned int runNumber) const
{
   // Might throw TableFailed
   LogMetadataTable metaTable(*m_session);
   // Make sure the run number is not already in the database. This could be the case
   // when moving messages from the backup databse to the primary database.
   coral::AttributeList conditionData;
   std::string condition;
   metaTable.parseCondition(partitionId, m_userName, runNumber, conditionData, condition);
   unsigned int numRows = metaTable.select(conditionData, condition, "", "", "", 0, 0);
   if (0 == numRows) {
      // Insert the new entry.
      LogMetadataTable::MetadataRow metadataRow(partitionId, m_userName, runNumber);
      metaTable.insertRow(metadataRow);

      ERS_DEBUG(0, "Inserted metadata information for Partition ID " << partitionId <<
                   " user name " << m_userName << " and run number " << runNumber << ".");
   }

   // Reset timeout.
   m_lastConnection = time(NULL);
}


void PrimaryDatabase::pingDatabase() const
{
   if ((time(NULL) - m_lastConnection) < ConnectionTimeout) {
      return;
   }

   ERS_LOG("Pinging the database to avoid idle timeout disconnection.\n");
   try {
      // The ping merely consists of quering the partitionID table to get the ID
      // for this partition.
      PartitionIDTable partitionIDsTable(*m_session);

      coral::AttributeList conditionData;
      std::string condition;

      partitionIDsTable.parseCondition(-1, m_tdaqRelease, m_partitionName, conditionData, condition);
      partitionIDsTable.select(conditionData, condition, "", "", "", 1, 0);

      // Update the last connection time.
      m_lastConnection = time(NULL);
   }
   catch (daq::ls::Exception& issue) {
      ers::error(issue);
      // @TODO what to do if this fails.
   }
}


