//----------------------------------------------------------------------------------------
// Title         : Logger agent
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : Logger.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 26/Mar/2102
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : Logger
// Description   : This class provides the interface to run the logger agent of the
//                 Log Service, that is, it subscribes to ERS and stores the messages to
//                 an ORACLE database via the CORAL interface. If this database is not
//                 accessible, it switches to a backup alternative: SQLite.
//----------------------------------------------------------------------------------------
// Copyright (c) 2012 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 26/Mar/2102: created.
//----------------------------------------------------------------------------------------

#include <time.h>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

#include <rc/RunParamsNamed.h>
#include <ipc/core.h>
#include <ipc/alarm.h>
#include <is/infodictionary.h>

#include "ls/lib/Logger.h"
#include "ls/lib/LogServiceInfoNamed.h"
#include "ls/Exceptions.h"

using namespace daq::ls;


Logger::Logger(std::string const& tdaqRelease,
               std::string const& partitionName,
               std::string const& userName,
               std::string const& isSrvName,
	       std::string const& ersTransportName,
               std::string const& primaryConString,
               std::string const& backupConString,
               unsigned int bufferSize)
       : m_db(primaryConString, tdaqRelease, partitionName, userName),
         m_bckpDb(backupConString, userName),
         m_tdaqRelease(tdaqRelease),
         m_partitionName(partitionName),
         m_partitionID(0),
         m_userName(userName),
         m_runNumber(0),
         m_logBuffer(bufferSize, userName),
         m_stopFlag(false),
         m_lastDbUpCheck(time(NULL)),
	 m_ersTransportName(ersTransportName)
{
   ERS_DEBUG(0, "Initializing log service for partition: " << partitionName);

   // Create partition.
   IPCPartition p(partitionName);

   // Initilize the IS object and add an entry to the IS repository
   try {
      m_ISinfo.reset(new LogServiceInfoNamed(p, isSrvName + ".LogServiceInfo"));
   }
   catch (daq::is::InvalidName& ex) {
      ers::warning(ex);
   }
}


void Logger::initialize(bool restarted)
{
   bool dbInit = m_db.initialize();
   bool bckpDbInit = m_bckpDb.initialize();

   if (!dbInit && !bckpDbInit) {
      // If neiher database could be initialized, there is nothing to be done.
      throw LoggerFailed(ERS_HERE, "Neither the primary nor the backup database could be initialized.");
   }

   // m_dbType will be overwritten if the primary database is correctly initialized.
   if (bckpDbInit) {
      m_dbType = DB_BACKUP;
   }

   if (dbInit) {
      try {
         m_partitionID = m_db.getPartitionId();
         m_runNumber   = m_db.getLastRunNumber(m_partitionID);

         ERS_DEBUG(0, "Partition ID " << m_partitionID << " is associated to TDAQ release " << m_tdaqRelease <<
                      " and partition " << m_partitionName << ".");
         ERS_DEBUG(0, "Last run number stored in the primary database associatd to partition ID " <<
                       m_partitionID << " is " << m_runNumber);

         // Check if there are messages to be restored from the backup database.
         if (bckpDbInit) {
            m_moveMessages(m_runNumber);
         }

         // If the logger has been restarted during StartOfRun, it may have missed the StartOfRun ERS
         // message and it may wrongly associate logs to the previous run number. To prevent this scenario,
         // first check the current run number on IS. If there is one and different to the latest recorded in
         // the Metadata table, update this table with the value in IS.
         if (restarted) {
            unsigned int isRunNumber = m_getRunNumberOnIs();
            if (isRunNumber != m_runNumber) {
               ERS_DEBUG(0, "Run number on IS (" << isRunNumber << ") differs from the latest recorded in the database: ("
                            << m_runNumber << ").");
               m_runNumber = isRunNumber;
               m_insertRunNumber();
            }
         }

         m_dbType = DB_PRIMARY;
      }
      catch (TableFailed& ex) {
         // Note backup database will be used.
         ers::error(ex);
      }
   }

   ERS_DEBUG(1, m_dbActive() << " database enabled.");

   // Initial IS publish
   m_publish();
}


void Logger::run(std::string const& expression)
{
    // Use the scope to terminate the periodic alarm before the ERS subscription is removed
    {
        // Set periodic task to do IS publishing and message collecting.
        IPCAlarm periodicAlarm(TaskPeriod, m_periodicTask, this);

        // Subscribe and start receiving ERS issues.
        try {
            ers::StreamManager::instance().add_receiver(m_ersTransportName, expression.c_str(), this);
        }
        catch(ers::Issue & e) {
            ers::fatal(e);
            m_stopFlag = true;
        }
        while(!m_stopFlag) {
            m_trigCond.wait();
            try {
                std::lock_guard<std::mutex> lk(m_mutex);

                int nmMsgLogged = (
                        DB_PRIMARY == m_dbType ?
                                m_db.insertBuffer(m_logBuffer, m_partitionID) :
                                m_bckpDb.insertBuffer(m_logBuffer));
                // Update stats
                m_stats.m_msgsLogged.inc(nmMsgLogged);
            }
            catch(TableFailed& e) {
                ers::error(e);
                if(DB_PRIMARY == m_dbType) {
                    ERS_LOG("Primary database failed. Switching to backup database.");
                    m_dbType = DB_BACKUP;
                } else {
                    // If writing in the backup table failed, exit.
                    throw LoggerFailed(ERS_HERE,
                                       "Primary database is not accessible and backup database failed to insert messages.");
                }
            }
        }
    }

    // Remove the ERS subscription
    ers::StreamManager::instance().remove_receiver(this);

    // Do one last publishing
    m_publish();
}


void Logger::stop()
{
   ERS_DEBUG(1, "Stopping the Logger.");
   m_stopFlag = true;
   m_trigCond.release();
}


void Logger::receive(ers::Issue const& issue)
{
   if (m_logBuffer.isFull()) {
      ers::error(LogBufferFailed(ERS_HERE, "Log buffer is full. A message has been discarded"));
      m_stats.m_bufferFull.set(true);
      ++m_stats.m_msgsDiscarded;
      // Store buffer in the database.
      m_trigCond.release();
      return;
   }

   // If this issue signals a new run number, it must be inserted in the metadata table.
   m_isNewRunNumber(issue);

   // Insert issue and update stats
   m_logBuffer.insert(issue, m_runNumber);
   m_stats.m_bufferFull.set(false);
   ++m_stats.m_msgsReceived;

   // If the threshold has been reached, trigger the dumping of the buffer in the database.
   if (m_logBuffer.size() >= m_logBuffer.threshold()) {
      m_trigCond.release();
   }
}


void Logger::restorePrimaryDb()
{
   ERS_LOG("Restoring the primary database.");
   try {
      // Get the partition ID
      m_partitionID = m_db.getPartitionId();
      unsigned int lastRunNumber = m_db.getLastRunNumber(m_partitionID);

      ERS_LOG("Partition ID " << m_partitionID << " is associated to TDAQ release " << m_tdaqRelease <<
              " and partition " << m_partitionName << ".");
      ERS_LOG("Last run number stored in the primary database associatd to partition ID " <<
              m_partitionID << " is " << lastRunNumber);

      // Move from the backup database to the primary database.
      m_moveMessages(lastRunNumber);

      // The last run number is always on IS. This value should be the same as the
      // last rn in the backup database. Otherwise, insert it.
      m_runNumber = m_getRunNumberOnIs();
      if (lastRunNumber != m_runNumber) {
         ERS_LOG("Run number on IS (" << m_runNumber << ") differs from the latest recorded in the database: ("
                 << lastRunNumber << ").");
         m_db.insertNewRunNumber(m_partitionID, m_runNumber);
      }

      ERS_LOG("Switching to primary database.");
      // Everything went well so we can finally switch to the primary database.
      m_dbType = DB_PRIMARY;
   }
   catch (TableFailed& ex) {
      // Note backup database will be used.
      ers::error(ex);
   }
}


void Logger::m_moveMessages(unsigned int lastRunNumber)
{
   // Get a list of run numbers in the backup database
   std::vector<unsigned int> runNumbers;
   m_bckpDb.getListRunNumbers(runNumbers);

   LogBuffer logBuffer(500, m_userName);

   for (unsigned int i = 0; i < runNumbers.size(); ++i) {
      // If not 0, this run number is not in the primary database yet.
      if (0 != runNumbers[i]) {
         m_db.insertNewRunNumber(m_partitionID, runNumbers[i]);
      }
      unsigned int const numMsgsToGet = 500;
      unsigned int numMsgsGot = 0;
      unsigned int numMsgsGotTotal = 0;
      do {
         numMsgsGot = m_bckpDb.getBulkSelect(runNumbers[i], numMsgsToGet, numMsgsGotTotal, logBuffer);
         numMsgsGotTotal += numMsgsGot;
         // If the messages are associated to rn = 0, overwrite it with the last run number in
         // the database. Note the backup database used rn = 0 when the primary database cannot
         // be accessed to retrieve the last run number.
         if (0 == runNumbers[i] && 0 != lastRunNumber) {
            for (unsigned int i = 0; i < logBuffer.size(); ++i) {
               logBuffer.at(i).m_runnumber = lastRunNumber;
            }
         }

         // Move the messages to the primary database
         m_db.insertBuffer(logBuffer, m_partitionID);

      } while (numMsgsGot == numMsgsToGet);

      ERS_DEBUG(0, "For run number " << runNumbers[i] << ": " << numMsgsGotTotal << " messages moved from the backup dabase to the primary database.");

      // Remove all messages for that run number.
      m_bckpDb.removeRunNumberMsgs(runNumbers[i]);
   }
}


unsigned int Logger::m_getRunNumberOnIs() const
{
   // In case an error occurs, default isRunNumber to the current run number.
   unsigned int isRunNumber = m_runNumber;

   try {
      RunParamsNamed runParams(m_partitionName, "RunParams.RunParams");
      runParams.checkout();
      isRunNumber = runParams.run_number;
   }
   catch (daq::is::Exception& issue) {
      ers::error(LoggerFailed(ERS_HERE, "The run number could not be retrieved from the IS." \
                                        "The logs will be associated to Run Number 0", issue));
   }

   return isRunNumber;
}


void Logger::m_isNewRunNumber(ers::Issue const& issue)
{
   if (0 == std::string(issue.get_class_name()).compare("rc::StartOfRun")) {
      std::stringstream tmp;
      tmp << issue.what();
      unsigned int runNumber;
      tmp >> runNumber;
      if (runNumber != m_runNumber) {
         m_runNumber = runNumber;
         ERS_DEBUG(1, "New run number received: " << m_runNumber << ".");
         // Only add a new entry with the run number if the primary database is active
         if (DB_PRIMARY == m_dbType) {
            // Spawn a thread to insert the new run number to return from receive() asap.
            boost::thread rnThread(boost::bind(&Logger::m_insertRunNumber, this));
         }
      }
   }
}


void Logger::m_insertRunNumber() const
{
   try {
      std::lock_guard<std::mutex> lk(m_mutex);

      m_db.insertNewRunNumber(m_partitionID, m_runNumber);
      ERS_DEBUG(1, "New run number inserted into the database: " << m_runNumber << ".");
   }
   catch (daq::ls::TableFailed& ex) {
      std::stringstream tmp;
      tmp << "Fail to insert run number " << m_runNumber << " in the database. Messages will not be accessible via the Log Manager";
      ers::fatal(LoggerFailed(ERS_HERE, tmp.str().c_str(), ex));
   }
}


bool Logger::m_periodicTask(void *parameter)
{
   Logger *that = static_cast<Logger*>(parameter);
   static unsigned long oldValue = 0;

   // Update the insert rate
   that->m_stats.m_insertRate.set((that->m_stats.m_msgsLogged.get() - oldValue) / TaskPeriod);
   oldValue = that->m_stats.m_msgsLogged.get();

   // Update the IS info
   that->m_publish();

   // Message collector: if there are messages in the buffer, log them.
   if (that->m_logBuffer.size() > 0)  {
      that->m_trigCond.release();
   }

   // If primary database is active, check if it needs to be pinged.
   if (DB_PRIMARY == that->m_dbType) {
      std::lock_guard<std::mutex> lk(that->m_mutex);

      that->m_db.pingDatabase();
   }
   else {
      // Check if the primary DB is accessible. Do this every 5 minutes to avoid flooding the system with errors.
      if ((time(NULL) - that->m_lastDbUpCheck) > IsPrimaryDbUpPeriod) {
         std::lock_guard<std::mutex> lk(that->m_mutex);

         that->m_lastDbUpCheck = time(NULL);
         if (that->m_db.initialize()) {
            ERS_LOG("Primary database is now accessible.");
            that->restorePrimaryDb();
         }
      }
   }

   return true;
}


void Logger::m_publish() const
{
   ERS_DEBUG(2, "Publishing Log Service info on IS.");

   m_ISinfo->msgsReceived  =  m_stats.m_msgsReceived.get();
   m_ISinfo->msgsLogged    =  m_stats.m_msgsLogged.get();
   m_ISinfo->insertRate    =  m_stats.m_insertRate.get();
   m_ISinfo->msgsDiscarded =  m_stats.m_msgsDiscarded.get();
   m_ISinfo->bufferFull    =  m_stats.m_bufferFull.get();

   try {
      m_ISinfo->checkin();
   }
   catch (daq::is::Exception& issue) {
      ers::warning(issue);
   }
}
