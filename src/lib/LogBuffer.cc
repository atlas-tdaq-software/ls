//----------------------------------------------------------------------------------------
// Title         : Log Buffer
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogBuffer.cc
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 12/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogBuffer
// Description   : This class encapsulates a buffer for the Log Messages. It's maximum
//                 capcaity is given as an argument.
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 12/Jun/2007: created.
//----------------------------------------------------------------------------------------

#include <sstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/locks.hpp>

#include <ers/Context.h>

#include "ls/lib/LogBuffer.h"

using namespace daq::ls;


LogBuffer::LogBuffer(unsigned int size, std::string const& userName)
          : m_buffer(size * CapacityRatio),
            m_threshold(size),
            m_userName(userName)
{
   ;
}

LogBuffer::LogBuffer()
          : m_buffer(1000 * CapacityRatio),
            m_threshold(1000),
            m_userName("")
{
   ;
}


void LogBuffer::insert(ers::Issue const& issue, long runNumber)
{
   std::string messageId(issue.get_class_name());
   // Silently drop ill-formed messages.
   if (true == messageId.empty()) {
      return;
   }
   // Make sure there is enough space in the buffer.
   // The callee already makes this check.
   // if (isFull()) {
   //   ers::error(LogBufferFailed(ERS_HERE, "Log buffer is full. A message has been discarded"));
   //   return;
   // }

   LogMsgsTable::LogMessage* msg = new LogMsgsTable::LogMessage();

   ers::Context const& issueContext = issue.context();
   // Parameters
   std::stringstream params;
   params << "PACKAGE_NAME: "    << issueContext.package_name()
          << ". FILE_NAME: "     << issueContext.file_name()
          << ". FUNCTION_NAME: " << issueContext.function_name()
          << ". LINE_NUMBER: "   << issueContext.line_number()
          << ". DATE_TIME: "     << issue.time_t() << ".";
   // Optional paramters
   std::stringstream optionalParams;
   const ers::string_map& issueOptionalParams = issue.parameters();
   ers::string_map::const_iterator it = issueOptionalParams.begin();
   for ( ; it != issueOptionalParams.end(); ++it) {
      optionalParams << it->first.c_str() << ": " << it->second.c_str() << ". ";
   }
   // Qualifiers
   std::stringstream qualifiers;
   std::vector<std::string> issueQual = issue.qualifiers();
   for (size_t i = 0; i < issueQual.size(); ++i)  {
      qualifiers << issueQual[i].c_str() << " ";
   }

   // Fill in the message
   msg->m_userName    = m_userName;
   msg->m_session     = 0;
   msg->m_runnumber   = runNumber;
   msg->m_id          = m_trimField(messageId);
   msg->m_machine     = m_trimField(issueContext.host_name());
   msg->m_application = m_trimField(issueContext.application_name());
   msg->m_dateTime    = issue.time_t();
   msg->m_severity    = issue.severity();
   msg->m_text        = m_trimField(issue.what());
   msg->m_parameters  = m_trimField(params.str());
   msg->m_optParam    = m_trimField(optionalParams.str());
   msg->m_qualifiers  = m_trimField(qualifiers.str());

   // Add chained messages if any.
   const ers::Issue* chainedIssue = issue.cause();
   while (chainedIssue) {
      m_buildChainedMessage(msg, chainedIssue);
      chainedIssue = chainedIssue->cause();
   }

   // Adding the element must be thread safe.
   {
      boost::mutex::scoped_lock scoped_lock(m_bufMutex);
      m_buffer.push_back(msg);
   }

   // 15 Dec 2011: no longer include this check.
   // if (msg->m_id.length()          == StrMaxLength || msg->m_machine.length()  == StrMaxLength ||
   //     msg->m_application.length() == StrMaxLength || msg->m_text.length()     == StrMaxLength ||
   //     msg->m_parameters.length()  == StrMaxLength || msg->m_optParam.length() == StrMaxLength ||
   //    msg->m_qualifiers.length()  == StrMaxLength)
   // {
   //    ERS_DEBUG(0, "Message had to be cropped because it was larger than maximum length allowed. User: " << msg->m_user <<
   //                 ". ID: " << msg->m_id << ". Machine: " << msg->m_machine << ". Application: " << msg->m_application <<
   //                 ". Text: " << msg->m_text << ". Parameters: " << msg->m_parameters << ". Optional parameters: " << msg->m_optParam <<
   //                 ". Qualifiers: " << msg->m_qualifiers <<".");
   // }
}


void LogBuffer::insert(LogMsgsTable::LogMessage* msg)
{
   boost::mutex::scoped_lock scoped_lock(m_bufMutex);
   // Make sure there is enough space in the buffer.
   if (m_buffer.size() == m_buffer.capacity()) {
      ers::error(LogBufferFailed(ERS_HERE, "Log buffer is full. Message has been discarded"));
      return;
   }

   m_buffer.push_back(msg);
}


LogMsgsTable::LogMessage const& LogBuffer::at(unsigned int indx) const
{
   boost::mutex::scoped_lock scoped_lock(m_bufMutex);
   // Check the m_buffer is not empty
   if (indx >= m_buffer.size()) {
      throw LogBufferFailed(ERS_HERE, "out of bound index");
   }

   return m_buffer[indx];
}


LogMsgsTable::LogMessage& LogBuffer::at(unsigned int indx)
{
   boost::mutex::scoped_lock scoped_lock(m_bufMutex);
   // Check the m_buffer is not empty
   if (indx >= m_buffer.size()) {
      throw LogBufferFailed(ERS_HERE, "out of bound index");
   }

   return m_buffer[indx];
}


void LogBuffer::swap(LogBuffer& other)
{
   boost::mutex::scoped_lock lk_1(m_bufMutex, boost::defer_lock);
   boost::mutex::scoped_lock lk_2(other.m_bufMutex, boost::defer_lock);
   boost::lock(lk_1, lk_2);

   m_buffer.swap(other.m_buffer);
}


void LogBuffer::transfer(LogBuffer& other)
{
   boost::mutex::scoped_lock lk_1(m_bufMutex, boost::defer_lock);
   boost::mutex::scoped_lock lk_2(other.m_bufMutex, boost::defer_lock);
   boost::lock(lk_1, lk_2);

   m_buffer.transfer(m_buffer.end(), other.m_buffer.begin(), other.m_buffer.end(), other.m_buffer);
}


size_t LogBuffer::size()
{
   boost::mutex::scoped_lock scoped_lock(m_bufMutex);
   return m_buffer.size();
}


bool daq::ls::LogBuffer::isFull()
{
   boost::mutex::scoped_lock scoped_lock(m_bufMutex);
   return (m_buffer.size() == m_buffer.capacity());
}


void LogBuffer::m_buildChainedMessage(LogMsgsTable::LogMessage* msg, ers::Issue const* issue) const
{
   // Each chained message is added as:
   // (Assume user name and sessionID are the same)
   // MsgStart: Name:[name]Host:[host]App:[application]Time:[date&time]
   //         Sev:[sev]Txt:[text]Param:[parameters \n Optional Paramters]MsgEnd
   // This should be easier to parse later.
   std::stringstream chainedMsg;
   ers::Context const& issueContext = issue->context();
   std::string messageId = issue->get_class_name();
   std::string hostName  = issueContext.host_name();
   std::string appName   = issueContext.application_name();
   std::string text      = issue->what();
   std::string sev       = ers::to_string(issue->severity());
   // Params
   std::stringstream params;
   params << "PACKAGE_NAME: " << issueContext.package_name()
          << ". FILE_NAME: " << issueContext.file_name()
          << ". FUNCTION_NAME: " << issueContext.function_name()
          << ". LINE_NUMBER: " << issueContext.line_number()
          << ". DATE_TIME: " << issue->time_t() << ".";
   // Optional parmeters
   std::stringstream optionalParams;
   const ers::string_map & issueOptionalParams = issue->parameters();
   ers::string_map::const_iterator it = issueOptionalParams.begin();
   for ( ; it != issueOptionalParams.end(); ++it) {
      optionalParams << it->first.c_str() << ": " << it->second.c_str() << ". ";
   }
   // Qualifiers
   std::stringstream qualifiers;
   std::vector<std::string> issueQual = issue->qualifiers();
   for (size_t i = 0; i < issueQual.size(); ++i)  {
      qualifiers << issueQual[i].c_str() << " ";
   }

   chainedMsg << "MsgStart: "
              << "ID:["    << messageId            << "]"
              << "Host:["  << hostName             << "]"
              << "App:["   << appName      << "]"
              << "Time:["  << issue->time_t()      << "]"
              << "Sev:["   << sev                  << "]"
              << "Txt:["   << text                 << "]"
              << "Param:[" << params.str()         << "]"
              << "Opt:["   << optionalParams.str() << "]"
              << "Qual:["  << qualifiers.str()     << "]EndMsg ";

   if ((chainedMsg.str().length() + msg->m_chainedMsgs.length()) < StrMaxLength) {
      msg->m_chainedMsgs += chainedMsg.str();
   } else {
      ERS_DEBUG(0, "Chained message not added to its parent log message due to field 'CHAINED_MSGS' overflow. Message: " << chainedMsg.str() << ".");
   }
}

std::string LogBuffer::userName() const {
    return m_userName;
}

std::string LogBuffer::m_trimField(std::string const& field) const
{
   if (field.length() < StrMaxLength) { return field; }
   return field.substr(0, StrMaxLength);
}
