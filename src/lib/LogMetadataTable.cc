//----------------------------------------------------------------------------------------
// Title         : Metadata table
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogMetadataTable.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 8/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogMetadataTable
// Description   : Class representing the Metadata table. This table is used to map the
//                 table name and the partition name. In addition, it stores the user that
//                 started the partition and the session value, that is, the data and time
//                 when the table was created. Only one table is kept and created if it
//                 does not exist when a partition is started.
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
//  8/Jun/2007: created.
//----------------------------------------------------------------------------------------

#include "ls/lib/LogMetadataTable.h"

#include <RelationalAccess/TableDescription.h>
#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/SchemaException.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IBulkOperation.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/ITablePrivilegeManager.h>
#include <RelationalAccess/IBulkOperation.h>
#include <RelationalAccess/ISessionProxy.h>

#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/TimeStamp.h>

#include <ers/ers.h>

using namespace daq::ls;


LogMetadataTable::LogMetadataTable(CoralItems::Session &session)
                 : CoralItems::Table(session, "METADATA")
{

}


void LogMetadataTable::create()
{
   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Create the Log Service table for the specific partition.
      coral::TableDescription tableDescription;
      tableDescription.setName(getName());

      // partitionID:long long:notnull
      tableDescription.insertColumn("PARTITION_ID", coral::AttributeSpecification::typeNameForId( typeid(long long) ) );
      tableDescription.setNotNullConstraint( "PARTITION_ID" );

      // userName:std::string:notnull
      tableDescription.insertColumn("USER_NAME", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "USER_NAME" );

      // runNumber:int:notnull
      tableDescription.insertColumn("RUN_NUMBER", coral::AttributeSpecification::typeNameForId( typeid(int) ) );
      tableDescription.setNotNullConstraint( "RUN_NUMBER" );

      // timestamp:coral::TimeStamp:notnull
      tableDescription.insertColumn("TIMESTAMP", coral::AttributeSpecification::typeNameForId( typeid(coral::TimeStamp) ) );
      tableDescription.setNotNullConstraint( "TIMESTAMP" );

      // Define primary key
      tableDescription.createIndex("PARTITION_INDEX", "PARTITION_ID", false);

      // Crate table
      coral::ITable& table = schema.createTable(tableDescription);

      // Set privileges: All users can Select and Insert.
      //                 Delete and Update is restricted to privileged users.
      table.privilegeManager().grantToPublic( coral::ITablePrivilegeManager::Select);
      table.privilegeManager().grantToPublic( coral::ITablePrivilegeManager::Insert);

      // Commit transaction
      transaction.Commit();
   }
   catch (coral::TableAlreadyExistingException &e) {
      ers::info(TableFailed(ERS_HERE, "create", getName().c_str(), e.what()));
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "create", getName().c_str(), e.what());
   }
   catch (daq::ls::Exception &e) {
      throw TableFailed(ERS_HERE, "create", getName().c_str(), "", e);
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "create" , getName().c_str(), "unknown");
   }
}


void LogMetadataTable::insertRow(CoralItems::Table::OneRow& oneRow)
{
   try {
      MetadataRow& newRow = static_cast<MetadataRow&>(oneRow);

      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table                 = schema.tableHandle(getName());
      coral::ITableDataEditor& tableEditor = table.dataEditor();

      coral::AttributeList rowBuffer;
      tableEditor.rowBuffer(rowBuffer);

      rowBuffer[0].data<long long>()   = newRow.m_tableID;
      rowBuffer[1].data<std::string>() = newRow.m_user;
      rowBuffer[2].data<long long>()   = newRow.m_runNumber;
      rowBuffer[3].data<long long>()   = time(NULL);

      // Insert row
      tableEditor.insertRow(rowBuffer);

      // Commit transaction
      transaction.Commit();
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "insert", getName().c_str(), e.what());
   }
   catch (daq::ls::Exception &e) {
      throw TableFailed(ERS_HERE, "insert", getName().c_str(), "", e);
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "insert" , getName().c_str(), "unknown");
   }
}


void LogMetadataTable::parseCondition(long long partitionID,
                                      const std::string& user,
                                      long long runNumber,
                                      coral::AttributeList& conditionData,
                                      std::string& condition)
{
   std::stringstream condBuffer;
   int indx = 0;

   if (-1 != partitionID) {
      condBuffer << "PARTITION_ID =: partitionID";
      conditionData.extend<long long> ("partitionID");
      conditionData[indx++].data<long long>() = partitionID;
   }
   if (!user.empty()) {
      if (condBuffer.str().size()) {
         condBuffer << " AND ";
      }
      condBuffer << "USER_NAME =: user";
      conditionData.extend<std::string>("user");
      conditionData[indx++].data<std::string>() = user;
   }
   if (-1 != runNumber) {
      if (condBuffer.str().size()) {
         condBuffer << " AND ";
      }
      condBuffer << "RUN_NUMBER =: runNumber";
      conditionData.extend<long long> ("runNumber");
      conditionData[indx++].data<long long>() = runNumber;
   }
   condition = condBuffer.str();
}


