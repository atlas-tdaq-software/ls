//----------------------------------------------------------------------------------------
// Title         : Log message table
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogMsgsTable.cc
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 8/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogMsgsTable
// Description   : Provides access to the Log Message. Access is always for a partition.
//                 A unique ID per message is not used when inserting them  in the database.
//                 The likelyhood of having two messages with exactly the same values in all
//                 the fields is very small. Nevertheless, if that case occurred there is no
//                 gain in having two identical entries.
// NOTE: the table cannot be created for the primary database (Oracle) since CORAL does not
//       provide the sufficient interface to define all aspects (Oracle paritioning, etc.).
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
//  8/Jun/2007: created.
//----------------------------------------------------------------------------------------

#include "ls/lib/LogMsgsTable.h"
#include "ls/lib/LogBuffer.h"

#include <memory>

#include <RelationalAccess/TableDescription.h>
#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/SchemaException.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/ITablePrivilegeManager.h>
#include <RelationalAccess/IBulkOperation.h>
#include <RelationalAccess/ISessionProxy.h>

#include <CoralBase/Attribute.h>
#include <CoralBase/TimeStamp.h>

#include <ers/ers.h>

using namespace daq::ls;


LogMsgsTable::LogMsgsTable(CoralItems::Session &session,
                           long partID,
                           std::string const& userName,
                           long sessionID)
             : CoralItems::Table(session, "LOG_MESSAGES"),
               m_partID(partID),
               m_userName(userName),
               m_session(sessionID)
{
      m_tableSchema.extend<long long>("PART_ID");
      m_tableSchema.extend<std::string>("USER_NAME");
      m_tableSchema.extend<long long>("SESSION_ID");
      m_tableSchema.extend<long long>("RUN_NUMBER");
      m_tableSchema.extend<std::string>("MSG_ID");
      m_tableSchema.extend<std::string>("MACHINE_NAME");
      m_tableSchema.extend<std::string>("APPLICATION_NAME");
      m_tableSchema.extend<long long>("ISSUED_WHEN");
      m_tableSchema.extend<short>("SEVERITY");
      m_tableSchema.extend<std::string>("MSG_TEXT");
      m_tableSchema.extend<std::string>("PARAM");
      m_tableSchema.extend<std::string>("CHAINED_MSGS");
      m_tableSchema.extend<coral::TimeStamp>("LOGGED_WHEN");
      m_tableSchema.extend<std::string>("OPT_PARAM");
      m_tableSchema.extend<std::string>("QUALIFIERS");
}


void LogMsgsTable::create()
{
   throw TableFailed(ERS_HERE, "create", getName().c_str(), "This table cannot be created via CORAL.");
}


void LogMsgsTable::insertRow(CoralItems::Table::OneRow& oneRow)
{
   try {
      LogMsgsTable::LogMessage& msg = static_cast<LogMsgsTable::LogMessage&>(oneRow);

      CoralItems::Transaction transaction(getSession());
      coral::ISchema& schema = getSession().getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get the table data editor handle
      coral::ITableDataEditor& tableEditor = schema.tableHandle(getName()).dataEditor();

      coral::AttributeList rowBuffer(m_tableSchema);

      rowBuffer[0].data<long long>()    = m_partID;
      rowBuffer[1].data<std::string>()  = msg.m_userName;
      rowBuffer[2].data<long long>()    = msg.m_session;
      rowBuffer[3].data<long long>()    = msg.m_runnumber;
      rowBuffer[4].data<std::string>()  = msg.m_id;
      rowBuffer[5].data<std::string>()  = msg.m_machine;
      rowBuffer[6].data<std::string>()  = msg.m_application;
      rowBuffer[7].data<long long>()    = msg.m_dateTime;
      rowBuffer[8].data<short>()        = msg.m_severity;
      rowBuffer[9].data<std::string>()  = msg.m_text;
      rowBuffer[10].data<std::string>() = msg.m_parameters;
      rowBuffer[11].data<std::string>() = msg.m_chainedMsgs;
      rowBuffer[12].data<coral::TimeStamp>() = coral::TimeStamp::now();
      rowBuffer[13].data<std::string>() = msg.m_optParam;
      rowBuffer[14].data<std::string>() = msg.m_qualifiers;

      // Insert row
      tableEditor.insertRow(rowBuffer);

      // Commit transaction
      transaction.Commit();
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "insert", getName().c_str(), e.what());
   }
   catch (daq::ls::Exception &e) {
      throw TableFailed(ERS_HERE, "insert", getName().c_str(), "", e);
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "insert" , getName().c_str(), "unknown");
   }
}


unsigned int LogMsgsTable::insertRowBulk(LogBuffer& logBuffer)
{
   CoralItems::Session& session = getSession();
   CoralItems::Transaction transaction(session);
   try {
      transaction.Start();
   }
   catch (daq::ls::TransactionFailed& ex) {
      throw TableFailed(ERS_HERE, "insert bulk", getName().c_str(), ex.what());
   }

   try {
       // Populate the attribute list object
       coral::AttributeList rowBuffer(m_tableSchema);
       coral::ISchema& schema = session.getProxy()->nominalSchema();
       coral::ITableDataEditor& tableEditor = schema.tableHandle(getName()).dataEditor();
       std::unique_ptr<coral::IBulkOperation> bulkInserter(tableEditor.bulkInsert(rowBuffer, logBuffer.threshold()));

       long long&        partID       = rowBuffer[0].data<long long>();
       std::string&      user         = rowBuffer[1].data<std::string>();
       long long&        sessionID    = rowBuffer[2].data<long long>();
       long long&        runnumber    = rowBuffer[3].data<long long>();
       std::string&      msgID        = rowBuffer[4].data<std::string>();
       std::string&      machine      = rowBuffer[5].data<std::string>();
       std::string&      application  = rowBuffer[6].data<std::string>();
       long long&        issuedWhen   = rowBuffer[7].data<long long>();
       short&            severity     = rowBuffer[8].data<short>();
       std::string&      text         = rowBuffer[9].data<std::string>();
       std::string&      parameters   = rowBuffer[10].data<std::string>();
       std::string&      chainedMsgs  = rowBuffer[11].data<std::string>();
       coral::TimeStamp& loggedWhen   = rowBuffer[12].data<coral::TimeStamp>();
       std::string&      optParam     = rowBuffer[13].data<std::string>();
       std::string&      qualifiers   = rowBuffer[14].data<std::string>();

       // Make this operation transactional: either the entire buffer is inserted
       // into the database, or nothing is. Since this is a ptr_vector container,
       // if the buffer is correctly inserted into the database, the LogMessage
       // objects will be automatically deleted and memory freed when the copyBuffer
       // object is out of scope.
       LogBuffer copyBuffer(logBuffer.threshold(), logBuffer.userName());
       copyBuffer.swap(logBuffer);
       unsigned int numBuffered = copyBuffer.size();

       try {
          for (unsigned int i = 0; i < numBuffered; ++i) {
             // No need to check for exception. The index is always within bounds.
             LogMsgsTable::LogMessage const& msg = copyBuffer.at(i);

             partID      = m_partID;
             user        = msg.m_userName;
             sessionID   = msg.m_session;
             runnumber   = msg.m_runnumber;
             msgID       = msg.m_id;
             machine     = msg.m_machine;
             application = msg.m_application;
             issuedWhen  = msg.m_dateTime;
             severity    = msg.m_severity;
             text        = msg.m_text;
             parameters  = msg.m_parameters;
             chainedMsgs = msg.m_chainedMsgs;
             loggedWhen  = coral::TimeStamp::now();
             optParam    = msg.m_optParam;
             qualifiers  = msg.m_qualifiers;

             bulkInserter->processNextIteration();
          }

          bulkInserter->flush();
       }
       catch (coral::Exception& ex) {
          // Roll back the buffer. This will rarely be needed.
          logBuffer.transfer(copyBuffer);
          throw TableFailed(ERS_HERE, "insert bulk", getName().c_str(), ex.what());
       }

       return numBuffered;
   }
   catch (TableFailed& e) {
       throw;
   }
   catch (coral::Exception &e) {
       throw TableFailed(ERS_HERE, "insert bulk", getName().c_str(), e.what());
   }
   catch (daq::ls::Exception &e) {
       throw TableFailed(ERS_HERE, "insert bulk", getName().c_str(), "", e);
   }
   catch (...) {
       throw TableFailed(ERS_HERE, "insert bulk" , getName().c_str(), "unknown");
   }
}


void LogMsgsTable::parseCondition(long runnumber,
                                  const std::string& machine,
                                  const std::string& app,
                                  long whenLow,
                                  long whenUp,
                                  short severity,
                                  const std::string& msgID,
                                  const std::string& msgTxt,
                                  const std::string& msgParam,
                                  const std::string& msgOptParam,
                                  const std::string& msgQual,
                                  coral::AttributeList& conditionData,
                                  std::string& condition)
{
      std::stringstream condBuffer;
      int indx = 0;

      // Always add the partition ID
      condBuffer << "PART_ID =: partID";
      conditionData.extend<long long> ("partID");
      conditionData[indx++].data<long long>() = m_partID;

      // Wrap values with wildcards to ease the user's search
      if(!m_userName.empty()) {
         condBuffer << " AND USER_NAME =: user";
         conditionData.extend<std::string> ("user");
         conditionData[indx++].data<std::string>() = m_userName;
      }
      if(-1 != m_session) {
         condBuffer << " AND SESSION_ID =: session";
         conditionData.extend<long long> ("session");
         conditionData[indx++].data<long long>() = m_session;
      }
      if(-1 != runnumber) {
         condBuffer << " AND RUN_NUMBER =: rn";
         conditionData.extend<long long> ("rn");
         conditionData[indx++].data<long long>() = runnumber;
      }
      if(!machine.empty()) {
         condBuffer << " AND MACHINE_NAME like: machine";
         conditionData.extend<std::string> ("machine");
         conditionData[indx++].data<std::string>() = "%"+machine+"%";
      }
      if(!app.empty()) {
         condBuffer << " AND APPLICATION_NAME like: app";
         conditionData.extend<std::string> ("app");
         conditionData[indx++].data<std::string>() = "%"+app+"%";
      }
      if(-1 != whenLow) {
         condBuffer << " AND ISSUED_WHEN >: whenLow";
         conditionData.extend<long long> ("whenLow");
         conditionData[indx++].data<long long>() = whenLow;
      }
      if(-1 != whenUp) {
         condBuffer << " AND ISSUED_WHEN <: whenUp";
         conditionData.extend<long long> ("whenUp");
         conditionData[indx++].data<long long>() = whenUp;
      }
      if(-1 != severity) {
         condBuffer << " AND SEVERITY =: severity";
         conditionData.extend<short> ("severity");
         conditionData[indx++].data<short>() = severity;
      }
      if(!msgID.empty()) {
         condBuffer << " AND MSG_ID like: id";
         conditionData.extend<std::string> ("id");
         conditionData[indx++].data<std::string>() = "%"+msgID+"%";
      }
      if(!msgTxt.empty()) {
         condBuffer << " AND MSG_TEXT like: txt";
         conditionData.extend<std::string> ("txt");
         conditionData[indx++].data<std::string>() = "%"+msgTxt+"%";
      }
      if(!msgParam.empty()) {
         condBuffer << " AND PARAM like: param";
         conditionData.extend<std::string> ("param");
         conditionData[indx++].data<std::string>() = "%"+msgParam+"%";
      }
      if(!msgOptParam.empty()) {
         condBuffer << " AND OPT_PARAM like: optParam";
         conditionData.extend<std::string> ("optParam");
         conditionData[indx++].data<std::string>() = "%"+msgOptParam+"%";
      }
      if(!msgQual.empty()) {
         condBuffer << " AND QUALIFIERS like: qualifier";
         conditionData.extend<std::string> ("qualifier");
         conditionData[indx++].data<std::string>() = "%"+msgQual+"%";
      }

      condition = condBuffer.str();
}

void LogMsgsTable::LogMessage::dump(std::ostream& stream) const
{
   stream << "User name:     " << m_userName    << std::endl;
   stream << "Session:       " << m_session     << std::endl;
   stream << "Run number:    " << m_runnumber   << std::endl;
   stream << "Message ID:    " << m_id          << std::endl;
   stream << "Machine:       " << m_machine     << std::endl;
   stream << "Application:   " << m_application << std::endl;
   stream << "Time:          " << m_dateTime    << std::endl;
   stream << "Severity:      " << m_severity    << std::endl;
   stream << "Text:          " << m_text        << std::endl;
   stream << "Params:        " << m_parameters  << std::endl;
   stream << "Opt params:    " << m_optParam    << std::endl;
   stream << "Qualifiers:    " << m_qualifiers  << std::endl;
   stream << "Chained messg: " << m_chainedMsgs << std::endl << std::endl;
}
