//----------------------------------------------------------------------------------------
// Title         : Log message table
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogMsgsBackupTable.cc
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 8/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogMsgsBackupTable
// Description   : Provides access to the Log Message. Access is always for a partition.
//                 A unique ID per message is not used when inserting them  in the database.
//                 The likelyhood of having two messages with exactly the same values in all
//                 the fields is very small. Nevertheless, if that case occurred there is no
//                 gain in having two identical entries.
// NOTE: the table cannot be created for the primary database (Oracle) since CORAL does not
//       provide the sufficient interface to define all aspects (Oracle paritioning, etc.).
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
//  8/Jun/2007: created.
//----------------------------------------------------------------------------------------

#include "ls/lib/LogMsgsBackupTable.h"
#include "ls/lib/LogMsgsTable.h"
#include "ls/lib/LogBuffer.h"

#include <memory>

#include <RelationalAccess/TableDescription.h>
#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/SchemaException.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/ITablePrivilegeManager.h>
#include <RelationalAccess/IBulkOperation.h>
#include <RelationalAccess/ISessionProxy.h>

#include <CoralBase/Attribute.h>
#include <CoralBase/TimeStamp.h>

#include <ers/ers.h>

using namespace daq::ls;


LogMsgsBackupTable::LogMsgsBackupTable(CoralItems::Session &session,
                                       std::string const& userName,
                                       long sessionID)
                   : CoralItems::Table(session, "LOG_MESSAGES"),
                     m_userName(userName),
                     m_session(sessionID)
{
      m_tableSchema.extend<std::string>("USER_NAME");
      m_tableSchema.extend<long long>("SESSION_ID");
      m_tableSchema.extend<long long>("RUN_NUMBER");
      m_tableSchema.extend<std::string>("MSG_ID");
      m_tableSchema.extend<std::string>("MACHINE_NAME");
      m_tableSchema.extend<std::string>("APPLICATION_NAME");
      m_tableSchema.extend<long long>("ISSUED_WHEN");
      m_tableSchema.extend<short>("SEVERITY");
      m_tableSchema.extend<std::string>("MSG_TEXT");
      m_tableSchema.extend<std::string>("PARAM");
      m_tableSchema.extend<std::string>("CHAINED_MSGS");
      m_tableSchema.extend<coral::TimeStamp>("LOGGED_WHEN");
      m_tableSchema.extend<std::string>("OPT_PARAM");
      m_tableSchema.extend<std::string>("QUALIFIERS");
}


void LogMsgsBackupTable::create()
{
   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Create the Log Service table for the specific partition.
      coral::TableDescription tableDescription;
      tableDescription.setName(getName());

      // userName:std::string:notnull
      tableDescription.insertColumn("USER_NAME", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "USER_NAME" );

      // sessionID:long long:notnull
      tableDescription.insertColumn("SESSION_ID", coral::AttributeSpecification::typeNameForId( typeid(long long) ) );
      tableDescription.setNotNullConstraint( "SESSION_ID" );

      // runNumber:int:notnull
      tableDescription.insertColumn("RUN_NUMBER", coral::AttributeSpecification::typeNameForId( typeid(long long) ) );
      tableDescription.setNotNullConstraint( "RUN_NUMBER" );

      // messageId:std::string:notnull
      tableDescription.insertColumn("MSG_ID", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "MSG_ID" );

      // machineName:std::string:notnull
      tableDescription.insertColumn("MACHINE_NAME", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "MACHINE_NAME" );

      // applicationName:std::string:notnull
      tableDescription.insertColumn("APPLICATION_NAME", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "APPLICATION_NAME" );

      // issuedWhen:long long:notnull
      tableDescription.insertColumn("ISSUED_WHEN", coral::AttributeSpecification::typeNameForId( typeid(long long) ) );
      tableDescription.setNotNullConstraint( "ISSUED_WHEN" );

      // severity:long long:notnull
      tableDescription.insertColumn("SEVERITY", coral::AttributeSpecification::typeNameForId( typeid(short) ) );
      tableDescription.setNotNullConstraint( "SEVERITY" );

      // messageText:std::string:notnull
      tableDescription.insertColumn("MSG_TEXT", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "MSG_TEXT" );

      // timestamp:coral::TimeStamp:notnull
      tableDescription.insertColumn("LOGGED_WHEN", coral::AttributeSpecification::typeNameForId( typeid(coral::TimeStamp) ) );
      tableDescription.setNotNullConstraint( "LOGGED_WHEN" );

      // params:std::string:notnull
      tableDescription.insertColumn("PARAM", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "PARAM" );

      // chaiendMessages:std::string:notnull
      tableDescription.insertColumn("CHAINED_MSGS", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "CHAINED_MSGS" );

      // optionalParmas:std::string:notnull
      tableDescription.insertColumn("OPT_PARAM", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "OPT_PARAM" );

      // qualifiers:std::string:notnull
      tableDescription.insertColumn("QUALIFIERS", coral::AttributeSpecification::typeNameForId( typeid(std::string) ) );
      tableDescription.setNotNullConstraint( "QUALIFIERS" );

      // Define primary key
      tableDescription.createIndex("MESSG_RUNNR_INDEX", "RUN_NUMBER", false);
      tableDescription.createIndex("MESSG_USR_INDEX", "USER_NAME", false);

      // Crate table
      coral::ITable& table = schema.createTable(tableDescription);

      // Set privileges: All users can Select and Insert.
      //                 Delete and Update is restricted to privileged users.
      table.privilegeManager().grantToPublic( coral::ITablePrivilegeManager::Select);
      table.privilegeManager().grantToPublic( coral::ITablePrivilegeManager::Insert);

      // Commit transaction
      transaction.Commit();
   }
   catch (coral::TableAlreadyExistingException &e) {
      // do nothing
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "create", getName().c_str(), e.what());
   }
   catch (daq::ls::Exception &e) {
      throw TableFailed(ERS_HERE, "create", getName().c_str(), "", e);
   }
}



void LogMsgsBackupTable::insertRow(CoralItems::Table::OneRow& oneRow)
{
   try {
      LogMsgsTable::LogMessage& msg = static_cast<LogMsgsTable::LogMessage&>(oneRow);

      CoralItems::Transaction transaction(getSession());
      coral::ISchema& schema = getSession().getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get the table data editor handle
      coral::ITableDataEditor& tableEditor = schema.tableHandle(getName()).dataEditor();

      coral::AttributeList rowBuffer(m_tableSchema);

      rowBuffer[0].data<std::string>()  = msg.m_userName;
      rowBuffer[1].data<long long>()    = msg.m_session;
      rowBuffer[2].data<long long>()    = msg.m_runnumber;
      rowBuffer[3].data<std::string>()  = msg.m_id;
      rowBuffer[4].data<std::string>()  = msg.m_machine;
      rowBuffer[5].data<std::string>()  = msg.m_application;
      rowBuffer[6].data<long long>()    = msg.m_dateTime;
      rowBuffer[7].data<short>()        = msg.m_severity;
      rowBuffer[8].data<std::string>()  = msg.m_text;
      rowBuffer[9].data<std::string>() = msg.m_parameters;
      rowBuffer[10].data<std::string>() = msg.m_chainedMsgs;
      rowBuffer[11].data<coral::TimeStamp>() = coral::TimeStamp::now();
      rowBuffer[12].data<std::string>() = msg.m_optParam;
      rowBuffer[13].data<std::string>() = msg.m_qualifiers;

      // Insert row
      tableEditor.insertRow(rowBuffer);

      // Commit transaction
      transaction.Commit();
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "insert", getName().c_str(), e.what());
   }
   catch (daq::ls::Exception &e) {
      throw TableFailed(ERS_HERE, "insert", getName().c_str(), "", e);
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "insert" , getName().c_str(), "unknown");
   }
}


unsigned int LogMsgsBackupTable::insertRowBulk(LogBuffer& logBuffer)
{
   CoralItems::Session& session = getSession();
   CoralItems::Transaction transaction(session);
   coral::ISchema& schema = session.getProxy()->nominalSchema();
   try {
      transaction.Start();
   }
   catch (daq::ls::TransactionFailed& ex) {
      throw TableFailed(ERS_HERE, "insert bulk", getName().c_str(), ex.what());
   }

   // Get table handle
   coral::ITable& table = schema.tableHandle(getName());
   coral::ITableDataEditor& tableEditor = table.dataEditor();

   // Populate the attribute list object
   coral::AttributeList rowBuffer(m_tableSchema);
   std::unique_ptr<coral::IBulkOperation> bulkInserter(tableEditor.bulkInsert(rowBuffer, logBuffer.threshold()));

   std::string&      user         = rowBuffer[0].data<std::string>();
   long long&        sessionID    = rowBuffer[1].data<long long>();
   long long&        runnumber    = rowBuffer[2].data<long long>();
   std::string&      msgID        = rowBuffer[3].data<std::string>();
   std::string&      machine      = rowBuffer[4].data<std::string>();
   std::string&      application  = rowBuffer[5].data<std::string>();
   long long&        issuedWhen   = rowBuffer[6].data<long long>();
   short&            severity     = rowBuffer[7].data<short>();
   std::string&      text         = rowBuffer[8].data<std::string>();
   std::string&      parameters   = rowBuffer[9].data<std::string>();
   std::string&      chainedMsgs  = rowBuffer[10].data<std::string>();
   coral::TimeStamp& loggedWhen   = rowBuffer[11].data<coral::TimeStamp>();
   std::string&      optParam     = rowBuffer[12].data<std::string>();
   std::string&      qualifiers   = rowBuffer[13].data<std::string>();

   // Make this operation transactional: either the entire buffer is inserted
   // into the database, or nothing is. Since this is a ptr_vector container,
   // if the buffer is correctly inserted into the database, the LogMessage
   // objects will be automatically deleted and memory freed when the copyBuffer
   // object is out of scope.
   LogBuffer copyBuffer(logBuffer.threshold(), logBuffer.userName());
   copyBuffer.swap(logBuffer);
   unsigned int numBuffered = copyBuffer.size();

   try {
      for (unsigned int i = 0; i < numBuffered; ++i) {
         // No need to check for exception. The index is always within bounds.
         LogMsgsTable::LogMessage const& msg = copyBuffer.at(i);

         user        = msg.m_userName;
         sessionID   = msg.m_session;
         runnumber   = msg.m_runnumber;
         msgID       = msg.m_id;
         machine     = msg.m_machine;
         application = msg.m_application;
         issuedWhen  = msg.m_dateTime;
         severity    = msg.m_severity;
         text        = msg.m_text;
         parameters  = msg.m_parameters;
         chainedMsgs = msg.m_chainedMsgs;
         loggedWhen  = coral::TimeStamp::now();
         optParam    = msg.m_optParam;
         qualifiers  = msg.m_qualifiers;

         bulkInserter->processNextIteration();
      }
      bulkInserter->flush();
   }
   catch (coral::Exception& ex) {
      // Roll back the buffer. This will rarely be needed.
      logBuffer.transfer(copyBuffer);
      throw TableFailed(ERS_HERE, "insert bulk", getName().c_str(), ex.what());
   }

   return numBuffered;
}


unsigned int LogMsgsBackupTable::selectForRunNumber(unsigned int runNumber,
                                                    unsigned int maxRows,
                                                    unsigned int offset,
                                                    LogBuffer& logBuffer)
{
   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table = schema.tableHandle(getName());

      // Get all messages from the table
      std::unique_ptr<coral::IQuery> query(table.newQuery());
      query->setRowCacheSize(100);

      coral::AttributeList variableList;
      variableList.extend("runNumber", typeid(long long) );
      variableList["runNumber"].data<long long>() = runNumber;
      query->setCondition("RUN_NUMBER = :runNumber", variableList);

      // If 0, do not set limits, just retrieve everything.
      if (maxRows > 0) {
         query->limitReturnedRows(maxRows, offset);
      }

      coral::ICursor& cursor = query->execute();
      unsigned int nRows = 0;
      while ( cursor.next() ) {
    	 LogMsgsTable::LogMessage* msg = new LogMsgsTable::LogMessage();

         const coral::AttributeList& row = cursor.currentRow();
         msg->m_userName    = row["USER_NAME"].data<std::string>();
         msg->m_session     = row["SESSION_ID"].data<long long>();
         msg->m_runnumber   = row["RUN_NUMBER"].data<long long>();;
         msg->m_id          = row["MSG_ID"].data<std::string>();
         msg->m_machine     = row["MACHINE_NAME"].data<std::string>();
         msg->m_application = row["APPLICATION_NAME"].data<std::string>();
         msg->m_dateTime    = row["ISSUED_WHEN"].data<long long>();
         msg->m_severity    = (ers::severity)row["SEVERITY"].data<short>();
         msg->m_text        = row["MSG_TEXT"].data<std::string>();
         msg->m_parameters  = row["PARAM"].data<std::string>();
         msg->m_chainedMsgs = row["CHAINED_MSGS"].data<std::string>();
         msg->m_optParam    = row["OPT_PARAM"].data<std::string>();
         msg->m_qualifiers  = row["QUALIFIERS"].data<std::string>();

         logBuffer.insert(msg);
         nRows++;
      }

      // Commit transaction
      transaction.Commit();

      return nRows;
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "select" , getName().c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "select" , getName().c_str(), "unknown");
   }
}


void LogMsgsBackupTable::parseCondition(long runnumber,
                                        const std::string& machine,
                                        const std::string& app,
                                        long whenLow,
                                        long whenUp,
                                        short severity,
                                        const std::string& msgID,
                                        const std::string& msgTxt,
                                        const std::string& msgParam,
                                        const std::string& msgOptParam,
                                        const std::string& msgQual,
                                        coral::AttributeList& conditionData,
                                        std::string& condition)
{
      std::stringstream condBuffer;
      int indx = 0;

      condBuffer << " USER_NAME = :user";
      conditionData.extend<std::string> ("user");
      conditionData[indx++].data<std::string>() = m_userName;

      // Wrap values with wildcards to ease the user's search
      if(-1 != m_session) {
         condBuffer << " AND SESSION_ID = :session";
         conditionData.extend<long long> ("session");
         conditionData[indx++].data<long long>() = m_session;
      }
      if(-1 != runnumber) {
         condBuffer << " AND RUN_NUMBER = :rn";
         conditionData.extend<long long> ("rn");
         conditionData[indx++].data<long long>() = runnumber;
      }
      if(!machine.empty()) {
         condBuffer << " AND MACHINE_NAME like :machine";
         conditionData.extend<std::string> ("machine");
         conditionData[indx++].data<std::string>() = "%"+machine+"%";
      }
      if(!app.empty()) {
         condBuffer << " AND APPLICATION_NAME like :app";
         conditionData.extend<std::string> ("app");
         conditionData[indx++].data<std::string>() = "%"+app+"%";
      }
      if(-1 != whenLow) {
         condBuffer << " AND ISSUED_WHEN > :whenLow";
         conditionData.extend<long long> ("whenLow");
         conditionData[indx++].data<long long>() = whenLow;
      }
      if(-1 != whenUp) {
         condBuffer << " AND ISSUED_WHEN < :whenUp";
         conditionData.extend<long long> ("whenUp");
         conditionData[indx++].data<long long>() = whenUp;
      }
      if(-1 != severity) {
         condBuffer << " AND SEVERITY = :severity";
         conditionData.extend<short> ("severity");
         conditionData[indx++].data<short>() = severity;
      }
      if(!msgID.empty()) {
         condBuffer << " AND MSG_ID like :id";
         conditionData.extend<std::string> ("id");
         conditionData[indx++].data<std::string>() = "%"+msgID+"%";
      }
      if(!msgTxt.empty()) {
         condBuffer << " AND MSG_TEXT like :txt";
         conditionData.extend<std::string> ("txt");
         conditionData[indx++].data<std::string>() = "%"+msgTxt+"%";
      }
      if(!msgParam.empty()) {
         condBuffer << " AND PARAM like :param";
         conditionData.extend<std::string> ("param");
         conditionData[indx++].data<std::string>() = "%"+msgParam+"%";
      }
      if(!msgOptParam.empty()) {
         condBuffer << " AND OPT_PARAM like :optParam";
         conditionData.extend<std::string> ("optParam");
         conditionData[indx++].data<std::string>() = "%"+msgOptParam+"%";
      }
      if(!msgQual.empty()) {
         condBuffer << " AND QUALIFIERS like :qualifier";
         conditionData.extend<std::string> ("qualifier");
         conditionData[indx++].data<std::string>() = "%"+msgQual+"%";
      }

      condition = condBuffer.str();
}
