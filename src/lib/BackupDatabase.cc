//----------------------------------------------------------------------------------------
// Title         : Database access
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : BackupDatabase.cc
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 26/Mar/2012
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : BackupDatabase
// Description   : This class encapsulates the access to the backup database: SQLite.
//----------------------------------------------------------------------------------------
// Copyright (c) 2012 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 26/Mar/2012: created.
//----------------------------------------------------------------------------------------

#include <string>

#include "ls/lib/BackupDatabase.h"
#include "ls/lib/LogBuffer.h"
#include "ls/lib/LogMsgsBackupTable.h"
#include "ls/Exceptions.h"

using namespace daq::ls;


BackupDatabase::BackupDatabase(std::string const& conString, std::string const& userName)
               : m_conString(conString),
                 m_userName(userName)
{
   ;
}


bool BackupDatabase::initialize()
{
   try {
      m_session.reset(new CoralItems::Session(m_conString));
      LogMsgsBackupTable logTable(*m_session, m_userName, 0);
      if (false == logTable.exists()) {
         logTable.create();
      }
      ERS_DEBUG(0, "Backup database initialized.");
   }
   catch (daq::ls::Exception& ex) {
      ers::error(ex);
      return false;
   }

   return true;
}


unsigned int BackupDatabase::insertBuffer(LogBuffer& logBuffer)
{
   unsigned int logsInserted = 0;
   // Create and open the session
   LogMsgsBackupTable logTable(*m_session, m_userName, 0);
   logsInserted = logTable.insertRowBulk(logBuffer);
   ERS_DEBUG(2, "Messages inserted into the backup database: " << logsInserted << ".");

   return logsInserted;
}


void BackupDatabase::getListRunNumbers(std::vector<unsigned int>& runNumbers) const
{
   LogMsgsBackupTable logTable(*m_session, m_userName, -1);
   coral::AttributeList conditionData;
   std::string condition;
   std::stringstream result;
   logTable.parseCondition(-1, "", "", -1, -1, -1, "", "", "", "" ,"", conditionData, condition);

   // Might throw TableFailed.
   int numRows = logTable.select(conditionData, condition, "RUN_NUMBER", "RUN_NUMBER ASC", "RUN_NUMBER", 0, 0, result);
   size_t pos1 = 0;
   for (int i = 0; i < numRows; ++i) {
      // Format:  0: [RUN_NUMBER (long long) : 35260]
      std::string rows(result.str());
      std::string str1("[RUN_NUMBER (long long) : ");
      pos1 = rows.find(str1, pos1);
      if (std::string::npos != pos1) {
         std::string str2("]");
         size_t pos2 = rows.find(str2, pos1);
         if (std::string::npos != pos2) {
            std::stringstream tmp;
            unsigned int rn;
            pos1 += str1.length();
            tmp << rows.substr(pos1, pos2-pos1);
            tmp >> rn;
            runNumbers.push_back(rn);
            ERS_DEBUG(3, "Run number retrieved from the backup database: " << rn << ".");
         }
      }
   }
}


unsigned int BackupDatabase::getBulkSelect(unsigned int runNumber,
                                           unsigned int numMsgs,
                                           unsigned int offset,
                                           LogBuffer& logBuffer) const
{
   LogMsgsBackupTable logTable(*m_session, m_userName, -1);
   // Might throw TableFailed.
   return logTable.selectForRunNumber(runNumber, numMsgs, offset, logBuffer);
}


void BackupDatabase::removeRunNumberMsgs(unsigned int runNumber) const
{
   LogMsgsBackupTable logTable(*m_session, m_userName, -1);
   coral::AttributeList conditionData;
   std::string condition;
   logTable.parseCondition(runNumber, "", "", -1, -1, -1, "", "", "", "" ,"", conditionData, condition);

   // Might throw TableFailed.
#ifndef ERS_NO_DEBUG
   unsigned int numMsgsDel =
#endif
   logTable.removeRow(conditionData, condition);
   ERS_DEBUG(2, "Messages removed from the backup database for run number " << runNumber << ": " << numMsgsDel << ".");
}

