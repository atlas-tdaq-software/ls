//----------------------------------------------------------------------------------------
// Title         : Partition ID table.
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : PartitionIDTable.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 20/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : PartitionIDTable
// Description   : Class to access the table mapping Partition Names with its associatied
//                 Partition ID.
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 31/Oct/2007: created.
//----------------------------------------------------------------------------------------

#include "ls/lib/PartitionIDTable.h"

#include <memory>

#include <RelationalAccess/TableDescription.h>
#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/SchemaException.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IBulkOperation.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/ITablePrivilegeManager.h>
#include <RelationalAccess/IBulkOperation.h>
#include <RelationalAccess/ISessionProxy.h>

#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/TimeStamp.h>

#include <ers/ers.h>

using namespace daq::ls;


PartitionIDTable::PartitionIDTable(CoralItems::Session &session)
                   : CoralItems::Table(session, "PARTITION_IDS")
{

}


void PartitionIDTable::create()
{
   throw TableFailed(ERS_HERE, "create", getName().c_str(), "This table cannot be created via CORAL.");
}


void PartitionIDTable::insertRow(CoralItems::Table::OneRow& oneRow)
{
   try {
      PartitionIDRow& newRow = static_cast<PartitionIDRow&>(oneRow);

      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table                 = schema.tableHandle(getName());
      coral::ITableDataEditor& tableEditor = table.dataEditor();

      coral::AttributeList rowBuffer;
      tableEditor.rowBuffer(rowBuffer);

      rowBuffer[0].data<long long>()   = newRow.m_tableID;
      rowBuffer[1].data<std::string>() = newRow.m_partitionName;
      rowBuffer[2].data<std::string>() = newRow.m_tdaqRelease;

      // Insert row
      tableEditor.insertRow(rowBuffer);

      // Commit transaction
      transaction.Commit();
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "insert", getName().c_str(), e.what());
   }
   catch (daq::ls::Exception &e) {
      throw TableFailed(ERS_HERE, "insert", getName().c_str(), "", e);
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "insert" , getName().c_str(), "unknown");
   }
}


void PartitionIDTable::parseCondition(long long partitionID,
                                      const std::string& tdaqRelease,
                                      const std::string& partitionName,
                                      coral::AttributeList& conditionData,
                                      std::string& condition)
{
   std::stringstream condBuffer;
   int indx = 0;

   if(-1 != partitionID) {
      condBuffer << "PARTITION_ID =: partitionID";
      conditionData.extend<long long> ("partitionID");
      conditionData[indx++].data<long long>() = partitionID;
   }
   if(!partitionName.empty()) {
      if (condBuffer.str().size()) {
         condBuffer << " AND ";
      }
      condBuffer << "PARTITION_NAME =: partitionName";
      conditionData.extend<std::string> ("partitionName");
      conditionData[indx++].data<std::string>() = partitionName;
   }
   if(!tdaqRelease.empty()) {
      if (condBuffer.str().size()) {
         condBuffer << " AND ";
      }
      condBuffer << "TDAQ_RELEASE =: tdaqRelease";
      conditionData.extend<std::string> ("tdaqRelease");
      conditionData[indx++].data<std::string>() = tdaqRelease;
   }

   condition = condBuffer.str();
}


long long PartitionIDTable::getPartID(const std::string& tdaqRelease,
                                      const std::string& partitionName)
{
   long long partID = -1;

   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table = schema.tableHandle(getName());

      // First check if the partition name and tdaq release pair is already in the table.
      std::stringstream condBuffer;
      coral::AttributeList conditionData;

      condBuffer << "TDAQ_RELEASE =: tdaqRelease AND ";
      conditionData.extend<std::string>("tdaqRelease");
      conditionData[0].data<std::string>() = tdaqRelease;

      condBuffer << "PARTITION_NAME =: partitionName";
      conditionData.extend<std::string>("partitionName");
      conditionData[1].data<std::string>() = partitionName;

      std::unique_ptr<coral::IQuery> query(table.newQuery());
      query->setRowCacheSize(10);
      query->setCondition(condBuffer.str(), conditionData);
      query->addToOrderList("PARTITION_ID");

      // Execute the SQL query
      coral::ICursor& cursor = query->execute();

      if (true ==cursor.next()) {
         const coral::AttributeList attrlist = cursor.currentRow();
         partID = attrlist["PARTITION_ID"].data<long long>();
      }
      // Close cursor
      cursor.close();

      // Commit transaction
      transaction.Commit();
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "getPartID" , getName().c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "getPartID" , getName().c_str(), "unknown");
   }

   return partID;
}


long long PartitionIDTable::getLastPartID()
{
   long long partID = -1;

   try {
      CoralItems::Session& session  = getSession();
      CoralItems::Transaction transaction(session);
      coral::ISchema& schema        = session.getProxy()->nominalSchema();

      // Start transaction
      transaction.Start();

      // Get table handle
      coral::ITable& table = schema.tableHandle(getName());

      // There is no entry with the given partition name.
      std::string condition = "";
      coral::AttributeList conditionData;

      std::unique_ptr<coral::IQuery> queryInsert(table.newQuery());
      queryInsert->setRowCacheSize(1);
      queryInsert->setCondition(condition, conditionData);
      queryInsert->addToOrderList("PARTITION_ID DESC");
      queryInsert->limitReturnedRows(1, 0);
      coral::ICursor& cursor = queryInsert->execute();
      if (true == cursor.next()) {
         const coral::AttributeList attrlist = cursor.currentRow();
         partID = attrlist["PARTITION_ID"].data<long long>();
      }
      // Close cursor
      cursor.close();

      // Commit transaction
      transaction.Commit();
   }
   catch (coral::Exception &e) {
      throw TableFailed(ERS_HERE, "getPartID" , getName().c_str(), e.what());
   }
   catch (...) {
      throw TableFailed(ERS_HERE, "getPartID" , getName().c_str(), "unknown");
   }

   return partID;
}

