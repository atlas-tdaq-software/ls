//----------------------------------------------------------------------------------------
// Title         : Trigger condition
// Project       : ATLAS, TDAQ Log Service
//----------------------------------------------------------------------------------------
// File          : LogTriggerCondition.h
// Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
// Created       : 20/Jun/2007
// Revision      : 7407 $
//----------------------------------------------------------------------------------------
// Namespace     : daq::ls
// Class         : LogTriggerCondition
// Description   : This class wraps around a boost::condition and is used to signal
//                 when the buffered has reached the logging threahold (500 by default)
//                 and it is time for the msgs to be copied onto the database..
//----------------------------------------------------------------------------------------
// Copyright (c) 2007 by University of California, Irvine. All rights reserved.
//----------------------------------------------------------------------------------------
// Modification history:
// 20/Jun/2007: created.
//----------------------------------------------------------------------------------------

#include "ls/lib/LogTriggerCondition.h"

#include <boost/thread/exceptions.hpp>

#include <ers/ers.h>

using namespace daq::ls;


void LogTriggerCondition::wait()
{
   try {
      boost::mutex::scoped_lock scoped_lock(m_logTrigMutex);
      m_logTrigger.wait(scoped_lock);
   }
   catch(boost::lock_error& e) {
      ERS_DEBUG(0, e.what());
   }
}


void LogTriggerCondition::release()
{
   boost::mutex::scoped_lock scoped_lock(m_logTrigMutex);
   m_logTrigger.notify_all();
}



