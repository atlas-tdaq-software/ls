# ls

## tdaq-09-02-01

-   Fixed issue with `sqlite` file permissions ([ADTCC-245](https://its.cern.ch/jira/browse/ADTCC-245)):

    The name of the user running the `logger` application can now be set in the connection string with a place-holder.
    As an example, here is a possible set of command line options:
    
    `-e mts -S app!=mts-sender* -t ${TDAQ_VERSION} -p ${TDAQ_PARTITION} -c ${LOGGER_CONNECT} -n RunParams -s 10000 -b sqlite_file:${TDAQ_LOGS_PATH}/Logger_BackupDb_$(USER).db`
         
    The $(USER) placeholder will be then replaced with the real user name.

