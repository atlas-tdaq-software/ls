package common.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//import common.db.preparedstatement.DBPreparedStatement;

//import util.logger.RCMSLogger;



/**
 * Abstract Class of DBConnector Implements the methods are shared by MySQL and
 * Oracle database.
 * 
 * @author Alexander Oh, Andrea Petrucci, Vincent Boyer
 */
public abstract class AbstractDBConnector implements DBConnectorIF 
{	
    /**
     * <code>url</code>: Holds URL of the database. Can be queried from
     * instance for debug purposes.
     */
    public String url = null;

    /**
     * <code>user</code>: User for connection to the database
     */
    public String user = null;

    /**
     * <code>passwd</code>: Password of dbUser for connection to the database
     */
    public String passwd = null;

    /**
     * <code>connection</code>: Holds connection to the database
     */
    protected Connection connection = null;

    /**
     * <code>logger</code>: Log4j logger
     */

    /**
	 * 
	 * Close the connectiom to the database
	 * 
	 */
    @Override
    public void closeConnection() throws DBConnectorException {
        try {
            //Check the database connection
            if (connection != null || connection.isClosed()) {
                connection.close();
                connection = null;
            }
        } catch (SQLException sqlExc) {
            throw new DBConnectorException("Cannot close the connection.",
                    sqlExc);
        }
        //logger.debug("Connection is closed.");
    }

    /**
	 * Return the Database connection.
	 * 
	 * @return Database connection.
	 */
    @Override
    public Connection getConnection() {
        return connection;
    }

    /**
	 * Returns the Passwd for the connection to the database.
	 * 
	 * @return Returns the Passwd.
	 */
    @Override
    public String getPasswd() {
        return passwd;
    }

    /**
	 * Returns the url of the database.
	 * 
	 * @return Returns the url of the database.
	 */
    @Override
    public String getUrl() {
        return url;
    }

    /**
	 * Returns the user for the connection to the database.
	 * 
	 * @return Returns the dbUser.
	 */
    @Override
    public String getUser() {
        return user;
    }

    /**
	 * Set the passwd for the connection to the database.
	 * 
	 * @param passwd
	 *            is the Passwd to set.
	 */
    @Override
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    /**
	 * Set the url of the database.
	 * 
	 * @param url
	 *            is the url to set.
	 */
    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    /**
	 * Set the user for the connection to the database.
	 * 
	 * @param user
	 *            is the user to set.
	 */
    @Override
    public void setUser(String user) {
        this.user = user;
    }

    /**
	 * Closes the ResultSet and the related Statement.
	 * 
	 * @param rs
	 *            is the ResultSet to close.
	 * @return always a null ResultSet
	 */
    @Override
    public ResultSet closeResultSet(ResultSet rs) {
        if (rs != null) {
            Statement stmt = null;
            try {
                stmt = rs.getStatement();
            } catch (SQLException sql) {
                //logger.warn("RS DB API: cannot get the Statement from ResultSet");
            }
            try {
                rs.close();
            } catch (SQLException sql) {
                //logger.warn("RS DB API: cannot close ResultSet");
            }
            rs = null;
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sql) {
                    //logger.warn("RS DB API: cannot close Statement");
                }
                stmt = null;
            }
        }
        return rs;
    }

    /**
	 * Closes the PreparedStatement.
	 * 
	 * @param preparedStatement is the PreparedStatement to close.
	 * @return
	 */
    @Override
    public PreparedStatement closePreparedStatement(PreparedStatement preparedStatement) {
    	
    	if(preparedStatement != null) {
    		try {
        		preparedStatement.close();
            } catch (SQLException sql) {
                //logger.warn("DB API: cannot close preparedStatement");
            }
            preparedStatement = null;
    	}
    	
    	return preparedStatement;
    }
    
    /**
	 * DBExecuteQuery wrap of the executeQuery to managed the connection problem
	 * with the DBMS.
	 * 
	 * @param sqlQuery
	 *            is Sql Query
	 * @return rs is the resulset of the query
	 * @throws DBConnectionException if a database access error occurs
	 */
	@Override
    public ResultSet DBExecuteQuery(String sqlQuery)
			throws DBConnectorException {
		// Debug
		//logger.debug("RSExecuteQuery: SQL: " + sqlQuery);

		ResultSet rs = null;
		Statement stmt = null;
		try {
			// Create the statement and execute the query
			stmt = connection.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt.executeQuery(sqlQuery);

		} catch (SQLException sqlEx1) {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e1) {
					//logger.warn("RS DB API: cannot close ResultSet");
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e2) {
					//logger.warn("RS DB API: cannot close Statement");
				}
				stmt = null;
			}
			try {
				// There are two cases:
				// 1) The mysql Server closed the connection with our
				// application
				// 2) The Sql Query is wrong
				// 
				// We try another time to connect with the MySql Server to
				// undestand which case we are.
				// If we have the same exception the problem isn't connect to DB
				// but it's wrong
				// the Sql Query.

				// Close and open new connection
				this.closeConnection();
				this.openConnection();

				// Create the statement and execute the query
				stmt = connection.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				rs = stmt.executeQuery(sqlQuery);

			} catch (DBConnectorException cce) {
				//logger.error("Cannot close and open the connection : " + cce);
				throw new DBConnectorException("Cannot close and open the connection.", cce);
			} catch (SQLException sqlEx2) {
				//logger.error("Cannot execute the query : " + sqlEx2);
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e1) {
						//logger.warn("RS DB API: cannot close ResultSet");
					}
					rs = null;
				}
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e2) {
						//logger.warn("RS DB API: cannot close Statement");
					}
					stmt = null;
				}
				throw new DBConnectorException("Cannot execute the query: "
						+ sqlQuery, sqlEx2);
			}
		}

		//logger.debug("Executed this query = " + sqlQuery);
		return rs;
	}

	/**
	 * DBExecute wrap of the execute to managed the connection problem
	 * with the DBMS.
	 * 
	 * @param sqlQuery
	 *            is Sql Query
	 * @return rs is the resulset of the query
	 * @throws DBConnectionException if a database access error occurs
	 */
	@Override
    public boolean DBExecute(String sqlQuery) throws DBConnectorException {
		// Debug
		//logger.debug("RSExecute: SQL: " + sqlQuery);
		boolean result = false;
		Statement stmt = null;
		try {
			// Create the statement and execute the query
			stmt = connection.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			result = stmt.execute(sqlQuery);

		} catch (SQLException sqlEx1) {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e2) {
					//logger.warn("RS DB API: cannot close Statement");
				}
				stmt = null;
			}
			try {
				// There are two cases:
				// 1) The mysql Server closed the connection with our
				// application
				// 2) The Sql Query is wrong
				// 
				// We try another time to connect with the MySql Server to
				// undestand which case we are.
				// If we have the same exception the problem isn't connect to DB
				// but it's wrong
				// the Sql Query.

				// Close and open new connection
				this.closeConnection();
				this.openConnection();

				// Create the statement and execute the query
				stmt = connection.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				result = stmt.execute(sqlQuery);

			} catch (DBConnectorException cce) {
				//logger.error("Cannot close and open the connection : " + cce);
				throw new DBConnectorException(
						"Cannot close and open the connection.", cce);
			} catch (SQLException sqlEx2) {
				//logger.error("Cannot execute the query : " + sqlEx2);
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e2) {
						//logger.warn("RS DB API: cannot close Statement");
					}
					stmt = null;
				}
				throw new DBConnectorException("Cannot execute the query: "
						+ sqlQuery, sqlEx2);
			}
		}

		//logger.debug("Executed this query = " + sqlQuery);
		return result;
	}

	/**
	 * DBExecuteUpdate wrap of the executeUpdate to managed the connection
	 * problem with the DBMS.
	 * 
	 * @param sqlUpdate
	 *            is Sql Query
	 * @throws DBConnectionException
	 */
	@Override
    public int DBExecuteUpdate(String sqlUpdate) throws DBConnectorException {
		int result;
		Statement stmt = null;
		try {
			// Create the statement and execute the query
			stmt = connection.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			result = stmt.executeUpdate(sqlUpdate);

		} catch (SQLException sqlEx1) {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e2) {
					//logger.warn("RS DB API: cannot close Statement");
				}
				stmt = null;
			}
			try {
				// There are two cases:
				// 1) The mysql Server closed the connection with our
				// application
				// 2) The Sql Udapte Query is wrong
				// 
				// We try another time to connect with the MySql Server to
				// undestand which case we are.
				// If we have the same exception the problem isn't connect to DB
				// but it's wrong
				// the Sql Update Query.

				// Close and open new connection
				this.closeConnection();
				this.openConnection();

				// Create the statement and execute the query
				stmt = connection.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				result = stmt.executeUpdate(sqlUpdate);
				
			} catch (DBConnectorException cce) {
				//logger.error("Cannot close and open the update connection : "
				//		+ cce);
				throw new DBConnectorException(
						"Cannot close and open the connection.", cce);
			} catch (SQLException sqlEx2) {
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e2) {
						//logger.warn("RS DB API: cannot close Statement");
					}
					stmt = null;
				}
				//logger.error("Cannot execute the update query : " + sqlEx2);
				throw new DBConnectorException(
						"Cannot execute the update query.", sqlEx2);
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e2) {
				//logger.warn("RS DB API: cannot close Statement");
			}
			stmt = null;
		}
		//logger.debug("Executed this update query = " + sqlUpdate);
		return result;
	}
    
    /**
	 * Get a PreparedStatement from a query.
	 * @param query 
	 * 			is the SQL query.
	 * @return PreparedStatement which represents the PreparedStatement.
	 * @throws DBConnectorException
	 */
	@Override
    public PreparedStatement getPreparedStatement(String query) throws DBConnectorException {
		PreparedStatement pstmt = null;
		
		try {
			pstmt = connection.prepareStatement(query);
		} catch (SQLException e) {
			
			// Clean the PreparedStatement
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e2) {
					//logger.warn("RS DB API: cannot close PreparedStatement");
				}
				pstmt = null;
			}
			
			// We try another time to connect with the Oracle Server to
			// undestand which case we are.

			try {
				// Close and open new connection
				try {
					this.closeConnection();
				} catch (Exception doNothing) {
					// forget it, if this fails, doesn't matter.
				}
				this.openConnection();

				// Retry to get the Prepared Statement
				pstmt = connection.prepareStatement(query);

			} catch (DBConnectorException cce) {
				
				// Clean the PreparedStatement
				/*if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException e2) {
						//logger.warn("RS DB API: cannot close PreparedStatement");
					}
					pstmt = null;
				}*/
				
				//logger.error("Cannot close and open the update connection : "
					//	+ cce);				
		
				throw new DBConnectorException(
						"Cannot close and open the connection.", cce);
				
			} catch (SQLException sqlEx2) {
				
				// Clean the PreparedStatement
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException e2) {
						//logger.warn("RS DB API: cannot close PreparedStatement");
					}
					pstmt = null;
				}				
				//logger.error("Cannot get the PreparedStatement from the query = " + query + sqlEx2);					
				throw new DBConnectorException(
						"Cannot get the PreparedStatement from the query = " + query, sqlEx2);
			}
		
		}
		
		return pstmt;
	}
}