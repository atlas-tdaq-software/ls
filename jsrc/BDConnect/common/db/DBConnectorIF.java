package common.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

//import common.db.preparedstatement.DBPreparedStatement;


/**
 * Interface class defining the methods for accessing the database.
 * 
 * @author Andrea Petrucci
 */
public interface DBConnectorIF {

	/**
	 * 
	 * Close the connectiom to DataBase
	 * 
	 * @throws DBConnectorException if a database access error occurs
	 */
	public void closeConnection() throws DBConnectorException;

	/**
	 * Return the Database connection.
	 * 
	 * @return Database connection.
	 */
	public Connection getConnection();

	/**
	 * Returns the Passwd for the connection to the database.
	 * 
	 * @return Returns the Passwd.
	 */
	public String getPasswd();

	/**
	 * Returns the url of the database.
	 * 
	 * @return Returns the url of the database.
	 */
	public String getUrl();

	/**
	 * Returns the user for the connection to the database.
	 * 
	 * @return Returns the dbUser.
	 */
	public String getUser();

	/**
	 * Open the connection to Database
	 * 
	 * @throws DBConnectionException if a database access error occurs
	 */
	public void openConnection() throws DBConnectorException;

	/**
	 * DBExecuteQuery wrap of the executeQuery to managed the connection problem
	 * with the DBMS.
	 * 
	 * @param sqlQuery
	 *            is Sql Query
	 * @return rs is the resulset of the query
	 * @throws DBConnectionException if a database access error occurs
	 */
	public ResultSet DBExecuteQuery(String sqlQuery)
			throws DBConnectorException;

	/**
	 * DBExecuteUpdate wrap of the executeUpdate to managed the connection
	 * problem with the DBMS.
	 * 
	 * @param sqlUpdate
	 *            is Sql Query
	 * @throws DBConnectionException
	 */
	public int DBExecuteUpdate(String sqlUpdate) throws DBConnectorException;

	/**
	 * DBExecute wrap of the execute to managed the connection
	 * problem with the DBMS.
	 * 
	 * @param sqlUpdate is Sql Query
	 * 
	 * @return true if the first result is a ResultSet object; false if it is an update count or there are no results
	 * @throws DBConnectorException if a database access error occurs
	 */
	public boolean DBExecute(String sqlUpdate) throws DBConnectorException;
	
	/**
	 * DBPreparedStatementExecuteQuery wrap of the executeQuery to managed the connection problem
	 * with the DBMS.
	 * 
	 * @param pstmt
	 *            is the DBPreparedStatement 
	 * @return rs is the resulset of the PreparedStatement
	 * @throws DBConnectionException if a database access error occurs
	 */
//	public ResultSet DBPreparedStatementExecuteQuery(DBPreparedStatement dbps) throws DBConnectorException;
	
	/**
	 * DBPreparedStatementExecuteQuery wrap of the execute to managed the connection problem
	 * with the DBMS.
	 * 
	 * @param pstmt
	 *            is the DBPreparedStatement 
	 * @return rs is the resulset of the PreparedStatement
	 * @throws DBConnectionException if a database access error occurs
	 */
//	public boolean DBPreparedStatementExecute(DBPreparedStatement dbps) throws DBConnectorException;
	
	/**
	 * DBPreparedStatementExecuteUpdate wrap of the executeUpdate to managed the connection
	 * problem with the DBMS.
	 * @param pstmt
	 *            is the DBPreparedStatement 
	 * @throws DBConnectionException
	 */
	//public int DBPreparedStatementExecuteUpdate(DBPreparedStatement dbps) throws DBConnectorException;
	
	/**
	 * Set the passwd for the connection to the database.
	 * 
	 * @param passwd
	 *            is the Passwd to set.
	 */
	public void setPasswd(String passwd);

	/**
	 * Set the url of the database.
	 * 
	 * @param url
	 *            is the url to set.
	 */
	public void setUrl(String url);

	/**
	 * Set the user for the connection to the database.
	 * 
	 * @param user
	 *            is the user to set.
	 */
	public void setUser(String user);

	/**
	 * Closes the ResultSet and the related Statement.
	 * 
	 * @param rs
	 *            is the ResultSet to close.
	 * @return always a null ResultSet
	 */
	public ResultSet closeResultSet(ResultSet rs);
	
	/**
	 * Closes the PreparedStatement.
	 * 
	 * @param preparedStatement is the PreparedStatement to close.
	 * @return
	 */
	public PreparedStatement closePreparedStatement(PreparedStatement preparedStatement);
	
	/**
	 * Get a PreparedStatement from a query.
	 * @param query 
	 * 			is the SQL query.
	 * @return PreparedStatement which represents the PreparedStatement.
	 * @throws DBConnectorException
	 */
	public PreparedStatement getPreparedStatement(String query) throws DBConnectorException;

}