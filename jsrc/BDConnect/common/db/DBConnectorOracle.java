package common.db;

import java.lang.reflect.InvocationTargetException;
import java.sql.DriverManager;
import java.sql.SQLException;

//import util.logger.RCMSLogger;

/**
 * 
 * Implements the methods are used for the Oracle database.
 * 
 * @author Alexander Oh, Andrea Petrucci, Vincent Boyer
 */
public class DBConnectorOracle extends AbstractDBConnector 
{
	public static int connectionTimeout = 10;
	
	/**
	 * Default Constructor
	 * 
	 * @param url
	 *            URL of oracle database.
	 * @param user
	 *            User for connection to the database.
	 * @param passwd
	 *            Password of dbUser for connection to the database.
	 */
	public DBConnectorOracle(String url, String user, String passwd)
	{ 
		// Set the parameters for connection to database
		this.url = url;
		this.user = user;
		this.passwd = passwd;
	}

	/**
	 * Open the connection to Database
	 * 
	 * @throws DBConnectionException if a database access error occurs
	 */
	@Override
    public void openConnection() throws DBConnectorException 
	{
		try {
			// Load jdbc driver if not already loaded
			Class.forName("oracle.jdbc.driver.OracleDriver");
			// Instantiate driver
			Class.forName("oracle.jdbc.driver.OracleDriver").getDeclaredConstructor().newInstance();
			// Connection to the database with the parameters url, user and
			DriverManager.setLoginTimeout(connectionTimeout);
			long ts = System.currentTimeMillis();
			connection = DriverManager.getConnection(this.url, this.user, this.passwd);			
      ers.Logger.debug(1, new ers.Issue("established \"" + this.url + "\" connection for \"" + this.user + "\" in " + (System.currentTimeMillis() - ts) + " ms"));
		}
		catch (ClassNotFoundException e) {
			throw new DBConnectorException("Cannot load jdbc driver.", e);
		} 
		catch (InstantiationException e) {
			throw new DBConnectorException("Cannot instantiate jdbc driver.", e);
		} 
		catch (IllegalAccessException e) {
			throw new DBConnectorException("Cannot instantiate jdbc driver.", e);
		}
    catch (IllegalArgumentException e) {
      throw new DBConnectorException("Cannot instantiate jdbc driver.", e);
    }
    catch (InvocationTargetException e) {
      throw new DBConnectorException("Cannot instantiate jdbc driver.", e);
    }
    catch (NoSuchMethodException e) {
      throw new DBConnectorException("Cannot instantiate jdbc driver.", e);
    }
    catch (SecurityException e) {
      throw new DBConnectorException("Cannot instantiate jdbc driver.", e);
    }
    catch (SQLException sqlEx) {
      throw new DBConnectorException("Cannot open connection to "
        + this.url + " with user " + this.user + ".", sqlEx);
    }
}
	
	/**
	 * Close the connection to Database
	 * 
	 * @throws DBConnectionException if a database access error occurs
	 */
/*	public void closeConnection() throws DBConnectorException 
	{
		try {		
			closeConnection();			
		} 
		catch (DBConnectorException e) {
			throw e;
		} 
	}*/	
}
