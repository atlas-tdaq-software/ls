package common.db;

/**
 * Exception class for accessing the database.
 * 
 * @author Andrea Petrucci
 *
 */
public class DBConnectorException 
extends Exception
{
	
	private static final long serialVersionUID = 6383620541122481573L;

	/**
	 * Constructs a ChechConnectionException with no specified detail message.
	 */
	public DBConnectorException()
	{
		
	}
	
	/**
	 * Constructs a ChechConnectionException with the specified detail message.
	 * 
	 * @param s the detail message
	 */
	public DBConnectorException( String s )
	{
		super( s );
	}
	
	/**
	 * Constructs a ChechConnectionException with the specified detail message and nested exception.
	 * 
	 * @param s the detail message
	 * @param ex the nested exception
	 */
	public DBConnectorException( String s, Throwable ex )
	{
		super( s, ex );
	}
}
