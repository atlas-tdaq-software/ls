package common.db;

import java.io.BufferedWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Map;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import GUI.InfoReporting;
import GUI.SearchCriteria;
import GUI.MessageListTable;
import GUI.MessageListTableJPanel;


/**
 * this class does all the access to the database
 * @author cbiau
 *
 */

public class LogServiceDB 
{
	private DBConnectorIF dbConnector;
	private String dbURL;
	private String dbUser;
	private String dbPasswd;						
    private static int globalSelectID = 0;
    private int currentSelectID;
    
	public LogServiceDB(String connectionString, String user, String pass)
	{
		dbURL = connectionString;
		dbUser = user; 
		dbPasswd = pass;
		dbConnector = new DBConnectorOracle(dbURL, dbUser, dbPasswd);
	}
		
	public void openConnection() throws DBConnectorException
	{
		try {
		  long ts = System.currentTimeMillis();
			InfoReporting.printInfo("Opening connection to: " + dbURL + ".");			
      dbConnector.openConnection();
      InfoReporting.printSuccess("Connection to: " + dbURL + " established in " + (System.currentTimeMillis() - ts) + " ms.");
		} catch (DBConnectorException e) {
			throw new DBConnectorException("Failed to connect to " + dbURL, e);
		}		
	}

	public void openConnection(String url, String user, String password) throws DBConnectorException
	{
		try {
    		// Create the dbConnector
			dbConnector = null;
			dbURL = url;
			dbUser = user;
			dbPasswd = password;
			openConnection();
		} catch (DBConnectorException e) {
			throw new DBConnectorException("Failed to connect to " + dbURL, e);
		}		
	}
	
	public void closeConnection() throws DBConnectorException
	{
		try {
      long ts = System.currentTimeMillis();
			InfoReporting.printInfo("Closing connection to: " + dbURL + ".");
			dbConnector.closeConnection(); 
			InfoReporting.printSuccess("Connection to: " + dbURL + " closed in " + (System.currentTimeMillis() - ts) + " ms.");
		} catch (DBConnectorException e) {
			throw new DBConnectorException("Failed to close connection to " + dbURL, e);
		}		
	}	
	
	
	/**
	 * Retrieves the unique TDAQ releases for which entrie are available.
	 * @return a list of TDAQ releases
	 */
	public void SelectTDAQreleases(ArrayList<String> listReleases) throws DBConnectorException
	{
		String sReqSessionList = "SELECT DISTINCT(TDAQ_RELEASE) FROM ATLAS_LOG_MESSG.PARTITION_IDS ORDER BY TDAQ_RELEASE ASC";
    ers.Logger.debug(1, new ers.Issue("execute query " + sReqSessionList));

		try {
			ResultSet rs = dbConnector.DBExecuteQuery(sReqSessionList);
			try {
				while (rs.next()) { 					
					listReleases.add(rs.getString("TDAQ_RELEASE"));
				}				
			} catch (SQLException e) {
				throw new DBConnectorException("", e);
			}
		} catch (DBConnectorException e) {
			throw new DBConnectorException("Failed to retrieve the Partition names.", e);
		}
	}
	
	
	
	/**
	 * Select all the partitions of the database
	 * It uses the PARTITION_IDS for that
	 * @return a table of string containing the partition names
	 */
	public void SelectListPartitions(Map<String, Long> listPartitions, String tdaqRelease) throws DBConnectorException
	{
		String sReqSessionList = "SELECT * FROM ATLAS_LOG_MESSG.PARTITION_IDS WHERE TDAQ_RELEASE='" + tdaqRelease + "' ORDER BY PARTITION_NAME ASC";
    ers.Logger.debug(1, new ers.Issue("execute query " + sReqSessionList));

		try {
			ResultSet rs = dbConnector.DBExecuteQuery(sReqSessionList);
			try {
				while (rs.next()) { 					
					listPartitions.put(rs.getString("PARTITION_NAME"), Long.decode(rs.getString("PARTITION_ID")));
				}				
			} catch (SQLException e) {
				throw new DBConnectorException("", e);
			}
		} catch (DBConnectorException e) {
			throw new DBConnectorException("Failed to retrieve the Partition names.", e);
		}
	}
	
	
	/**
	 * This function is to take the list of the users 
	 * when the partition is equal to 'partition'
	 * @param partition
	 * @param the list of users of the partition
	 */
	public void SelectListUser(Long partitionID, ArrayList<String> listUsers, long startTime) throws DBConnectorException
	{
		String  sReqSelect = "SELECT DISTINCT(user_name) FROM ATLAS_LOG_MESSG.METADATA WHERE partition_id = '" + partitionID + 
		                     "' AND TIMESTAMP > " + startTime + " ORDER BY user_name";

    ers.Logger.debug(1, new ers.Issue("execute query " + sReqSelect));

		try {
			ResultSet rs = dbConnector.DBExecuteQuery(sReqSelect);
			try {
				while (rs.next())
				{
					listUsers.add(rs.getString("user_name"));
				}
				
			} catch (SQLException e) {
				throw new DBConnectorException("", e);				
			}
		} catch (DBConnectorException e) {
			throw new DBConnectorException("Failed to retrieve the User names.", e);
		}		
	}
	
	/**
	 * This function takes the list of sessions for parition = 'partition'
	 * and user = 'user'
	 * @param partition
	 * @param user
	 * @param the list of sessions
	 */
	public void SelectListRunNumbers(Long partitionID, String user, ArrayList<String> listRunNumbers, boolean timeZoneGeneva, long startTime) throws DBConnectorException
	{
		String  sReqSelect = "SELECT DISTINCT(RUN_NUMBER), TIMESTAMP FROM ATLAS_LOG_MESSG.METADATA WHERE partition_id = '" + 
		                     partitionID + "' AND user_name = '" + user + "' AND TIMESTAMP > " + startTime + " ORDER BY RUN_NUMBER DESC";

    ers.Logger.debug(1, new ers.Issue("execute query " + sReqSelect));

		try {
			ResultSet rs = dbConnector.DBExecuteQuery(sReqSelect);
			
			SimpleDateFormat df  = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
			TimeZone         tz  = (timeZoneGeneva?TimeZone.getTimeZone("Europe/Zurich"):TimeZone.getDefault());
			Calendar         cal = new GregorianCalendar(tz);
			df.setTimeZone(tz);
			try {		
				while (rs.next())
				{
					long time = (long)1000 * Integer.valueOf(rs.getString("TIMESTAMP")); // Convert to ms.
					cal.setTime(new Date(time));
					listRunNumbers.add(Integer.valueOf(rs.getString("RUN_NUMBER")) + "  (" + df.format(cal.getTime()) + ")");
				}				
				
			} catch (SQLException e) {
				throw new DBConnectorException("", e);
			}			
		} catch (DBConnectorException e) {
			throw new DBConnectorException("Failed to retrieve the Run Number values.", e);
		}
	}
	
	
	/**
	 * Executes a database query with the search criteria passed. 
	 * @param SQL query
	 * @param Vector of messages to add the messages in
	 * @return
	 */
	public boolean SelectMessages(SearchCriteria criteria, MessageListTable listTable, boolean timeZoneGeneva) throws DBConnectorException
	{
        currentSelectID = ++globalSelectID;
        
		DefaultTableModel tableModel = listTable.getMessageListTableModel();				
		String query                 = criteria.getQuerySelect();		
		if (query.isEmpty()) {
		   throw new DBConnectorException("A Partition must be defined in order to correctly compose a query.");
		}

		ResultSet rs;
		int rowCount = 0;
		try {			
			// Get total number of rows for this given criteria
			int numberOfRows = getNbResults(criteria);
			// Since this is handled within a thread, make sure that this is the last partition/user/sesssion
			// selected. If not, do not display on the table.			
			if (currentSelectID != globalSelectID) {
			   return false;
			}

			// Get the messages			
			rs = dbConnector.DBExecuteQuery(query);
			// Since this is handled within a thread, make sure that this is the last partition/user/sesssion
			// selected. If not, do not display on the table.			
			if (currentSelectID != globalSelectID) {
				return false;
			}
			
			SimpleDateFormat df  = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
			TimeZone         tz  = (true==timeZoneGeneva?TimeZone.getTimeZone("Europe/Zurich"):TimeZone.getDefault());
			Calendar         cal = new GregorianCalendar(tz);
			df.setTimeZone(tz);
			try {				
				Vector<String> row = null;				
				while (rs.next())
				{			
					row = new Vector<String>();


					long time = 1000 * rs.getLong("ISSUED_WHEN"); // Convert to ms.
					cal.setTime(new Date(time));

					String severity;
					int sevInt = Integer.valueOf(replaceNull(rs.getString("SEVERITY")));
					if (sevInt < 6) {
						severity = MessageListTableJPanel.strSeverity[sevInt];
					}
					else {
						severity = "Undefined";
					}	
					
					String chainedMsgs = replaceNull(rs.getString("CHAINED_MSGS"));				
					if (chainedMsgs.isEmpty()) {
						row.add("");
					}
					else {
						row.add("+");
						// Use the root log message time for all the chained messages. We need to do this
						// because for release < tdaq-03-00-00 the chained messages timesamp was local
						// time instead of UTC. Assume that the root and the chained messages were issued
						// within the same second.
						chainedMsgs = "RootTime:[" + df.format(cal.getTime()) + "]" + chainedMsgs;
					}
					
					row.add(replaceNull(rs.getString("MACHINE_NAME")));
					row.add(replaceNull(rs.getString("APPLICATION_NAME")));					
					row.add(df.format(cal.getTime()));
					row.add(severity);
					row.add(replaceNull(rs.getString("MSG_ID")));
					row.add(replaceNull(rs.getString("MSG_TEXT")));
					row.add(replaceNull(rs.getString("PARAM")));
					row.add(replaceNull(rs.getString("OPT_PARAM")));
					row.add(replaceNull(rs.getString("QUALIFIERS")));
					row.add(chainedMsgs);
					tableModel.insertRow(rowCount, row);
					++rowCount;					
				}	
				listTable.setNumberOfRowsLocal(rowCount);	
				listTable.setNumberOfRowsDatabase(numberOfRows);
			} catch (SQLException e) {
				throw new DBConnectorException("Failed to retrieve the messages for partition " + criteria.getPartitionName() + ". SQL query: " + query, e);
			}
		} catch (DBConnectorException e) {
			throw new DBConnectorException("Failed to retrieve the messages for partition " + criteria.getPartitionName() + ". SQL query: " + query, e);
		}
		
		return true;
	}	
	

	public int SaveMessages(SearchCriteria criteria, BufferedWriter output, long numRows, boolean timeZoneGeneva) throws DBConnectorException
	{		
		final int rowsAccess = 1000; 
		int msgCount = 0;
		String query;
		long rowFromBackup;
		long rowToBackup;
		ResultSet rs;

		rowFromBackup = criteria.getLimitFrom();
		rowToBackup = criteria.getLimitTo();

		criteria.setLimitFrom(0);
		criteria.setLimitTo(0);
		int numPages;
		numPages = (int)(numRows / rowsAccess);	    
	    if (((int)(numRows % rowsAccess)) > 0 ) {
	    	++numPages;
	    }		
	    
		SimpleDateFormat df  = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
		TimeZone         tz  = (true==timeZoneGeneva?TimeZone.getTimeZone("Europe/Zurich"):TimeZone.getDefault());
		Calendar         cal = new GregorianCalendar(tz);
		df.setTimeZone(tz);
		while (numPages-- > 0) {			
			criteria.setLimitTo(criteria.getLimitTo() + rowsAccess);			
			criteria.composeQuery();
			query = criteria.getQuerySelect();
			if (query.isEmpty()) {
				throw new DBConnectorException("A Partition must be defined in order to correctly compose a query.");				
			}
			try {			
				rs = dbConnector.DBExecuteQuery(query);
				while (rs.next())
				{					
					// Translate the UTC time in a readable format
					long time = (long)1000 * Integer.valueOf(rs.getString("ISSUED_WHEN"));	
					cal.setTime(new Date(time));					
					
    				String severity;
					int sevInt = Integer.valueOf(replaceNull(rs.getString("SEVERITY")));
					if (sevInt < 6) {
						severity = MessageListTableJPanel.strSeverity[sevInt];
					}
					else {
						severity = "Undefined";
					}
					try {
						output.write(++msgCount + ": "  + replaceNull(rs.getString("MACHINE_NAME")) 
								                + " | " + replaceNull(rs.getString("APPLICATION_NAME")) 
								                + " | " + df.format(cal.getTime()) 
								                + " | " + severity 
								                + " | " + replaceNull(rs.getString("MSG_ID")) 
								                + " | " + replaceNull(rs.getString("MSG_TEXT")) 
								                + " | " + replaceNull(rs.getString("PARAM"))
								                + " | " + replaceNull(rs.getString("OPT_PARAM"))
								                + " | " + replaceNull(rs.getString("QUALIFIERS")));
						output.newLine();
						
						// Add the chained messages
						String chainedMsgs = replaceNull(rs.getString("CHAINED_MSGS"));
					
						if (!chainedMsgs.isEmpty()) {							
							String[] tabTemp1, tabTemp2, tabTemp3;
							tabTemp1 = chainedMsgs.split("EndMsg ");

							String cmMsgId;
							String cmHost;
							String cmApplication;
							String cmSeverity;
							String cmMsgText;
							String cmParam;
							String cmOptParam;
							String cmQualifiers;
							
							for (Integer i = 0; i < tabTemp1.length; i++)
							{
								tabTemp2 = tabTemp1[i].split(":\\[");			
								tabTemp3 = tabTemp2[1].split("\\]");
								cmMsgId = tabTemp3[0];

								tabTemp3 = tabTemp2[2].split("\\]");
								cmHost = tabTemp3[0];

								tabTemp3 = tabTemp2[3].split("\\]");
								cmApplication = tabTemp3[0];

								tabTemp3 = tabTemp2[4].split("\\]");						 

								// Use the root log message time for all the chained messages. We need to do this
								// because for release < tdaq-03-00-00 the chained messages timesamp was local
								// time instead of UTC. Assume that the root and the chained messages were issued
								// within the same second.
								
								tabTemp3   = tabTemp2[5].split("\\]");
								cmSeverity = tabTemp3[0];

								tabTemp3  = tabTemp2[6].split("\\]");
								cmMsgText = tabTemp3[0];

								tabTemp3 = tabTemp2[7].split("\\]");
								cmParam  = tabTemp3[0];

								tabTemp3   = tabTemp2[8].split("\\]");
								cmOptParam = tabTemp3[0];

								tabTemp3     = tabTemp2[9].split("\\]");
								cmQualifiers = tabTemp3[0];								

								output.write("\tcaused by: " + ": " + cmHost + " | " + cmApplication + " | " +
										     df.format(cal.getTime()) + " | " + cmSeverity + " | " + cmMsgId + " | " +
										     cmMsgText + " | " + cmParam + " | " + cmOptParam + " | " + cmQualifiers);
								output.newLine();
							}
						}
					}
					catch (Exception e) {
						InfoReporting.printError(e.getMessage());
					}				
				}
				try {
					output.flush();
				}
				catch (Exception e) {
					InfoReporting.printError(e.getMessage());
				}									
			}
			catch (SQLException e) {
				throw new DBConnectorException("Failed to retrieve the messages for partition " + criteria.getPartitionName() + ". SQL query: " + query, e);
			}			 
			catch (DBConnectorException e) {
				throw new DBConnectorException("Failed to retrieve the messages for partition " + criteria.getPartitionName() + ". SQL query: " + query, e);
			}
			criteria.setLimitFrom(criteria.getLimitFrom() + rowsAccess);			
		}
		
		criteria.setLimitFrom(rowFromBackup);
		criteria.setLimitTo(rowToBackup);
		
		return msgCount;
	}		
	
	/**
	 * Returns the number of entries that match a given criteria
	 * @param table
	 * @param criteria 
	 * @return the number of rows
	 */
	public int getNbResults(SearchCriteria criteria) throws DBConnectorException
	{
		int count = 0;
		String query = criteria.getQueryCount();
		ResultSet rs;		
		try {
			rs = dbConnector.DBExecuteQuery(query);
			try {
				rs.next();
				count = rs.getInt("cpt");
			} catch (SQLException e) {
				throw new DBConnectorException("Failed to retrieve the number of entries for partition " + criteria.getPartitionName() + ". SQL query: " + query, e);				
			}
			
		} catch (DBConnectorException e) {
			throw new DBConnectorException("Failed to retrieve the number of entries for partition " + criteria.getPartitionName() + ". SQL query: " + query, e);		
		}		
		return count;
	}	
	
	/**
	 * when a String given is null it replaces it by ""
	 * @param temp
	 * @return temp
	 */
	public String replaceNull(String temp)
	{
		if (temp == null)
			temp ="";
		
		return temp;
	}

	public String getDbPasswd() {
		return dbPasswd;
	}

	public void setDbPasswd(String dbPasswd) {
		this.dbPasswd = dbPasswd;
	}

	public String getDbURL() {
		return dbURL;
	}

	public void setDbURL(String dbURL) {
		this.dbURL = dbURL;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}
}

