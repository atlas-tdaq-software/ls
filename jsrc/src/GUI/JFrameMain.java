package GUI;

/**
 * this class is the main frame of the application
 * it regroup all the other elements and make the link between them so they can interact
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.TimeZone;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.text.SimpleDateFormat;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.HeadlessException;

import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JEditorPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;

import common.db.DBConnectorException;
import common.db.LogServiceDB;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class JFrameMain extends javax.swing.JFrame  {
	
	class SavelAllThread extends Thread 
	{
		public SavelAllThread() {
		}

		@Override
        public void run() {
			Thread progressBarThread = new Thread()
            {				
				@Override
                public void run() {					
					// Kick in the progress bar
					getMessageListTableJPanel().getPaginationJPanel().getMiscInfo().setText("Saving all the logs....");
					getMessageListTableJPanel().getPaginationJPanel().getProgressBar().setIndeterminate(true);
					getMessageListTableJPanel().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				}
            }; 
            try {
            	SwingUtilities.invokeAndWait(progressBarThread);
            }
            catch (Exception e) {
            	InfoReporting.printError(e.getMessage());
            }            
            saveAllLogs();
            
            // Stop the progress bar
			getMessageListTableJPanel().getPaginationJPanel().getProgressBar().setIndeterminate(false);
			getMessageListTableJPanel().getPaginationJPanel().getMiscInfo().setText("");		
			getMessageListTableJPanel().setCursor(Cursor.getDefaultCursor());
		}		
	}		
	

	private static final long serialVersionUID = 6948826131954536600L;
	public int maxMsgsDisplayed = 1000;

	
        public String databaseConnection;
        public String databaseUser; 
        public String databasePass;
	private String dbSource = "offline";	
		
	private boolean timeZoneGeneva = true; 
	
	private JMenuBar jMenuBarTop;
	private JMenu jMenuFile;
	private JMenu jMenuColumn;
	private JMenu jMenuConfig;
	private JPanel jPanelContent;
	private JMenuItem jMenuItemSaveAll;
	private JMenuItem jMenuItemSaveSelection;
	private JSeparator jSeparator1;	
	private JSeparator jSeparator2;
	private JSeparator jSeparator3;
	private JSeparator jSeparator4;
	private JSeparator jSeparator5;
	private JSeparator jSeparator6;
	private JSeparator jSeparator7;
	private JMenuItem jMenuItemRefreshPartitionTree;
	private JMenuItem jMenuItemRefreshMessageTable;	
	private JMenuItem jMenuItemSelectAll;
	private JMenuItem jMenuItemQuit;
	private JMenuItem jMenuItemMsgID;
	private JMenuItem jMenuItemHost;
	private JMenuItem jMenuItemApplication;
	private JMenuItem jMenuItemIssued;
	private JMenuItem jMenuItemSeverity;
	private JMenuItem jMenuItemMessage;
	private JMenuItem jMenuItemParam;	
	private JMenuItem jMenuItemOptParam;	
	private JMenuItem jMenuItemQualifiers;	
	private JCheckBoxMenuItem jMenuItemVerbosity;
	private JMenuItem jMenuItemMsgsShown;
	private JMenuItem jMenuTableSpan;
	private JCheckBoxMenuItem jMenuItemCaseSensitive;
	private JMenuItem jMenuItemTimeZone;		
	private JMenuItem jMenuTdaqRelease;
	private ArrayList<String> listTdaqReleases;
	private String currentTdaqRelease;
	
	private JMenu jMenuHelp;
	private JMenuItem jMenuItemHelpContents;
	private JFrame helpFrame;
	private JEditorPane helpPanel;
	
	private JSplitPane jSplitPane1;
	private JSplitPane jSplitPane2;
	private JSplitPane jSplitPane3;
	private JSplitPane jSplitPane4;
	
	private ContentDetailJPanel contentDetailJPanel;
	private MessageListTableJPanel messageListTableJPanel;
	private TableTreeJPanel tableTreeJPanel;	
	private SearchJPanel searchJPanel;
	private SearchTreeJPanel searchTreeJPanel;
		
	private long tableTreeStartTime;
	
	//Create a file chooser
	private JFileChooser jFileChooser;

	private String userConnect;
	private boolean isUserConnected;
	private String userName;
	private String userFirstName;
	private Font defaultFont;
	private HashMap<String, String> partition2Table;
	private boolean[] columnsEnabled = { true, false, true, true, true, true, true, false, false, false, false };	
	
	/**
	* Auto-generated main method to display this JFrame
	*/
        public static void main(String[] args) {
            try {
                JFrameMain inst = new JFrameMain(args);
                inst.setVisible(true);
    
                inst.userConnect = "";
                inst.isUserConnected = false;
                inst.defaultFont = new Font("Dialog", Font.PLAIN, 12);
    
            }
            catch(final daq.coral.DBInfoExtractor.BadInfoException e) {
                System.err.println("Cannot start, some error occurred: " + e.getMessage());
                //e.printStackTrace();
            }
        }
    
        public JFrameMain(String[] arg) throws daq.coral.DBInfoExtractor.BadInfoException {
            super("Log Service Manager");
            
            getCommandParameters(arg);
            this.getConnectionParams();
            
            initGUI();
        }
    
        private void getConnectionParams() throws daq.coral.DBInfoExtractor.BadInfoException {
            final daq.coral.DBInfoExtractor infoExtr = daq.coral.DBInfoExtractor.createFromLogicalName("LOG_MANAGER");
                
            this.databaseUser = infoExtr.getUser();
            this.databasePass = infoExtr.getPassword();
            this.databaseConnection = infoExtr.getConnectionString();
        }
	
	private void getCommandParameters(String[] commArgs) 
	{
	  ers.Logger.debug(0, new ers.Issue("command line: " + Arrays.toString(commArgs)));

		int argCount = commArgs.length;	
		int argIndx  = 0;
		String usage = "\nUsage: GUI.JFrameMain [-i StartTime]\n"                         +		
                       "Options/Arguments:"                                               +
                       "\n\t-i StartTime     Start time for the partition/user/run table." + 
                       "\n\t                 Format: dd-mm-yyyy."                          +
                       "\n\t                 Default value: 90 days back."                 +
                       "\n\t-t TDAQ release  TDAQ release to retrieve the logs from."      +
                       "\n";			   
		
		if ((argCount % 2) > 0) {
			ers.Logger.info(new ers.Issue(usage));
			System.exit(-1);
		}
		
		// Default values. Should be overwritten by the script launcher.
		tableTreeStartTime = System.currentTimeMillis()/1000;
		currentTdaqRelease = "tdaq-10-00-00";

		// Roughly substract three months (92 days).
		tableTreeStartTime -= 3600 * 24 * 92;
		while (argIndx < argCount) {
			if (true == commArgs[argIndx].equals("-i")) {
				argIndx++;
				// Convert to UTC time.
				try {
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					// Convert to seconds
					tableTreeStartTime = formatter.parse(commArgs[argIndx]).getTime() / 1000;
				}
				catch (java.text.ParseException e) {					
		      ers.Logger.error(new ers.Issue("Invalid time format: " + commArgs[argIndx] + "." + usage));
					System.exit(-1);					
				}
			}
			else if (true == commArgs[argIndx].equals("-t")) {
				argIndx++;
				currentTdaqRelease = commArgs[argIndx];
			}
			else {
        ers.Logger.error(new ers.Issue("Unknown switch: " + commArgs[argIndx] + "." + usage));
				System.exit(-1);
			}
			argIndx++;
		}
	}
	
	
	private void initGUI() 
	{		
		addWindowListener(new WindowListener() {
            @Override
            public void windowClosed(WindowEvent arg0) {
            }
            @Override
            public void windowActivated(WindowEvent arg0) {
            }
            @Override
            public void windowClosing(WindowEvent arg0) {
            	System.out.println("Exiting the Log Manager.");
            	System.exit(0);
            }
            @Override
            public void windowDeactivated(WindowEvent arg0) {
            }
            @Override
            public void windowDeiconified(WindowEvent arg0) {
            }
            @Override
            public void windowIconified(WindowEvent arg0) {
            }
            @Override
            public void windowOpened(WindowEvent arg0) {
            }
        });
		
		partition2Table = new HashMap<String, String>();
		
		try {
			int frameWidth = 1100;
			int frameHeight = 760;
			
	        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	        // Start in the centre
	        if (screenSize.width > frameWidth && screenSize.height > frameHeight) {
	           Point location = new Point((screenSize.width-frameWidth)/2, (screenSize.height-frameHeight)/2);
               setLocation(location);
	        }		

			BorderLayout thisLayout = new BorderLayout();
			getContentPane().setLayout(thisLayout);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.getContentPane().setBackground(new java.awt.Color(103,103,103));
			this.getContentPane().setForeground(new java.awt.Color(127,124,124));

			// IMPORTANT: DO NOT MODIFY THE ORDER HOW OBJECTS ARE INSTANTIATED *****
			{
				jMenuBarTop = new JMenuBar();
				setJMenuBar(jMenuBarTop);
				jMenuBarTop.setPreferredSize(new java.awt.Dimension(1016, 20));
				jMenuBarTop.setSize(1016, 20);
				jMenuBarTop.setBackground(new java.awt.Color(126,134,255));
				jMenuBarTop.setAlignmentX(0.0f);
				jMenuBarTop.setAutoscrolls(true);
				jMenuBarTop.setLocation(new java.awt.Point(0, 0));
				{
					jMenuFile = new JMenu();
					jMenuBarTop.add(jMenuFile);
					jMenuFile.setText("File");
					jMenuFile.setBackground(new java.awt.Color(225,225,225));
					{
						jMenuItemSelectAll = new JMenuItem();
						jMenuItemSelectAll.setText("Select all shown (Ctrl + a)");
						jMenuFile.add(jMenuItemSelectAll);
						jMenuItemSelectAll
							.addActionListener(new ActionListener() {
							@Override
                            public void actionPerformed(ActionEvent evt) {
								jMenuItemSelectAllActionPerformed(evt);
							}
							});
					}
					{
						jMenuItemSaveSelection = new JMenuItem();
						jMenuFile.add(jMenuItemSaveSelection);
						jMenuItemSaveSelection
							.setText("Save selection (CTRL + s)");
						jMenuItemSaveSelection
							.addActionListener(new ActionListener() {
							@Override
                            public void actionPerformed(ActionEvent evt) {
								jMenuItemSaveSelectionActionPerformed(evt);
							}
							});
					}
					{
						jMenuItemSaveAll = new JMenuItem();
						jMenuFile.add(jMenuItemSaveAll);
						jMenuItemSaveAll
							.setText("Save all");
						jMenuItemSaveAll
							.addActionListener(new ActionListener() {
							@Override
                            public void actionPerformed(ActionEvent evt) {
								jMenuItemSaveAllActionPerformed(evt);
							}
							});
					}
					{
						jSeparator1 = new JSeparator();
						jMenuFile.add(jSeparator1);
					}
					{
						jMenuItemRefreshPartitionTree = new JMenuItem();
						jMenuFile.add(jMenuItemRefreshPartitionTree);
						jMenuItemRefreshPartitionTree
							.setText("Refresh tree");
						jMenuItemRefreshPartitionTree
							.addActionListener(new ActionListener() {
							@Override
                            public void actionPerformed(ActionEvent evt) {
								jMenuItemRefreshPartitionTreePerformed(evt);
							}
							});
					}
					{
						jMenuItemRefreshMessageTable = new JMenuItem();
						jMenuFile.add(jMenuItemRefreshMessageTable);
						jMenuItemRefreshMessageTable
							.setText("Refresh table");
						jMenuItemRefreshMessageTable
							.addActionListener(new ActionListener() {
							@Override
                            public void actionPerformed(ActionEvent evt) {
								jMenuItemRefreshMessageTablePerformed(evt);
							}
							});
					}	
					{
						jSeparator2 = new JSeparator();
						jMenuFile.add(jSeparator2);
					}					
					{
						jMenuItemQuit = new JMenuItem();
						jMenuFile.add(jMenuItemQuit);
						jMenuItemQuit.setText("Quit");
						jMenuItemQuit.addActionListener(new ActionListener() {
							@Override
                            public void actionPerformed(ActionEvent evt) {
								jMenuItemQuitActionPerformed(evt);
							}
						});
					}
				}
				{
					jMenuColumn = new JMenu("Columns");
					jMenuBarTop.add(jMenuColumn);
					jMenuColumn.setBackground(new java.awt.Color(225,225,225));
					{
						jMenuItemHost = new JCheckBoxMenuItem("Host", columnsEnabled[1]);
						jMenuColumn.add(jMenuItemHost);
						jMenuItemHost.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemColumnHostActionPerformed(jMenuItemHost.isSelected());
						}
						});						
					}
					{
						jMenuItemApplication = new JCheckBoxMenuItem("Application", columnsEnabled[2]);
						jMenuColumn.add(jMenuItemApplication);
						jMenuItemApplication.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemColumnApplicationActionPerformed(jMenuItemApplication.isSelected());
						}
						});
					}
					{
						jMenuItemIssued = new JCheckBoxMenuItem("Issued", columnsEnabled[3]);
						jMenuColumn.add(jMenuItemIssued);
						jMenuItemIssued.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemColumnIssuedActionPerformed(jMenuItemIssued.isSelected());
						}
						});
					}
					{
						jMenuItemSeverity = new JCheckBoxMenuItem("Severity", columnsEnabled[4]);
						jMenuColumn.add(jMenuItemSeverity);
						jMenuItemSeverity.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemColumnSeverityActionPerformed(jMenuItemSeverity.isSelected());
						}
						});
					}					
					{
						jMenuItemMsgID = new JCheckBoxMenuItem("ID", columnsEnabled[5]);
						jMenuColumn.add(jMenuItemMsgID);
						jMenuItemMsgID.addActionListener(new ActionListener() {
							@Override
                            public void actionPerformed(ActionEvent evt) {
								jMenuItemColumnMsgIDActionPerformed(jMenuItemMsgID.isSelected());
							}
						});
					}
					{
						jMenuItemMessage = new JCheckBoxMenuItem("Message", columnsEnabled[6]);
						jMenuColumn.add(jMenuItemMessage);
						jMenuItemMessage.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemColumnMsgActionPerformed(jMenuItemMessage.isSelected());
						}
						});
					}					
					{
						jMenuItemParam = new JCheckBoxMenuItem("Parameters", columnsEnabled[7]);
						jMenuColumn.add(jMenuItemParam);
						jMenuItemParam.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemColumnParamActionPerformed(jMenuItemParam.isSelected());
						}
						});
					}		
					{
						jMenuItemOptParam = new JCheckBoxMenuItem("Optional Parameters", columnsEnabled[8]);
						jMenuColumn.add(jMenuItemOptParam);
						jMenuItemOptParam.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemColumnOptParamActionPerformed(jMenuItemOptParam.isSelected());
						}
						});
					}					
					{
						jMenuItemQualifiers = new JCheckBoxMenuItem("Qualifiers", columnsEnabled[9]);
						jMenuColumn.add(jMenuItemQualifiers);
						jMenuItemQualifiers.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemColumnQualifiersActionPerformed(jMenuItemQualifiers.isSelected());
						}
						});
					}					
				}
				{
					jMenuConfig = new JMenu("Configuration");
					jMenuBarTop.add(jMenuConfig);
					{
						int timeSpanDays = (int)(System.currentTimeMillis()/1000 - tableTreeStartTime) / (60*60*24);
						jMenuTableSpan = new JMenuItem();
						jMenuTableSpan.setText("Table span: " + timeSpanDays + " days");
						jMenuTableSpan.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemTableSpanPerformed(evt);
						}
						});	
						jMenuConfig.add(jMenuTableSpan);	

						jSeparator6 = new JSeparator();
						jMenuConfig.add(jSeparator6);
						
						jMenuItemMsgsShown = new JMenuItem();
						jMenuItemMsgsShown.setText("Messages displayed: " + maxMsgsDisplayed);
						jMenuItemMsgsShown.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemMsgsShownPerformed(evt);
						}
						});	
						jMenuConfig.add(jMenuItemMsgsShown);	
						
						jSeparator3 = new JSeparator();
						jMenuConfig.add(jSeparator3);
						
						jMenuItemCaseSensitive = new JCheckBoxMenuItem("Case insensitive search", false);
						jMenuItemCaseSensitive.setText("Case insensitive search");
						jMenuConfig.add(jMenuItemCaseSensitive);
						
						jSeparator4 = new JSeparator();
						jMenuConfig.add(jSeparator4);						
					
						jMenuItemTimeZone = new JMenuItem();
						jMenuItemTimeZone.setText("Time zone: Geneva (CERN)");
						jMenuItemTimeZone.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemTimeZonePerformed(evt);
						}
						});							
						jMenuConfig.add(jMenuItemTimeZone);
						
						jSeparator5 = new JSeparator();
						jMenuConfig.add(jSeparator5);
						
						jMenuTdaqRelease = new JMenuItem();						
						jMenuTdaqRelease.setText("TDAQ release: " +  currentTdaqRelease);
						jMenuTdaqRelease.addActionListener(new ActionListener() {
						@Override
                        public void actionPerformed(ActionEvent evt) {
							jMenuItemTdaqReleasePerformed(evt);
						}
						});
						jMenuConfig.add(jMenuTdaqRelease);
											
						jSeparator7 = new JSeparator();
						jMenuConfig.add(jSeparator7);
						
						jMenuItemVerbosity = new JCheckBoxMenuItem("Verbosity", true);
						jMenuItemVerbosity.setText("Verbosity");
						jMenuConfig.add(jMenuItemVerbosity);
					}
				}
			}			
			{
				contentDetailJPanel = new ContentDetailJPanel();
				Dimension minimumSize = new Dimension(500, 0);
				contentDetailJPanel.setMinimumSize(minimumSize);
			}		
			// Very ugly way of initializing this and having a info/error reporting mechanism....
			InfoReporting.init(this);		

			{
				jMenuHelp = new JMenu("Help");
				jMenuBarTop.add(jMenuHelp);
				{
					jMenuItemHelpContents = new JMenuItem("Help Contents");
					jMenuItemHelpContents.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent evt) {
						jMenuItemHelpContentsPerformed(evt);
					}
					}); 
					jMenuHelp.add(jMenuItemHelpContents);	
					
					helpFrame = new JFrame("Log Manager Help");
					helpFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			        // Start in the centre
					int helpWidth = 700;
					int helpHeight = 600;	
					
			        if (screenSize.width > helpWidth && screenSize.height > helpHeight) {
			           Point location = new Point((screenSize.width-helpWidth)/2, (screenSize.height-helpHeight)/2);
			           helpFrame.setLocation(location);
			        }		
			        helpFrame.setSize(helpWidth, helpHeight);
					
					helpPanel = new JEditorPane();
					helpPanel.setContentType("text/html");
					helpPanel.setEditable(false);	
					JScrollPane jsp = new JScrollPane(helpPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				    helpFrame.add(jsp, BorderLayout.CENTER);
				    
					try {
						String path;
						// If the Log Manager is launched via web start, use the help contents in the atlasdaq. 
						// Otherwise, use the instalation files. 
						if (null != System.getenv().get("TDAQ_INST_PATH")) {
							path = "file://" + System.getenv().get("TDAQ_INST_PATH") + "/share/data/ls/help/LogManagerUserGuide.html";
						} else {
							path = "https://atlasdaq.cern.ch/jnlp/logmanager/help/LogManagerUserGuide.html";
						}
						helpPanel.setPage(path);
					} 
					catch (Exception e) {
						InfoReporting.printError(e.getMessage());
					}	
					helpFrame.validate();
				}
			}			
			{
				searchJPanel = new SearchJPanel(this);	
				Dimension minimumSize = new Dimension(500, 50);
				searchJPanel.setMinimumSize(minimumSize);				
			}
			{
				searchTreeJPanel = new SearchTreeJPanel(this);
				Dimension minimumSize = new Dimension(200, 50);
				searchTreeJPanel.setMinimumSize(minimumSize);
				
			}
			{
				messageListTableJPanel = new MessageListTableJPanel(this);
				Dimension minimumSize = new Dimension(500, 200);
				messageListTableJPanel.setMinimumSize(minimumSize);
				
			}
			{
				tableTreeJPanel = new TableTreeJPanel(this);
				Dimension minimumSize = new Dimension(200, 50);
				tableTreeJPanel.setMinimumSize(minimumSize);
			}
  			{
				jSplitPane1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, messageListTableJPanel, contentDetailJPanel.getJScrollPane());				
				jSplitPane1.setDividerSize(1);
				jSplitPane1.setResizeWeight(0.8);
				jSplitPane1.setContinuousLayout(true);
				jSplitPane1.setDividerLocation(450);
			}
			{
				jSplitPane2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, searchJPanel, jSplitPane1);				
				jSplitPane2.setDividerSize(0);
				jSplitPane2.setResizeWeight(0);
				jSplitPane2.setContinuousLayout(true);
				jSplitPane2.setDividerLocation(80);
			}  			
			{
				jSplitPane3 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, searchTreeJPanel, tableTreeJPanel.getJScrollPane());				
				jSplitPane3.setDividerSize(0);
				jSplitPane3.setResizeWeight(0);
				jSplitPane3.setContinuousLayout(true);
				jSplitPane3.setDividerLocation(80);
			}  			
			{
				jSplitPane4 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, jSplitPane3, jSplitPane2);				
				jSplitPane4.setDividerSize(3);
				jSplitPane4.setResizeWeight(0.2);
				jSplitPane4.setContinuousLayout(true);
				jSplitPane4.setDividerLocation(230);
				getContentPane().add(jSplitPane4, "Center");
			}		
			
			this.addFocusListener(new FocusAdapter() {
				@Override
                public void focusGained(FocusEvent evt) {
					rootFocusGained(evt);
				}
			});
			this.addComponentListener(new ComponentListener() {
		//		@Override
				@Override
                public void componentHidden(ComponentEvent e) {}
			//	@Override
				@Override
                public void componentMoved(ComponentEvent e) {}
//				@Override
				@Override
                public void componentResized(ComponentEvent e) {}
	//			@Override
				@Override
                public void componentShown(ComponentEvent e) {}
				
			});			
			pack();		
			this.setSize(frameWidth, frameHeight);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * These function are the get and set of the user's settings
	 */
	public boolean GetIsConnected() { return isUserConnected; }
	public void SetIsConnected(boolean val) { isUserConnected = val; }
	
	public String GetUserConnect() { return userConnect; }
	public void SetUserConnect(String val) { userConnect = val; }
	
	public String GetUserName() { return userName; }
	public void SetUserName(String val) { userName = val; }

	public String GetUserFirstName() { return userFirstName; }
	public void SetUserFirstName(String val) { userFirstName = val; }
	
	
	private void rootFocusGained(FocusEvent evt) {
		/*/if (isUserConnected == true) 
		{
			jMenuItemConnect.setText("Disconnect");
			jLabelUser.setText(this.GetUserFirstName() + " " + this.GetUserName());
			
			tableTreeJPanel.getTableTree().filljTreeTable();
			//this.filljTreeTable();
		/*}
		if (isUserConnected == false) 
			jMenuItemConnect.setText("Connect");	*/	
	}
	
	/**
	 * function to quit the application
	 * @param evt
	 */
	private void jMenuItemQuitActionPerformed(ActionEvent evt) {
		System.out.println("Exiting the Log Manager.");
		System.exit(0);
	}
	
	/**
	 * function to set the number of messages shown.
	 * @param evt
	 */
	private void jMenuItemMsgsShownPerformed(ActionEvent evt) {		
		String msgsShown = "";
		try {
			int oldValue = maxMsgsDisplayed;
			msgsShown = JOptionPane.showInputDialog(this, "Number of log messages shown.", "", JOptionPane.PLAIN_MESSAGE);
			maxMsgsDisplayed = Integer.valueOf(msgsShown);
			jMenuItemMsgsShown.setText("Messages displayed: " + maxMsgsDisplayed);
			// 10000 for no reason really
			if (maxMsgsDisplayed > 10000) {
				InfoReporting.printWarning("The value chosen is rather large, your system may run out of memory.");				
			}
			
			SearchCriteria criteria = messageListTableJPanel.getListMessageTable().getSearchCriteria();
			criteria.setLimitTo(criteria.getLimitTo() + (maxMsgsDisplayed - oldValue));
		}
		catch(NumberFormatException e) {
			InfoReporting.printWarning("The input value '" + msgsShown + "' is not a digit.");
		}
	}
	
	/**
	 * function to set the time span for the parittion/user/runNumber table
	 * @param evt
	 */
	private void jMenuItemTableSpanPerformed(ActionEvent evt) {		
		String timeSpan = "";
		try {
			timeSpan = JOptionPane.showInputDialog(this, "Time span in days for the Partition/User/runNumber table.", "", JOptionPane.PLAIN_MESSAGE);
			int timeSpanDays = Integer.valueOf(timeSpan);			
			jMenuTableSpan.setText("Table span: " + timeSpanDays + " days");
			
			SimpleDateFormat formatter = new SimpleDateFormat("d-M-yyyy");
			// Current time in seconds
			tableTreeStartTime = System.currentTimeMillis() / 1000;
			// Reduce the nubmer of days input			
			tableTreeStartTime -= 60 * 60 * 24 * timeSpanDays;
			tableTreeJPanel.getTableTree().filljTreeTable();			
		}
		catch(NumberFormatException e) {
			InfoReporting.printWarning("The input value '" + timeSpan + "' is not a digit.");
		}
	}	
	

	/**
	 * function display the Help Contents
	 * @param evt
	 */
	private void jMenuItemHelpContentsPerformed(ActionEvent evt) {	
		helpFrame.setVisible(true);
		//listPath(new File("../data/"));
	}
	

	/**
	 * function to set the database connection.
	 * @param evt
	 */
	private void jMenuItemTimeZonePerformed(ActionEvent evt) 
	{
		try {
			String local = "Local: " + TimeZone.getDefault().getDisplayName();
			Object[] possibilities = {"Geneva (CERN)", local};
			String dbType = (String)JOptionPane.showInputDialog(this,
                                                                "Time zone:\n",
                                                                "Time zone setting",
                                                                 JOptionPane.PLAIN_MESSAGE,
                                                                 null,
                                                                 possibilities,
                                                                 "Geneva (CERN)");		
			if (null == dbType) {
				return;
			}
			if (0 == dbType.compareTo("Geneva (CERN)")) {
            	timeZoneGeneva = true;
            	jMenuItemTimeZone.setText("Time zone: Geneva (CERN)");
            	
            }
			else if (0 == dbType.compareTo(local)) {				
				timeZoneGeneva = false;
				jMenuItemTimeZone.setText("Time zone: " + TimeZone.getDefault().getDisplayName());
			}
            else {
				// Should never be here
            }
			
			messageListTableJPanel.getMessageListTable().refreshTable();
			tableTreeJPanel.getTableTree().filljTreeTable();			
			searchJPanel.setSpinnerTime();
		}
		catch(HeadlessException e) {
			InfoReporting.printWarning("Cause: " + e.getCause() + ". Message: " + e.getMessage());
		}
	}
	
	
	private void jMenuItemTdaqReleasePerformed(ActionEvent evt)
	{
		// Lazy initialization. Only get the list of TDAQ releases if the user chooses this option.		
		if (null == listTdaqReleases) {
			listTdaqReleases = new ArrayList<String>();
			
			InfoReporting.printInfo("Retrieveing list of TDAQ releases.");
			try {			
				LogServiceDB db = new LogServiceDB(databaseConnection, databaseUser, databasePass);	
				db.openConnection();					
				db.SelectTDAQreleases(listTdaqReleases);    				
				db.closeConnection();	
			}
			catch (DBConnectorException e) {			
				InfoReporting.printError(e);			
			}
			InfoReporting.printInfo("List of TDAQ releases retrieved.");
			
			// currentTdaqRelease is filled in at compilation time and refers to the TDAQ release
			// used when building this binary. If this release is not yet in the database (a partition
			// has not run yet) add it manually.
			if (-1 == listTdaqReleases.indexOf(currentTdaqRelease)) {
				listTdaqReleases.add(currentTdaqRelease);
				Collections.sort(listTdaqReleases);				
			}
		}
		
		
		try {
			//Object[] possibilities = {"tdaq-02-00-03", "tdaq-03-00-01"};
			String release = (String)JOptionPane.showInputDialog(this,
                                                                 "Messages for TDAQ release:\n",
                                                                 "TDAQ release",
                                                                  JOptionPane.PLAIN_MESSAGE,
                                                                  null,
                                                                  listTdaqReleases.toArray(),
                                                                  currentTdaqRelease);		
			if (null == release) {
				return;
			}
			
			if (0 != currentTdaqRelease.compareTo(release)) {
				currentTdaqRelease = release;
				jMenuTdaqRelease.setText("TDAQ release: " +  currentTdaqRelease);				
				tableTreeJPanel.getTableTree().filljTreeTable();
			}			 			
 		}
		catch(HeadlessException e) {
			InfoReporting.printWarning("Cause: " + e.getCause() + ". Message: " + e.getMessage());
		}				
	}
	
	
	
	public void showContent() { jPanelContent.setVisible(true); }	
	public void hideContent() { jPanelContent.setVisible(false); }	
	
	public ContentDetailJPanel getContentDetailJPanel() { return contentDetailJPanel; }
	public MessageListTableJPanel getMessageListTableJPanel() { return messageListTableJPanel; }
	public TableTreeJPanel getTableTreeJPanel() { return tableTreeJPanel;	}	
	public SearchJPanel getSearchJPanel() { return searchJPanel; }
		
	public void SetContentDetailJPanel(ContentDetailJPanel val) { contentDetailJPanel = val; }
	public void MessageListTableJPanel(MessageListTableJPanel val) { messageListTableJPanel = val; }
	public void SetTableTreeJPanel(TableTreeJPanel val) { tableTreeJPanel = val; }
	public void SetSearchJPanel(SearchJPanel val) { searchJPanel = val; }

	
	/**
	 * this function selects all the displayed content
	 * @param evt
	 */
	private void jMenuItemSelectAllActionPerformed(ActionEvent evt) {
		this.messageListTableJPanel.getListMessageTable().selectAll();
	}
	
	
	/**
	 * this function permit to the user to save his/her selection in *.txt
	 * it opens a filechooser so the user gives the name and the path of the file
	 * @param evt
	 */
	private void jMenuItemSaveSelectionActionPerformed(ActionEvent evt) 
	{		
		saveSelection();
	}
	
	
	public void saveSelection()
	{
		jFileChooser = new JFileChooser();
		
		FileFilter txt = new FiltreSimple("Text documents (*.txt)",".txt");
		//FileFilter xml = new FiltreSimple("Fichiers XML (*.xml)",".xml");
		jFileChooser.addChoosableFileFilter(txt);
		//jFileChooser.addChoosableFileFilter(xml);
     
		int returnVal = jFileChooser.showSaveDialog(this);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = jFileChooser.getSelectedFile();			
			if (jFileChooser.getSelectedFile().getName().indexOf(".txt") == -1 ) {
				file = new File(file.getPath().toString() + ".txt");	            
			}
			
    		try {
                int[] selectedRows = messageListTableJPanel.getSelectedRows();	    		

    			FileWriter fw = new FileWriter(file, false);    			
    			BufferedWriter output = new BufferedWriter(fw);

    			// Add the header
    			output.write("DATE : " + (new Date()).toString()); 
    			output.newLine();  
    			output.write("PARTITION NAME : " + messageListTableJPanel.getListMessageTable().getSearchCriteria().getPartitionName());	    
    			output.newLine();  
        		output.write("FILE NAME : " + file.getName());
				output.newLine();        		
        		output.write("");
				output.newLine();        		
        		output.write("|   Machine Name   |   Application Name  |    Issued When    |    Severity  |          Msg Text        |      Param      |");
				output.newLine();        		
        		output.write("--------------------------------------------------------------------------------------------------------------------------");
				output.newLine();
    			// Write to file
    			output.flush();

    			int rowCount = 0;
    			
    			// Write the messages
    			DefaultTableModel model = messageListTableJPanel.getDefaultTableModel();
    			
    			for (rowCount = 0; rowCount < selectedRows.length; rowCount++)
        		{
        			int row = selectedRows[rowCount];    			
        			output.write((String)model.getValueAt(row, 1) + " | " + (String)model.getValueAt(row, 2) + "  |  " +
        						 (String)model.getValueAt(row, 3) + " | " + (String)model.getValueAt(row, 4) + "  |  " +
        						 (String)model.getValueAt(row, 5) + " | " + (String)model.getValueAt(row, 6) + "  |  " +
        						 (String)model.getValueAt(row, 5) + " | ");
        			output.newLine();        
        			
        			
        			
        		}    
        		output.write("--------------------------------------------------------------------------------------------------------------------------");
				output.newLine();    			
        		output.write("MESSAGE SAVED : " + rowCount);
				output.newLine();        		
        		output.write("--------------------------------------------------------------------------------------------------------------------------");
        		output.newLine();    			
    			
    			//close the file
    			output.close();
    			
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}            
        }
	}
	
	
	/**
	 * this function permit to the user to save his/her selection in *.txt
	 * it opens a filechooser so the user gives the name and the path of the file
	 * @param evt
	 */
	private void jMenuItemSaveAllActionPerformed(ActionEvent evt) 
	{	
		// Create thread to handle the request and wait for it to finish	
		SavelAllThread savelAllThread = new SavelAllThread();
		savelAllThread.start();	
	}

	private void jMenuItemRefreshPartitionTreePerformed(ActionEvent evt) 
	{	
		tableTreeJPanel.getTableTree().filljTreeTable();
	}

	private void jMenuItemRefreshMessageTablePerformed(ActionEvent evt) 
	{	
		messageListTableJPanel.getMessageListTable().refreshTable();
	}
	
	private void saveAllLogs() 
	{	
		jFileChooser = new JFileChooser();
		
		FileFilter txt = new FiltreSimple("Text documents (*.txt)",".txt");
		//FileFilter xml = new FiltreSimple("Fichiers XML (*.xml)",".xml");
		jFileChooser.addChoosableFileFilter(txt);
		//jFileChooser.addChoosableFileFilter(xml);
     
		int returnVal = jFileChooser.showSaveDialog(this);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = jFileChooser.getSelectedFile();			
			if (jFileChooser.getSelectedFile().getName().indexOf(".txt") == -1 ) {
				file = new File(file.getPath().toString() + ".txt");	            
			}
			
			InfoReporting.printInfo("Saving in file '" + file + "' all log files that meet the defined search criteria .");    		
    		try {
    			FileWriter fw = new FileWriter(file, false);    			
    			BufferedWriter output = new BufferedWriter(fw);

    			// Add the header
    			output.write("DATE : " + (new Date()).toString()); 
    			output.newLine();  
    			output.write("PARTITION NAME : " + messageListTableJPanel.getListMessageTable().getSearchCriteria().getPartitionName());    
    			output.newLine();  
        		output.write("FILE NAME : " + file.getName());
				output.newLine();        		
        		output.write("");
				output.newLine();        		
        		output.write("|   Machine Name   |   Application Name  |    Issued When    |    Severity  |          Msg Text        |      Param      |");
				output.newLine();        		
        		output.write("--------------------------------------------------------------------------------------------------------------------------");
				output.newLine();
    			// Write to file
    			output.flush();

    			int msgsSaved = 0;
    			// Write the messages
    			try {			
    			    LogServiceDB db = new LogServiceDB(databaseConnection, databaseUser, databasePass);	
    				db.openConnection();					
    				msgsSaved = db.SaveMessages(messageListTableJPanel.getListMessageTable().getSearchCriteria(), output, messageListTableJPanel.getListMessageTable().getNumberOfRowsDatabase(), timeZoneGeneva);    				
    				db.closeConnection();	
    			}
    			catch (DBConnectorException e) {			
    				InfoReporting.printError(e);
    				return;				
    			}      			
        		output.write("--------------------------------------------------------------------------------------------------------------------------");
				output.newLine();    			
        		output.write("MESSAGE SAVED : " + msgsSaved);
				output.newLine();        		
        		output.write("--------------------------------------------------------------------------------------------------------------------------");
        		output.newLine();    			
    			
    			//close the file
    			output.close();
    			
    			InfoReporting.printSuccess(msgsSaved + " logs saved in file '" + file + "'.");    			
    			
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}            
        }
	}
	
	 
	
	/**
	 * Column action perform
	 * @param evt
	 */
	public void jMenuItemColumnHostActionPerformed(boolean selected) 
	{
		// If there is no table, exit. The search criteria is only created when a table is selected.
		if (messageListTableJPanel.getListMessageTable().getSearchCriteria() == null) {
			return;
		}
		int columnIndx = 1;
		
		if (false == selected) {
			// Might throw if the column is not active
			try {
				messageListTableJPanel.getListMessageTable().removeColumn(messageListTableJPanel.getListMessageTable().getColumn("Host"));	
			}
			catch (IllegalArgumentException e) { } // Do nothing
			columnsEnabled[columnIndx] = false;
		}
		else {
			columnsEnabled[columnIndx] = true;
			TableColumn column = new TableColumn(1);
			int columnCount = messageListTableJPanel.getListMessageTable().getColumnCount();			
			messageListTableJPanel.getListMessageTable().addColumn(column);	
			messageListTableJPanel.getListMessageTable().moveColumn(columnCount, columnIndx);
		}	
	}
	

	/**
	 * Column action perform
	 * @param evt
	 */
	public void jMenuItemColumnApplicationActionPerformed(boolean selected) 
	{
		// If there is no table, exit. The search criteria is only created when a table is selected.
		if (messageListTableJPanel.getListMessageTable().getSearchCriteria() == null) {
			return;
		}
		
		int columnIndx = 2;
		
		if (false == selected) {
			// Might throw if the column is not active
			try {
				messageListTableJPanel.getListMessageTable().removeColumn(messageListTableJPanel.getListMessageTable().getColumn("Application"));	
			}
			catch (IllegalArgumentException e) { } // Do nothing			
			columnsEnabled[columnIndx] = false;
		}
		else {			
			columnsEnabled[columnIndx] = true;

			// Loop thorugh the array of booleans until this column position. Count how many
			// columns are active. This gives us the index where to place this column to.
			int columnSlot = 1;
			if (columnsEnabled[0] == true) {
				++columnSlot;
			}
			TableColumn column = new TableColumn(columnIndx);
			int columnCount = messageListTableJPanel.getListMessageTable().getColumnCount();
			messageListTableJPanel.getListMessageTable().addColumn(column);	
			messageListTableJPanel.getListMessageTable().moveColumn(columnCount, columnSlot);
		}	
	}
	

	/**
	 * Column action perform
	 * @param evt
	 */
	public void jMenuItemColumnIssuedActionPerformed(boolean selected) 
	{
		// If there is no table, exit. The search criteria is only created when a table is selected.
		if (messageListTableJPanel.getListMessageTable().getSearchCriteria() == null) {
			return;
		}
		
		int columnIndx = 3;
		
		if (false == selected) {
			// Might throw if the column is not active
			try {
				messageListTableJPanel.getListMessageTable().removeColumn(messageListTableJPanel.getListMessageTable().getColumn("Issued"));	
			}
			catch (IllegalArgumentException e) { } // Do nothing			
			columnsEnabled[columnIndx] = false;
		}
		else {			
			columnsEnabled[columnIndx] = true;

			// Loop thorugh the array of booleans until this column position. Count how many
			// columns are active. This gives us the index where to place this column to.
			int columnSlot = 1;
			for (int i = 1; i < columnIndx; ++i) {
				if (columnsEnabled[i] == true) {
					++columnSlot;
				}
			}			
			TableColumn column = new TableColumn(columnIndx);
			int columnCount = messageListTableJPanel.getListMessageTable().getColumnCount();
			messageListTableJPanel.getListMessageTable().addColumn(column);	
			messageListTableJPanel.getListMessageTable().moveColumn(columnCount, columnSlot);
		}	
	}	
	
	/**
	 * Column action perform
	 * @param evt
	 */
	public void jMenuItemColumnSeverityActionPerformed(boolean selected) 
	{
		// If there is no table, exit. The search criteria is only created when a table is selected.
		if (messageListTableJPanel.getListMessageTable().getSearchCriteria() == null) {
			return;
		}
		
		int columnIndx = 4;
		
		if (false == selected) {
			// Might throw if the column is not active
			try {
				messageListTableJPanel.getListMessageTable().removeColumn(messageListTableJPanel.getListMessageTable().getColumn("Severity"));	
			}
			catch (IllegalArgumentException e) { } // Do nothing			
			columnsEnabled[columnIndx] = false;
		}
		else {			
			columnsEnabled[columnIndx] = true;

			// Loop thorugh the array of booleans until this column position. Count how many
			// columns are active. This gives us the index where to place this column to.
			int columnSlot = 1;
			for (int i = 1; i < columnIndx; ++i) {
				if (columnsEnabled[i] == true) {
					++columnSlot;
				}
			}			
			TableColumn column = new TableColumn(columnIndx);
			int columnCount = messageListTableJPanel.getListMessageTable().getColumnCount();
			messageListTableJPanel.getListMessageTable().addColumn(column);	
			messageListTableJPanel.getListMessageTable().moveColumn(columnCount, columnSlot);
		}	
	}	
	
	
	/**
	 * Column action perform
	 * @param evt
	 */
	public void jMenuItemColumnMsgIDActionPerformed(boolean selected) 
	{
		// If there is no table, exit. The search criteria is only created when a table is selected.
		if (messageListTableJPanel.getListMessageTable().getSearchCriteria() == null) {
			return;
		}
		
		int columnIndx = 5;
		
		if (false == selected) {
			// Might throw if the column is not active
			try {
				messageListTableJPanel.getListMessageTable().removeColumn(messageListTableJPanel.getListMessageTable().getColumn("Msg Id"));	
			}
			catch (IllegalArgumentException e) { } // Do nothing			
			columnsEnabled[columnIndx] = false;
		}
		else {			
			columnsEnabled[columnIndx] = true;

			// Loop thorugh the array of booleans until this column position. Count how many
			// columns are active. This gives us the index where to place this column to.
			int columnSlot = 1;
			for (int i = 1; i < columnIndx; ++i) {
				if (columnsEnabled[i] == true) {
					++columnSlot;
				}
			}			
			TableColumn column = new TableColumn(columnIndx);
			int columnCount = messageListTableJPanel.getListMessageTable().getColumnCount();
			messageListTableJPanel.getListMessageTable().addColumn(column);	
			messageListTableJPanel.getListMessageTable().moveColumn(columnCount, columnSlot);
		}	
	}
	
	/**
	 * Column action perform
	 * @param evt
	 */
	public void jMenuItemColumnMsgActionPerformed(boolean selected) 
	{
		// If there is no table, exit. The search criteria is only created when a table is selected.
		if (messageListTableJPanel.getListMessageTable().getSearchCriteria() == null) {
			return;
		}
		
		int columnIndx = 6;
		
		if (false == selected) {
			// Might throw if the column is not active
			try {
				messageListTableJPanel.getListMessageTable().removeColumn(messageListTableJPanel.getListMessageTable().getColumn("Message"));	
			}
			catch (IllegalArgumentException e) { } // Do nothing			
			columnsEnabled[columnIndx] = false;			
		}
		else {			
			columnsEnabled[columnIndx] = true;

			// Loop thorugh the array of booleans until this column position. Count how many
			// columns are active. This gives us the index where to place this column to.
			int columnSlot = 1;
			for (int i = 1; i < columnIndx; ++i) {
				if (columnsEnabled[i] == true) {
					++columnSlot;
				}
			}
			TableColumn column = new TableColumn(columnIndx);
			int columnCount = messageListTableJPanel.getListMessageTable().getColumnCount();
			messageListTableJPanel.getListMessageTable().addColumn(column);	
			messageListTableJPanel.getListMessageTable().moveColumn(columnCount, columnSlot);
		}	
	}
	
	
	/**
	 * Column action perform
	 * @param evt
	 */
	public void jMenuItemColumnParamActionPerformed(boolean selected) 
	{
		// If there is no table, exit. The search criteria is only created when a table is selected.
		if (messageListTableJPanel.getListMessageTable().getSearchCriteria() == null) {
			return;
		}
		
		int columnIndx = 7;
		if (false == selected) {
			// Might throw if the column is not active
			try {
				messageListTableJPanel.getListMessageTable().removeColumn(messageListTableJPanel.getListMessageTable().getColumn("Param"));	
			}
			catch (IllegalArgumentException e) { } // Do nothing			
			columnsEnabled[columnIndx] = false;
		}
		else {			
			columnsEnabled[columnIndx] = true;

			// Loop thorugh the array of booleans until this column position. Count how many
			// columns are active. This gives us the index where to place this column to.
			int columnSlot = 1;
			for (int i = 1; i < columnIndx; ++i) {
				if (columnsEnabled[i] == true) {
					++columnSlot;
				}
			}
			
			TableColumn column = new TableColumn(columnIndx);
			int columnCount = messageListTableJPanel.getListMessageTable().getColumnCount();
			messageListTableJPanel.getListMessageTable().addColumn(column);		
			messageListTableJPanel.getListMessageTable().moveColumn(columnCount, columnSlot);			
		}
	}	
	
	
	/**
	 * Column action perform
	 * @param evt
	 */
	public void jMenuItemColumnOptParamActionPerformed(boolean selected) 
	{
		// If there is no table, exit. The search criteria is only created when a table is selected.
		if (messageListTableJPanel.getListMessageTable().getSearchCriteria() == null) {
			return;
		}
		
		int columnIndx = 8;

		if (false == selected) {
			// Might throw if the column is not active
			try {
				messageListTableJPanel.getListMessageTable().removeColumn(messageListTableJPanel.getListMessageTable().getColumn("Opt Param"));	
			}
			catch (IllegalArgumentException e) { } // Do nothing			
			columnsEnabled[columnIndx] = false;
		}
		else {			
			columnsEnabled[columnIndx] = true;

			// Loop thorugh the array of booleans until this column position. Count how many
			// columns are active. This gives us the index where to place this column to.
			int columnSlot = 1;
			for (int i = 1; i < columnIndx; ++i) {
				if (columnsEnabled[i] == true) {
					++columnSlot;
				}
			}
			
			TableColumn column = new TableColumn(columnIndx);
			int columnCount = messageListTableJPanel.getListMessageTable().getColumnCount();
			messageListTableJPanel.getListMessageTable().addColumn(column);	
			messageListTableJPanel.getListMessageTable().moveColumn(columnCount, columnSlot);
		}	
	}	
	
	/**
	 * Column action perform
	 * @param evt
	 */
	public void jMenuItemColumnQualifiersActionPerformed(boolean selected) 
	{
		// If there is no table, exit. The search criteria is only created when a table is selected.
		if (messageListTableJPanel.getListMessageTable().getSearchCriteria() == null) {
			return;
		}
		
		int columnIndx = 9;
		
		if (false == selected) {
			// Might throw if the column is not active
			try {
				messageListTableJPanel.getListMessageTable().removeColumn(messageListTableJPanel.getListMessageTable().getColumn("Qualifiers"));	
			}
			catch (IllegalArgumentException e) { } // Do nothing			
			columnsEnabled[columnIndx] = false;			
		}
		else {			
			columnsEnabled[columnIndx] = true;

			// Loop thorugh the array of booleans until this column position. Count how many
			// columns are active. This gives us the index where to place this column to.
			int columnSlot = 1;
			for (int i = 1; i < columnIndx; ++i) {
				if (columnsEnabled[i] == true) {
					++columnSlot;
				}
			}
			TableColumn column = new TableColumn(columnIndx);
			int columnCount = messageListTableJPanel.getListMessageTable().getColumnCount();
			messageListTableJPanel.getListMessageTable().addColumn(column);	
			messageListTableJPanel.getListMessageTable().moveColumn(columnCount, columnSlot);
		}	
	}		
	
	/**
	 * this class permits to specify the authorized extensions of the file that will be saved 
	 * @author cbiau
	 *
	 */
	public class FiltreSimple extends FileFilter
	{
		   //Description and extension accepted by the filter
		   private String description;
		   private String extension;
		   //Constructor with the description and the accepted extension 
		   public FiltreSimple(String description, String extension){
		      if(description == null || extension ==null){
		         throw new NullPointerException("The description (or extension) cannot be NULL.");
		      }
		      this.description = description;
		      this.extension = extension;
		   }
		   //Implementation of FileFilter
		   @Override
        public boolean accept(File file){
		      if(file.isDirectory()) { 
		         return true; 
		      } 
		      String nomFichier = file.getName().toLowerCase(); 

		      return nomFichier.endsWith(extension);
		   }
		   @Override
        public String getDescription()
		   {
		      return description;
		   }
	}
	
	/**
	 * Return the default font pane.
	 * 
	 * @return the default font.
	 */
	public Font getDefaultFont() {
		return defaultFont;
		
	}

	public HashMap<String, String> getPartition2Table() {
		return partition2Table;
	}

	public void setPartition2Table(HashMap<String, String> partition2Table) {
		this.partition2Table = partition2Table;
	}

	public boolean[] getColumnEnabled() {
		return columnsEnabled;
	}

	public JMenuItem getJMenuItemApplication() {
		return jMenuItemApplication;
	}

	public JMenuItem getJMenuItemHost() {
		return jMenuItemHost;
	}

	public JMenuItem getJMenuItemIssued() {
		return jMenuItemIssued;
	}

	public JMenuItem getJMenuItemMessage() {
		return jMenuItemMessage;
	}

	public JMenuItem getJMenuItemMsgID() {
		return jMenuItemMsgID;
	}

	public JMenuItem getJMenuItemParam() {
		return jMenuItemParam;
	}
	
	public JMenuItem getJMenuItemOptParam() {
		return jMenuItemOptParam;
	}
	
	public JMenuItem getJMenuItemQualifiers() {
		return jMenuItemQualifiers;
	}	

	public JMenuItem getJMenuItemSeverity() {
		return jMenuItemSeverity;
	}
	
	public boolean getCaseInsensitiveSearch() {
		return jMenuItemCaseSensitive.isSelected();
	}
	
	public JCheckBoxMenuItem getJMenuItemVerbosity() {
		return jMenuItemVerbosity;
	}

	public int getMaxMsgsDisplayed() {
		return maxMsgsDisplayed;
	}

	public void setMaxMsgsDisplayed(int value) {
		maxMsgsDisplayed = value;
	}

	public String getDatabaseConnection() {
		return databaseConnection;
	}

	public void setDatabaseConnection(String databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	public String getDatabasePass() {
		return databasePass;
	}

	public void setDatabasePass(String databasePass) {
		this.databasePass = databasePass;
	}

	public String getDatabaseUser() {
		return databaseUser;
	}

	public void setDatabaseUser(String databaseUser) {
		this.databaseUser = databaseUser;
	}

	public String getDbSource() {
		return dbSource;
	}
	
	public SearchTreeJPanel getSearchTree() {
		return searchTreeJPanel;
	}
	
	public boolean getTimeZoneGeneva() {
		return timeZoneGeneva;
	}

	public long getTableTreeStartTime()
	{
		return tableTreeStartTime;
	}
	public String getCurrentTdaqRelease()
	{
		return currentTdaqRelease;
	}
	public boolean[] getColumnsEnabled() { return columnsEnabled; }
}


