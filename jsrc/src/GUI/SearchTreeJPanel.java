package GUI;

/**
 * this class permits to the user to make a search on different criteria
 * it contains the graphical interface for it and the search functions 
 */
import java.lang.String;
import java.lang.Integer;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;

import GUI.InfoReporting;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class SearchTreeJPanel extends javax.swing.JPanel {

	private static final long serialVersionUID = -1570550093506542161L;
	JFrameMain jFrameMain;
	private JButton quickSearchButton;
	private JTextField partitionUser;
	private JTextField runNumber;
	
	public SearchTreeJPanel(JFrameMain frameMain) {
		super();
		
		jFrameMain = frameMain;
		initGUI();
	}
	
	private void initGUI() {
		try {
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {0.33, 0.33, 0.33};
			thisLayout.rowHeights = new int[] {20, 20, 20};
			thisLayout.columnWeights = new double[] {0, 1};
			thisLayout.columnWidths = new int[] {90, 150};
			this.setLayout(thisLayout);
			this.setBackground(new java.awt.Color(117,117,117));
			this.setBorder(BorderFactory.createEtchedBorder(BevelBorder.LOWERED));
			this.setForeground(new java.awt.Color(255,255,255));
			this.setMinimumSize(new Dimension(1000, 0));
			{
				JLabel partuser = new JLabel("Partition.User:");
				partuser.setForeground(Color.white);
				this.add(partuser, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 1, 0, 0), 0, 0));											
			}
			{
				partitionUser = new JTextField("ATLAS.crrc");
			/*	partitionUser.addMouseListener(new MouseAdapter() {
					public void mousePressed(MouseEvent evt) {
						if (0 == partitionUser.getText().compareTo("Partition.User")) {
							partitionUser.setText("");
						}	
					}
				});	*/				
				this.add(partitionUser, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 1), 0, 0));				
			}			
			{
				JLabel rn = new JLabel("Run Number:");
				rn.setForeground(Color.white);
				this.add(rn, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 2, 0, 0), 0, 0));
			}
			{
				runNumber = new JTextField("");
				this.add(runNumber, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 1), 0, 0));				
			}

			{
				quickSearchButton = new JButton();
				quickSearchButton.setText("Search");
				quickSearchButton.setFont(new java.awt.Font("Lucida Sans",1,8));
				quickSearchButton.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent evt) {
						if (jFrameMain.getTableTreeJPanel().getTableTree().getRefreshingPartitionList() == true) {
							InfoReporting.printWarning("This action is not allowed whilst the Partition list is being refreshed.");
						}
						else {
							QuickSearchButtonActionPerformed();
						}
					}
				});	
				this.add(quickSearchButton, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(2, 10, 0, 0), 0, 0));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}		
	
	/**
	 * Narrow down partition, user, run number triad.
	 * @param evt
	 */	
	public void QuickSearchButtonActionPerformed() {
		int rn = -1;
		if (false == runNumber.getText().isEmpty()) {
			try {
				rn = Integer.parseInt(runNumber.getText().trim());
				if (rn < 0) {
					InfoReporting.printError("Wrong input: the Run Number must be larger than or equal to 0.");
					return;					
				}
			}
			catch (NumberFormatException e) {
				InfoReporting.printError("Wrong input: the Run Number must be a digit.");
				return;
			}		
		}
				
		String partUserStr = partitionUser.getText().trim();
		String partition = "";
		String user = "";		

		int indx1 = partUserStr.indexOf(".");
		if (-1 != indx1) {
			if (indx1 != partUserStr.lastIndexOf(".")) {
				InfoReporting.printError("Wrong input: make sure to follow the format Partition.User");
				return;			
			}
			partition = partUserStr.substring(0, indx1);
			user = partUserStr.substring(indx1+1, partUserStr.length());
		}
		else {
			partition = partUserStr;
		}
		jFrameMain.getTableTreeJPanel().goToObject(partition, user, rn);
	}
	
	public void update(String partition, String user, long rn) {
		String paritionUser = "";
		if (partition.length() != 0) {
			paritionUser = partition;
			if (user.length() != 0) {
				paritionUser += "." + user;
			}
		}		
		String rnStr = "";
		if (-1 != rn) {
			rnStr = Long.toString(rn);
		}
		
		// Update values
		partitionUser.setText(paritionUser);
		runNumber.setText(rnStr);
	}
}
