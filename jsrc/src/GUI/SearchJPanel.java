package GUI;

/**
 * this class permits to the user to make a search on different criteria
 * it contains the graphical interface for it and the search functions 
 */
import java.lang.String;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.JScrollPane;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import GUI.SearchCriteria;
import GUI.InfoReporting;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class SearchJPanel extends javax.swing.JPanel {

	private static final long serialVersionUID = -1570550093506542161L;
	JFrameMain jFrameMain;
	private JButton searchjButton;
	private JButton quickSearchButton;
		
	private JLabel     messageLabel;
	private JTextField messageTextField;	
	private JLabel     hostLabel;
	private JTextField hostTextField;
	private JLabel     qualifiersLabel;
	private JTextField qualifiersTextField;	
	private JLabel     applicationLabel;
	private JTextField applicationTextField;		
	private JLabel     paramLabel;
	private JTextField paramTextField;		
	private JLabel     optionalLabel;
	private JTextField optionalParamTextField;	
	private JLabel     messageIDLabel;
	private JTextField messageIDTextField;
	
	private JSpinner         issuedFrom;
	private JSpinner         issuedTo;
	private SpinnerDateModel dateModelFrom;
	private SpinnerDateModel dateModelTo;
	private JList            jList1;
	private JPopupMenu       enableMenu; 
	private Date             dateNow;
	private long             timeZoneOffset;
	
	private MouseEvent lastExpressionEvent;
	private JPopupMenu expressionPopup;
	private JMenuItem  expressionItem;

	private int tmp = 0;
	public SearchJPanel(JFrameMain frameMain) {
		super();
		
		jFrameMain = frameMain;
		initGUI();
	}
	
	//@SuppressWarnings("deprecation")
	private void initGUI() {
		try 
		{
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {0.33, 0.33, 0.33};
			thisLayout.rowWeights = new double[] {0.33, 0.33, 0.33};
			thisLayout.rowHeights = new int[] {20, 20, 20};
			thisLayout.columnWeights = new double[] {0.0, 0.2, 0.0, 0.2, 0.0, 0.2, 0.0};
			thisLayout.columnWidths = new int[] {85, 215, 55, 215, 75, 215, 135};
			this.setLayout(thisLayout);
			this.setBackground(new java.awt.Color(117,117,117));
			this.setBorder(BorderFactory.createEtchedBorder(BevelBorder.LOWERED));
		//	this.setFont(jFrameMain.defaultFont); 
			this.setForeground(new java.awt.Color(255,255,255));
			this.setMinimumSize(new Dimension(1000, 0));
//			this.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0,0,0)));			

			{
				messageLabel = new JLabel("Message:");
				messageLabel.setForeground(Color.white);
				this.add(messageLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 2, 0, 0), 0, 0));
			}
			{
				messageTextField = new JTextField();
				this.add(messageTextField, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 1), 0, 0));
			}			
			{
				hostLabel = new JLabel("Host:");
				hostLabel.setForeground(Color.white);
				this.add(hostLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));				
			}	
			{
				hostTextField = new JTextField();
				this.add(hostTextField, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 1), 0, 0));
			}			
			{
				qualifiersLabel = new JLabel("Qualifiers:");
				qualifiersLabel.setForeground(Color.white);
				this.add(qualifiersLabel, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));								
			}
			{
				qualifiersTextField = new JTextField();
				this.add(qualifiersTextField, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 1), 0, 0));
			}			
			{			
				String[] data = {"Fatal", "Error", "Warning", "Information"};				
				int[] selectedIndx = {0, 1};
				jList1 = new JList(data);
				jList1.setDragEnabled(true);
				jList1.setSelectedIndices(selectedIndx);
				JScrollPane scrollPane = new JScrollPane(jList1);				
				this.add(scrollPane, new GridBagConstraints(6, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 2, 0), 0, 0));					
			}				
			{
				applicationLabel = new JLabel("Application:");
				applicationLabel.setForeground(Color.white);
				this.add(applicationLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 2, 0, 0), 0, 0));				
			}		
			{
				applicationTextField = new JTextField();
				this.add(applicationTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 1), 0, 0));
			}				
			{
				paramLabel = new JLabel("Param:");
				paramLabel.setForeground(Color.white);
				this.add(paramLabel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));				
			}
			{
				paramTextField = new JTextField();
				this.add(paramTextField, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 1), 0, 0));
			}
			
			{
				optionalLabel = new JLabel("Opt Param:");
				optionalLabel.setForeground(Color.white);
				this.add(optionalLabel, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));								
			}
			{
				optionalParamTextField = new JTextField();
				this.add(optionalParamTextField, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 1), 0, 0));
			}		
			{
				messageIDLabel = new JLabel("Message  ID:");
				messageIDLabel.setForeground(Color.white);
				this.add(messageIDLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 2, 0, 0), 0, 0));				
			}
			{
				messageIDTextField = new JTextField();
				this.add(messageIDTextField, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 1), 0, 0));
			}	
			{
				enableMenu = new JPopupMenu();
				final JMenuItem enableItem = new JMenuItem("Disable time");	
				enableMenu.add(enableItem);				
				enableItem.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseReleased(MouseEvent evt) {
						if (true == issuedFrom.isEnabled()) {
							issuedFrom.setEnabled(false);
							issuedTo.setEnabled(false);
							enableItem.setText("Enable time");
						}
						else {
							issuedFrom.setEnabled(true);
							issuedTo.setEnabled(true);
							enableItem.setText("Disable time");
						}	
					}						
				});				
			}
			{
				JLabel issuedFrom = new JLabel("From:");
				issuedFrom.setForeground(Color.white);						
				issuedFrom.addMouseListener(new MouseAdapter() {
					@Override
                    public void mousePressed(MouseEvent evt) {
						if(SwingUtilities.isRightMouseButton(evt)) {
							enableMenu.show(evt.getComponent(), evt.getX(), evt.getY());
						}
					}	
				});				
				this.add(issuedFrom, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));				
			}
			
			{
				dateModelFrom = new SpinnerDateModel(new Date(), null, null, Calendar.HOUR_OF_DAY);
				issuedFrom = new JSpinner(dateModelFrom);
				issuedFrom.setEditor(new JSpinner.DateEditor(issuedFrom, "dd MMM yyyy HH:mm:ss"));
				this.add(issuedFrom, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(1, 0, 1, 1), 0, 0));
			}
			{				
				JLabel issuedFrom = new JLabel("To:");
				issuedFrom.setForeground(Color.white);						
				issuedFrom.addMouseListener(new MouseAdapter() {
					@Override
                    public void mousePressed(MouseEvent evt) {
						if(SwingUtilities.isRightMouseButton(evt)) {
							enableMenu.show(evt.getComponent(), evt.getX(), evt.getY());
						}
					}	
				});				
				this.add(issuedFrom, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));				
			}
			{
				dateModelTo = new SpinnerDateModel(new Date(), null, null, Calendar.HOUR_OF_DAY);
				issuedTo = new JSpinner(dateModelTo);			
				issuedTo.setEditor(new JSpinner.DateEditor(issuedTo, "dd MMM yyyy HH:mm:ss"));				
				this.add(issuedTo, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(1, 1, 0, 1), 0, 0));				
			}
			
			setSpinnerTime();
			
			{
				searchjButton = new JButton();
				searchjButton.setText("Filter");
				searchjButton.setFont(new java.awt.Font("Lucida Sans",1,8));
				searchjButton.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent evt) {
						SearchjButtonActionPerformed();
					}
				});
				// Don't let he user click unless there is content to form the search criteria.
				searchjButton.setVisible(false); 
				this.add(searchjButton, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 2, 0), 0, 0));				
			}
			
		    class PopupListener extends MouseAdapter {
		    	@Override
                public void mousePressed(MouseEvent e) {
		    		maybeShowPopup(e);
		    	}

		    	@Override
                public void mouseReleased(MouseEvent e) {
		    		maybeShowPopup(e);
		    	}

		    	private void maybeShowPopup(MouseEvent e) {		    		
		    		if (e.isPopupTrigger()) {	    			
		    			expressionPopup.show(e.getComponent(), e.getX(), e.getY());
		    			lastExpressionEvent = e;
		    		}
		    	}
		    }
			{			
		    	expressionPopup = new JPopupMenu();
				expressionItem  = new JMenuItem("Literal expression"); 
				expressionPopup.add(expressionItem);
				expressionItem.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseReleased(MouseEvent evt) {
						lastExpressionEvent.getComponent().setForeground(Color.white);
					}					
				});					

				expressionItem = new JMenuItem("Regular expression");
				expressionPopup.add(expressionItem);
				expressionItem.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseReleased(MouseEvent evt) {						
						lastExpressionEvent.getComponent().setForeground(new Color(184, 207, 229));
					}					
				});					

			    MouseListener popupListener = new PopupListener();
			    messageLabel.addMouseListener(popupListener);
			    hostLabel.addMouseListener(popupListener);
			    qualifiersLabel.addMouseListener(popupListener);
			    applicationLabel.addMouseListener(popupListener);
			    paramLabel.addMouseListener(popupListener);
			    optionalLabel.addMouseListener(popupListener);
			    messageIDLabel.addMouseListener(popupListener);			    
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public JButton getSearchjButton() { return searchjButton; } 	

	
	/**
	 * Get the search criteria
	 * @param evt
	 */	
	public void SearchjButtonActionPerformed() {
		// Reset the pagination values
		jFrameMain.getMessageListTableJPanel().getListMessageTable().getSearchCriteria().setLimitFrom(0);
		jFrameMain.getMessageListTableJPanel().getListMessageTable().getSearchCriteria().setLimitTo(jFrameMain.getMaxMsgsDisplayed());
		jFrameMain.getMessageListTableJPanel().getPaginationJPanel().setIndxPage(0);		
		
		jFrameMain.getMessageListTableJPanel().getListMessageTable().refreshTable();
	}	
	
	/**
	 * Get the search criteria
	 * @param evt
	 */
	public void SearchjButtonActionPerformed(SearchCriteria criteria) 
	{
		// At least one severity level must be selected 
		if (jList1.isSelectionEmpty() == true) {
			InfoReporting.printError("At least one <b>Severity</b> type must be selected.");
			return;
		}
		// Make sure the From is smaller than the To
		if (dateModelTo.getDate().getTime() < dateModelFrom.getDate().getTime()) {
			InfoReporting.printError("The <b>From</b> date value must be smaller than the <b>To</b> date value.");
			return;
		}
				
		if (hostTextField.getText().isEmpty()) {
			criteria.setHost("", false);
		}
		else {
			criteria.setHost(hostTextField.getText(), Color.white != hostLabel.getForeground());
		}

		if (applicationTextField.getText().isEmpty()) {
			criteria.setApplication("", false);
		}
		else {
			criteria.setApplication(applicationTextField.getText(), Color.white != applicationLabel.getForeground());
		}				
	
		// Make sure the times were modified		
		if (dateNow.equals(issuedFrom.getValue()) || false == issuedFrom.isEnabled()) {
			criteria.setIssuedFrom(-1);						
		}
		else {
			long time = dateModelFrom.getDate().getTime();
			criteria.setIssuedFrom((time - timeZoneOffset)/1000);
		}
		
		if (dateNow.equals(issuedTo.getValue()) || false == issuedFrom.isEnabled()) {		
			criteria.setIssuedTo(-1);		
		}
		else {			
			long time = dateModelTo.getDate().getTime();
			criteria.setIssuedTo((time - timeZoneOffset)/1000);
		}	

		criteria.setSev(jList1.getSelectedIndices());
		
		if (messageIDTextField.getText().isEmpty() ) {		   
			criteria.setMsgID("", false);
		}
		else {
			criteria.setMsgID(messageIDTextField.getText(), Color.white != messageIDLabel.getForeground());
		}
		
		if (messageTextField.getText().isEmpty() ) {		   
			criteria.setMsg("", false);
		}
		else {
			criteria.setMsg(messageTextField.getText(), Color.white != messageLabel.getForeground());
		}
			
		if (paramTextField.getText().isEmpty() ) {		   
			criteria.setParam("", false);
		}
		else {
			criteria.setParam(paramTextField.getText(), Color.white != paramLabel.getForeground());
		}
		
		if (optionalParamTextField.getText().isEmpty() ) {		   
			criteria.setOptParam("", false);
		}
		else {
			criteria.setOptParam(optionalParamTextField.getText(), Color.white != optionalLabel.getForeground());
		}		
		
		if (qualifiersTextField.getText().isEmpty() ) {		   
			criteria.setQualifiers("", false);
		}
		else {
			criteria.setQualifiers(qualifiersTextField.getText(), Color.white != qualifiersLabel.getForeground());
		}		
	
		criteria.setLimitFrom(criteria.getLimitFrom());
		criteria.setLimitTo(criteria.getLimitTo());
	}
	
	public void setSpinnerTime()
	{
		TimeZone tzGva    = TimeZone.getTimeZone("Europe/Zurich");				
		Calendar calGva   = new GregorianCalendar(tzGva);				
		TimeZone tzLocal  = TimeZone.getDefault();
		Calendar calLocal = new GregorianCalendar(tzLocal);

		timeZoneOffset = 0;
		if (true == jFrameMain.getTimeZoneGeneva()) {			
			timeZoneOffset = (calGva.get(Calendar.ZONE_OFFSET)   + calGva.get(Calendar.DST_OFFSET)) -
			                 (calLocal.get(Calendar.ZONE_OFFSET) + calLocal.get(Calendar.DST_OFFSET));
		}

		long tnow = new Date().getTime() + timeZoneOffset;
		dateNow = new Date(tnow);
		dateModelFrom.setValue(dateNow);
		dateModelTo.setValue(dateNow);		
	}
}
