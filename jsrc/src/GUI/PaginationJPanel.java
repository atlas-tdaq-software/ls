package GUI;

/**
 * this class is the pagination for the table
 * it permits to navigate in the different pages of the messages
 */

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class PaginationJPanel extends javax.swing.JPanel 
{	
	private static final long serialVersionUID = 7384351780808085132L;
	
//	public static StoreTableContent storeTableContent;
	public static MessageListTable messageListTable;
	
	private JButton jButtonFirst;
	private JButton jButtonNext;
	private JButton jButtonLast;
	private JButton jButtonPrevious;
	private JTextField pageInfo;
	private JProgressBar progressBar;
	private JLabel miscInfo;
	private int indxPage;
	private JFrameMain jFrameMain; 

	public PaginationJPanel(JFrameMain frameMain) {
		super();

		jFrameMain = frameMain;
		try {
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {0.0};
			thisLayout.rowHeights = new int[] {10};
			thisLayout.columnWeights = new double[] {0.0, 0.0, 0.1, 0.1, 0.0, 0.0, 0.0};
			thisLayout.columnWidths = new int[] {30, 30, 150, 100, 50, 30, 30};
			this.setLayout(thisLayout);
			{
				jButtonFirst = new JButton();
				this.add(jButtonFirst, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
				jButtonFirst.setIcon(new ImageIcon(getClass().getClassLoader().getResource("jsrc/src/GUI/data/icone_first.gif")));
				jButtonFirst.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent evt) {
						jButtonFirstActionPerformed(evt);
					}
				});
			}
			{
				jButtonPrevious = new JButton();
				this.add(jButtonPrevious, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
				jButtonPrevious.setIcon(new ImageIcon(getClass().getClassLoader().getResource("jsrc/src/GUI/data/icone_previous.gif")));
				jButtonPrevious.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent evt) {
						jButtonPreviousActionPerformed(evt);
					}
				});
			}
			{
				miscInfo = new JLabel();
				miscInfo.setHorizontalAlignment(SwingConstants.CENTER);
				miscInfo.setBackground(new java.awt.Color(240,240,240));
				miscInfo.setForeground(new java.awt.Color(0,60,255));					
				this.add(miscInfo, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));				
			}			
			{
				pageInfo = new JTextField();
				pageInfo.setHorizontalAlignment(SwingConstants.CENTER);
				pageInfo.setBackground(new java.awt.Color(103,103,103));
				pageInfo.setForeground(new java.awt.Color(255,255,255));	
				this.add(pageInfo, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));				
			}
			{
				progressBar = new JProgressBar(0, 100);
				progressBar.setBackground(new java.awt.Color(230,230,230));
				progressBar.setForeground(new java.awt.Color(0,0,0));	
				this.add(progressBar, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
			}			
			{
				jButtonNext = new JButton();
				this.add(jButtonNext, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
				jButtonNext.setIcon(new ImageIcon(getClass().getClassLoader().getResource("jsrc/src/GUI/data/icone_next.gif")));
				jButtonNext.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent evt) {
						goToNextPage();
					}
				});
			}
			{
				jButtonLast = new JButton();
				this.add(jButtonLast, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
				jButtonLast.setIcon(new ImageIcon(getClass().getClassLoader().getResource("jsrc/src/GUI/data/icone_last.gif")));
				jButtonLast.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent evt) {
						jButtonLastActionPerformed(evt);
					}
				});
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		indxPage = 0;
	}
	
	
	/**
	 * this function is the action of the button first
	 * @param evt
	 */
	private void jButtonFirstActionPerformed(ActionEvent evt) {
		int numberOfPages = messageListTable.getNumberOfPages();
		if (numberOfPages > 1 && indxPage > 0) {
			indxPage = 0;
			int maxMsgPerTable = jFrameMain.getMaxMsgsDisplayed();
			messageListTable.getSearchCriteria().setLimitFrom(indxPage * maxMsgPerTable);
			messageListTable.getSearchCriteria().setLimitTo((indxPage + 1) * maxMsgPerTable);
			messageListTable.refreshTable();
		}
	}
	
	/**
	 * this function is the action of the button previous
	 * @param evt
	 */
	private void jButtonPreviousActionPerformed(ActionEvent evt) {
		int numberOfPages = messageListTable.getNumberOfPages();
		if (numberOfPages > 1 && indxPage > 0) {
			--indxPage;
			int maxMsgPerTable = jFrameMain.getMaxMsgsDisplayed();
			messageListTable.getSearchCriteria().setLimitFrom(indxPage * maxMsgPerTable);
			messageListTable.getSearchCriteria().setLimitTo((indxPage + 1) * maxMsgPerTable);
			messageListTable.refreshTable();
		}
	}
	
	/**
	 * this function is the action of the button next
	 * @param evt
	 */
	public boolean goToNextPage()
	{
		boolean retval = false;
		int numberOfPages = messageListTable.getNumberOfPages();
		if (numberOfPages > 1 && indxPage < (numberOfPages-1)) {
			++indxPage;
			int maxMsgPerTable = jFrameMain.getMaxMsgsDisplayed();
			messageListTable.getSearchCriteria().setLimitFrom(indxPage * maxMsgPerTable);
			messageListTable.getSearchCriteria().setLimitTo((indxPage + 1) * maxMsgPerTable);			
			messageListTable.refreshTable();
			retval = true;
		}		
		return retval;
	}
	
	public void goToFirstPage()
	{
		indxPage = 0;
		int maxMsgPerTable = jFrameMain.getMaxMsgsDisplayed();
		messageListTable.getSearchCriteria().setLimitFrom(indxPage * maxMsgPerTable);
		messageListTable.getSearchCriteria().setLimitTo((indxPage + 1) * maxMsgPerTable);
		messageListTable.refreshTable();	
	}
		
	/**
	 * this function is the action of the button last
	 * @param evt
	 */
	private void jButtonLastActionPerformed(ActionEvent evt) {
		int numberOfPages = messageListTable.getNumberOfPages();		
		if (numberOfPages > 1 && indxPage < numberOfPages) {
			indxPage = numberOfPages-1;
			int maxMsgPerTable = jFrameMain.getMaxMsgsDisplayed();
			messageListTable.getSearchCriteria().setLimitFrom(indxPage * maxMsgPerTable);
			messageListTable.getSearchCriteria().setLimitTo((indxPage + 1) * maxMsgPerTable);
			messageListTable.refreshTable();			
		}		
	}


	public JTextField getPageInfo() {
		return pageInfo;
	}


	public int getIndxPage() {
		return indxPage;
	}


	public void setIndxPage(int indxPage) {
		this.indxPage = indxPage;
	}


	public JProgressBar getProgressBar() {
		return progressBar;
	}


	public JLabel getMiscInfo() {
		return miscInfo;
	}


	public void setMiscInfo(JLabel miscInfo) {
		this.miscInfo = miscInfo;
	}
}
