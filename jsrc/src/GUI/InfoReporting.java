package GUI;

import common.db.DBConnectorException;
import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.BadLocationException;
import javax.swing.JCheckBoxMenuItem;


public class InfoReporting 
{
	static JTextPane pane;	
	static StyledDocument doc;
	
	static private Style styleSuccessTxt;
	static private Style styleInfoTxt;
	static private Style styleWarningTxt;	
	static private Style styleErrorTxt;	
	static private JCheckBoxMenuItem menuVerbosity;
	
	public InfoReporting()
	{	
	}
	
	static public void init(JFrameMain frameMain) {
		pane = frameMain.getContentDetailJPanel().getMessageContentjTextPane();
		doc = (StyledDocument)pane.getDocument();
		menuVerbosity = frameMain.getJMenuItemVerbosity();
			
		styleSuccessTxt = doc.addStyle("succesTxt", null);
		StyleConstants.setForeground(styleSuccessTxt, new java.awt.Color(0, 157, 0));
		
		styleInfoTxt = doc.addStyle("infoTxt", null);
		StyleConstants.setForeground(styleInfoTxt, Color.blue);		
		
		styleWarningTxt = doc.addStyle("warningTxt", null);
		StyleConstants.setForeground(styleWarningTxt, new java.awt.Color(255, 117, 0));
		
		styleErrorTxt = doc.addStyle("errorTxt", null);
		StyleConstants.setForeground(styleErrorTxt, Color.red);
	}
	
	
	/**
	 * Function to output an success message.
	 * @param success
	 */
	static public void printSuccess(String msg)
	{
		if (menuVerbosity.isSelected() == true) {	
			try {
				doc.insertString(0, "\n" + msg + "\n", styleSuccessTxt);
				pane.setCaretPosition(0);
			}
			catch(BadLocationException e) {	
			}
		}
	}	
	
	
	/**
	 * Function to output an information message.
	 * @param info
	 */
	static public void printInfo(String msg)
	{
		if (menuVerbosity.isSelected()  == true) {	
			try {
				doc.insertString(0, "\n" + msg + "\n", styleInfoTxt);
				pane.setCaretPosition(0);
			}
			catch(BadLocationException e) {	
			}
		}
	}		
	
	
	/**
	 * Function to output a warning message.
	 * @param warning
	 */
	static public void printWarning(String msg)
	{
		if (menuVerbosity.isSelected()  == true) {	
			try {
				doc.insertString(0, "\n" + msg + "\n", styleWarningTxt);	
				pane.setCaretPosition(0);
			}
			catch(BadLocationException e) {	
			}
		}		
	}
	
	
	/**
	 * Function to output an error message.
	 * @param error
	 */
	static public void printError(String msg)
	{	
		try {
			doc.insertString(0, "\n" + msg + "\n", styleErrorTxt);
			pane.setCaretPosition(0);
		}
		catch(BadLocationException e) {	
		}			
	}	


	/**
	 * Function to output an error from an exception.
	 * @param exception
	 */
	static public void printError(DBConnectorException dbException)
	{	
		String errorString = dbException.getMessage();
		Throwable cause = dbException;
		while((cause = cause.getCause()) != null) {
			errorString += "\n\twas caused by:" + cause.getMessage();
		}

		try {
			doc.insertString(0, errorString + "\n", styleErrorTxt);
			pane.setCaretPosition(0);
		}
		catch(BadLocationException e) {	
		}
	}	
}
