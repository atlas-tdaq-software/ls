package GUI;

/**
 * this class displays the table of messages
 * it has the mouse and key listeners
 */
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class MessageListTableJPanel extends javax.swing.JPanel {
	
	private static final long serialVersionUID = 1L;
	
	public final static Color[][] severityColors = new Color[][] {
		{ new java.awt.Color(0, 255, 127), new java.awt.Color(0, 0, 0) },  			//Background = green, foreground = black
		{ new java.awt.Color(255, 255, 0), new java.awt.Color(0, 0, 0) },  			//Background = yellow, foreground = black
		{ new java.awt.Color(0, 206, 209), new java.awt.Color(255, 255, 255) },  	//Background = turquoise, foreground = white
		{ new java.awt.Color(255, 140, 0), new java.awt.Color(255, 255, 255) },  	//Background = orange, foreground = white
		{ new java.awt.Color(255, 69, 0), new java.awt.Color(255, 255, 255) },  	//Background = dark orange, foreground = white
		{ new java.awt.Color(206, 0, 0), new java.awt.Color(255, 255, 255) },		//Background = dark red, foreground = white
		{ new java.awt.Color(206, 0, 0), new java.awt.Color(255, 255, 255) }};		//Background = dark red, foreground = white	
	
	
	public final static Color[][] severityColorsSelected = new Color[][] {
			{ new java.awt.Color(0, 232, 116), new java.awt.Color(0, 0, 0) },  			//Background = green, foreground = black
			{ new java.awt.Color(232, 232, 0), new java.awt.Color(0, 0, 0) },  			//Background = yellow, foreground = black
			{ new java.awt.Color(0, 174, 179), new java.awt.Color(255, 255, 255) },  	//Background = turquoise, foreground = white
			{ new java.awt.Color(234, 129, 0), new java.awt.Color(255, 255, 255) },  	//Background = orange, foreground = white
			{ new java.awt.Color(236, 65, 0), new java.awt.Color(255, 255, 255) },  	//Background = dark orange, foreground = white
			{ new java.awt.Color(166, 0, 0), new java.awt.Color(255, 255, 255) },		//Background = dark red, foreground = white
	{ new java.awt.Color(166, 0, 0), new java.awt.Color(255, 255, 255) }};		//Background = dark red, foreground = white	
		
	public final static String[] strSeverity = new String[] 
	                 { "Debug",  "Log", "Information", "Warning", "Error", "Fatal", "Undefined"};		
		
	private JScrollPane MessageListjScrollPane;
	private MessageListTable messageListTable;

	private JTableHeader header;	
	private JFrameMain jFrameMain;
	
	private PaginationJPanel paginationJPanel;
	
	JPopupMenu refreshMenu; 	
	
	private HashMap<String, String> head2column;
	private HashMap<String, String> orderDirection;

	
	public MessageListTableJPanel(JFrameMain frameMain) 
	{
		super();
		
		jFrameMain = frameMain;
		
		try {					
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {1.0, 0.0};
			thisLayout.rowHeights = new int[] {200, 20};
			thisLayout.columnWeights = new double[] {0.1};
			thisLayout.columnWidths = new int[] {7};
			this.setLayout(thisLayout);
			{
				messageListTable = new MessageListTable(jFrameMain);
				//messageListTable.setRowMargin(5);
				messageListTable.setGridColor(Color.GRAY);
				//messageListTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); 
				messageListTable.setDefaultRenderer(Object.class, 
                        new MessageListTableCellRenderer()); 
				messageListTable.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseClicked(MouseEvent evt) {
						MessageListTableMouseClicked(evt);
					}
					@Override
                    public void mousePressed(MouseEvent evt) {		
						if(SwingUtilities.isRightMouseButton(evt)) {
							refreshMenu.show(evt.getComponent(), evt.getX(), evt.getY());	
						}
					}				
				});

				messageListTable.addKeyListener(new KeyListener()  {
					@Override
                    public void keyPressed(KeyEvent evt) { 
						messageListTableKeyTyped(evt);
						 }
					@Override
                    public void keyReleased(KeyEvent evt) {	}

					@Override
                    public void keyTyped(KeyEvent evt) { }
				});
				
				// Sort by clicking on a column title
				header = messageListTable.getTableHeader();
			    header.setUpdateTableInRealTime(true);
			    header.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseClicked(MouseEvent evt) {
						HeaderMessageListTableMouseClicked(evt);
					}
				});
			    header.setReorderingAllowed(false);							

				MessageListjScrollPane = new JScrollPane();
				MessageListjScrollPane.setViewportView(messageListTable) ;
				MessageListjScrollPane.setBorder(new javax.swing.border.CompoundBorder(
												new javax.swing.border.CompoundBorder(
												new javax.swing.border.LineBorder(Color.gray),
												new javax.swing.border.EtchedBorder(javax.swing.border.EtchedBorder.RAISED, Color.white, Color.lightGray)
												),
												new javax.swing.border.LineBorder(Color.darkGray)));
				this.add(MessageListjScrollPane, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));	
			}
			{
				paginationJPanel = new PaginationJPanel(jFrameMain);
				this.add(paginationJPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
				messageListTable.setPaginationJPanel(paginationJPanel);
				PaginationJPanel.messageListTable = messageListTable;				
			}
			{
				refreshMenu = new JPopupMenu();
				JMenuItem refreshItem = new JMenuItem("Refresh");
				JMenuItem focusItem = new JMenuItem("Focus");
				refreshMenu.add(refreshItem);
				refreshMenu.add(focusItem);
				refreshItem.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseReleased(MouseEvent evt) {
						if (jFrameMain.getTableTreeJPanel().getTableTree().getRefreshingPartitionList() == true) {
							InfoReporting.printWarning("This action is not allowed whilst the Partition list is being refreshed.");
						}
						else {
							messageListTable.refreshTable();
						}
					}					
				});				
				focusItem.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseReleased(MouseEvent evt) {
						if (jFrameMain.getTableTreeJPanel().getTableTree().getRefreshingPartitionList() == true) {
							InfoReporting.printWarning("This action is not allowed whilst the Partition list is being refreshed.");
						}
						else {
							messageListTable.refreshTableWithFocus();
						}
					}					
				});				
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		head2column = new HashMap<String, String>();
		head2column.put("MSG_ID", "MSG_ID");
		head2column.put("HOST", "MACHINE_NAME");
		head2column.put("APPLICATION", "APPLICATION_NAME");
		head2column.put("ISSUED", "ISSUED_WHEN");
		head2column.put("SEVERITY", "SEVERITY");
		head2column.put("MESSAGE", "MSG_TEXT");
		head2column.put("PARAM", "PARAM ");
		head2column.put("OPT_PARAM", "OPT_PARAM ");
		head2column.put("QUALIFIERS", "QUALIFIERS ");
		
		orderDirection = new HashMap<String, String>();
		orderDirection.put("MSG_ID", "ASC");
		orderDirection.put("HOST", "ASC");
		orderDirection.put("APPLICATION", "ASC");
		orderDirection.put("ISSUED", "ASC");
		orderDirection.put("SEVERITY", "ASC");
		orderDirection.put("MESSAGE", "ASC");
		orderDirection.put("PARAM", "ASC ");
		orderDirection.put("OPT_PARAM", "ASC ");
		orderDirection.put("QUALIFIERS", "ASC ");
	}
		

	/**
	 * Custom TableCellRendererto to sign messages with chained messages and to
	 * add the appropriate color in the severity column.
	 * @author cbiau
	 *
	 */
	class MessageListTableCellRenderer implements TableCellRenderer 
	{	
		private JLabel label = new JLabel(); 
		private JButton buttonPlus = new JButton("+");
		private JButton buttonMinus = new JButton("-");
		
		@Override
        public Component getTableCellRendererComponent(JTable table, Object value, 
				                                       boolean isSelected, boolean hasFocus, 
				                                       int line, int column) 
		{ 
		    Component comp = null;
		    // If it's the first column and there is a chained msg, put a button
		    if((table.getColumnName(column).equals("cm")) && ((value.toString().equalsIgnoreCase("+")) || (value.toString().equalsIgnoreCase("-")))) 
		    { 
		    	JButton button = null;
		    	if ((value.toString().equalsIgnoreCase("+")))
		    		button = buttonPlus;
		    	else
		    		button = buttonMinus;
		    	
	    		button.setVisible(true);
	    		button.setEnabled(true);
		    	
	    		//in this case comp is a JButton
		    	comp = button;
		    }
		    else
		    {
		    	label.setHorizontalAlignment(SwingConstants.CENTER); 
		    	// If the line is selected, the severity is a little darker and the rest is light blue
		    	if (isSelected == true)
		    	{
		    		if(table.getColumnName(column).equals("Severity")) 
			    	{
			    		int i = 0;
			    		String severity = value.toString().toUpperCase();
				    	while (!severity.equals(strSeverity[i].toUpperCase())) {
				    		i++;
				    	}
			    		label.setBackground(severityColorsSelected[i][0]);
			    		label.setForeground(severityColorsSelected[i][1]);
			    	}
			    	else
			    	{
			    		label.setBackground( new java.awt.Color(157, 206, 255));
				    	label.setForeground( new java.awt.Color(0, 0, 0));
			    	}
		    	}
		    	// If not selected, the severity is as defined in the class Message and the rest is white		    	
		    	else
		    	{
		    		if(table.getColumnName(column).equals("Severity")) 
			    	{
			    		int i = 0;
			  			    		
			    		String severity = value.toString().toUpperCase();		    		
				    	while (!severity.equals(strSeverity[i].toUpperCase())) {
				    		i++;
				    	}			    	
				    	label.setBackground(severityColors[i][0]);
				    	label.setForeground(severityColors[i][1]);
			    	}
			    	else
			    	{
			    		label.setBackground( new java.awt.Color(255, 255, 255));
				    	label.setForeground( new java.awt.Color(0, 0, 0));
			    	}
		    	}
		    			   
		    	// JLable is transparent by default 
			    label.setOpaque(true); 
			    
			    if (null != value) { 
			    	label.setText(value.toString());
			    } else {
			    	label.setText("");
			    }
			     
			    // Comp is a JLabel
		    	comp = label;
		    } 	    
		    return comp; 
		  } 		  
		} 

	
	
	/**
	 * Set the List of Messages on the table
	 */
	public void setListMessageTable(MessageListTable table) { messageListTable = table; }
	
	/**
	 * Get the List of Messages
	 */
	public MessageListTable getListMessageTable() { return messageListTable; }
	
	public PaginationJPanel getPaginationJPanel() { return paginationJPanel; }
	public void setPaginationJPanel(PaginationJPanel temp) { paginationJPanel = temp; }
	
	
	/**
	 * Set DefaultTableModel to the configuration chooser.
	 * 
	 * @param defaultTableModel
	 * 			is the DefaultTableModel to set.
	 */
	public void setDefaultTableModel(DefaultTableModel defaultTableModel) {
		
		if(defaultTableModel != null)			
			// Set the model tree
			messageListTable.setModel(defaultTableModel);		
	}
	
	/**
	 * Get DefaultTreeModel to the configuration chooser.
	 * 
	 * @return the DefaultTreeModel of the table.
	 */
	public DefaultTableModel getDefaultTableModel() {
		
		return (DefaultTableModel)messageListTable.getModel();

	}
	
	/**
	 * Return the selected row.
	 * 
	 * @return the selected row
	 */
	public int getSelectedRow() {
		return messageListTable.getSelectedRow();
		
	}
	
	/**
	 * Return the selected rows.
	 * 
	 * @return the selected rows
	 */
	public int[] getSelectedRows() {
		return messageListTable.getSelectedRows();
		
	}

	/**
	 * Return the scroll pane.
	 * 
	 * @return the scroll pane
	 */
	public JScrollPane getJScrollPane() {
		return MessageListjScrollPane;
		
	}		
	
	/**
	 * Actions to do when a row of a JTable is selected.
	 */
	public void valueChanged(TreeSelectionEvent e) {
		// Display the selected full path 
		int rowSelected = this.getSelectedRow();
	}
	
	/**
	* Enables or disables this component, depending on the value of the parameter b. An enabled component can respond to user input and generate events. 
    * Components are enabled initially by default.
	* 
	* @param b
	* 		If true, this component is enabled, otherwise this component is disabled.
	* 
	*/
	@Override
    public void setEnabled(boolean b) {		
		messageListTable.setEnabled(b);		
	}
	
	/**
	 * Actions to do when the mouse is released on the rsConfigChooserTree.
	 * 
	 * @param e a MouseEvent.
	 */
	public void TableTreeMouseReleased(MouseEvent e) {		
		// If it is a right click
	}
	
	/**
	 * Action to do when a click on the mouse is performed.
	 * 
	 * @param evt is an EventObject.
	 */
	//@SuppressWarnings("deprecation")
	private void MessageListTableMouseClicked(MouseEvent evt) 
	{
		// If it is a right click
		if(SwingUtilities.isLeftMouseButton(evt)) 
		{				
			// Display the details of the message selected in the ContentDetail panel 
			int rowSelected;
			rowSelected = this.getSelectedRow();
			if (rowSelected == - 1) {
				return;
			}
			
			// The content details are updated now in MessageListTable.outputSelection
			// jFrameMain.getContentDetailJPanel().fillContentDetails(messageListTable, rowSelected);

			// Show chained messages if the user clicked on the first column. 
			if (messageListTable.getSelectedColumn() == 0)
			{
				if (messageListTable.getValueAt(rowSelected, 0).toString().equalsIgnoreCase("+"))
				{
					messageListTable.setValueAt("-", messageListTable.getSelectedRow(), 0);
					
                    int indexBeg;
                    int indexEnd;
                    String tmp;	
					String rootTime;
					String msgId;
					String host;
					String application;
					String severity;
					String msgText;
					String param;
					String optParam;
					String qualifiers;
									

			        /* MESSAGE FORMAT:	
		              "RootTime:[Root time in string]
		              "MsgStart: "
		              << "ID:["    << mc->getID()                 << "]"
		              << "Host:["  << mc->getMachineName()        << "]"
		              << "App:["   << mc->getApplicationName()    << "]"
		              << "Time:["  << mc->getERSTime()            << "]"
		              << "Sev:["   << mc->getSeverity()           << "]"
		              << "Txt:["   << mc->getText()               << "]"
		              << "Param:[" << mc->getParameters()         << "]"
		              << "Opt:["   << mc->getOptionalParameters() << "]"
		              << "Qual:["  << mc->getQual()               << "]EndMsg "			               
                    */					
					String chainedMsgs = (String)messageListTable.getMessageListTableModel().getValueAt(rowSelected, 10);

					// Retrieve the root log message timestamp.  
					// Use the root log message time for all the chained messages. We need to do this
					// because for release < tdaq-03-00-00 the chained messages timesamp was local
					// time instead of UTC. Assume that the root and the chained messages were issued
					// within the same second.
					tmp      = "RootTime:[";
					String[] tabTemp1, tabTemp2, tabTemp3; 					
					tabTemp1 = chainedMsgs.split("]EndMsg ");
					indexBeg = chainedMsgs.indexOf(tmp) + tmp.length();
					tmp      = "]";
					indexEnd = chainedMsgs.indexOf(tmp, indexBeg);						
					rootTime = chainedMsgs.substring(indexBeg, indexEnd);
					
					// Remove the RootTime parameter.
					chainedMsgs = chainedMsgs.substring(indexEnd + 1);

					for (Integer i = 0; i < tabTemp1.length; )
					{
						String chMsg = tabTemp1[i];
						tmp      = "ID:[";
						indexBeg = chMsg.indexOf(tmp) + tmp.length();
						tmp      = "]Host:[";
						indexEnd = chMsg.indexOf(tmp, indexBeg);						
						msgId    = chMsg.substring(indexBeg, indexEnd);
						
						indexBeg = indexEnd + tmp.length();
						tmp      = "]App:[";
						indexEnd = chMsg.indexOf(tmp, indexBeg);
						host     = chMsg.substring(indexBeg, indexEnd);

						indexBeg = indexEnd + tmp.length();
						tmp      = "]Time:[";
						indexEnd = chMsg.indexOf(tmp, indexBeg);
						application = chMsg.substring(indexBeg, indexEnd);	
						
						indexBeg  = indexEnd + tmp.length();
						tmp       = "]Sev:[";
						indexEnd  = chMsg.indexOf(tmp, indexBeg);
						
						indexBeg = indexEnd + tmp.length();
						tmp      = "]Txt:[";
						indexEnd = chMsg.indexOf(tmp, indexBeg);
						severity = chMsg.substring(indexBeg, indexEnd);
						
						indexBeg = indexEnd + tmp.length();
						tmp      = "]Param:[";
						indexEnd = chMsg.indexOf(tmp, indexBeg);
						msgText  = chMsg.substring(indexBeg, indexEnd);

						indexBeg = indexEnd + tmp.length();
						tmp      = "]Opt:[";
						indexEnd = chMsg.indexOf(tmp, indexBeg);
						param    = chMsg.substring(indexBeg, indexEnd);
						
						indexBeg = indexEnd + tmp.length();
						tmp      = "]Qual:[";
						indexEnd = chMsg.indexOf(tmp, indexBeg);
						
						// Back compatibility, before tdaq-02-00-03 (patches) messges 
						// did not include the qualifiers field.
						if (-1 == indexEnd) {
							optParam = chMsg.substring(indexBeg, chMsg.length());
							qualifiers = "";
						}
						else {
							optParam = chMsg.substring(indexBeg, indexEnd);
							
							indexBeg   = indexEnd + tmp.length();
							qualifiers = chMsg.substring(indexBeg, chMsg.length());
						}

						Vector<String> row = new Vector<String>();

						++i;
						row.add(i.toString());
						row.add(host);
						row.add(application);
						row.add(rootTime);
						row.add(severity.substring(0, 1).toUpperCase() + severity.substring(1).toLowerCase());
						row.add(msgId);
						row.add(msgText);
						row.add(param);
						row.add(optParam);
						row.add(qualifiers);

						messageListTable.getMessageListTableModel().insertRow(rowSelected+i, row);
					}
				}
				else if (messageListTable.getValueAt(messageListTable.getSelectedRow(),0).toString().equalsIgnoreCase("-")) {
					// Add the '+' sign and remove the chained messages.
					messageListTable.setValueAt("+", messageListTable.getSelectedRow(), 0);
					while (true == Pattern.compile("[0-9]").matcher(messageListTable.getValueAt(rowSelected + 1, 0).toString()).find()) 
					{
					   messageListTable.getMessageListTableModel().removeRow(rowSelected+1);
					}
				}
				else {
					// do nothng
				}
			}
		}		
	}

	/**
	 * Action to do when a click on the mouse is performed on the header of the table.
	 * 
	 * @param evt is an EventObject.
	 */
	private void HeaderMessageListTableMouseClicked(MouseEvent evt) 
	{
		int column = header.columnAtPoint(evt.getPoint());

		// Don't order if clicked on the chained message column
		if (column == 0) {
			return;
		}
		
		String columnName = messageListTable.getColumnName(column).replace(" ", "_").toUpperCase();
		messageListTable.getSearchCriteria().setOrderBy(head2column.get(columnName));
		
		if (orderDirection.get(columnName) == "ASC") {
			// Very ugly solution....
			orderDirection.remove(columnName);
			orderDirection.put(columnName, "DESC");
			messageListTable.getSearchCriteria().setOrderDir(false);
		}
		else {
			// Very ugly solution....
			orderDirection.remove(columnName);
			orderDirection.put(columnName, "ASC");
			messageListTable.getSearchCriteria().setOrderDir(true);
		}
		
		paginationJPanel.setIndxPage(0);
		messageListTable.refreshTable();
	}
		
	/**
	 * this function gives the action to do when a key is typed
	 * @param evt
	 */
	private void messageListTableKeyTyped(KeyEvent evt) {
		if ((evt.getKeyCode()==KeyEvent.VK_A)&&(evt.isControlDown()))  
		 {
			this.getListMessageTable().selectAll();
		 }
		
		if ((evt.getKeyCode()==KeyEvent.VK_S)&&(evt.isControlDown()))  
		 {
			jFrameMain.saveSelection();
		 }
	}	
	
	public MessageListTable getMessageListTable() {
		return messageListTable;
	}	
}

