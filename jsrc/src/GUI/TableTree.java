package GUI;

/**
 * this class is for the tableTree it has the same particularities ad JTree plus some more
 * it permits me to fill the tree and to get the selected items
 */
import java.awt.Cursor;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Enumeration;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import common.db.DBConnectorException;
import common.db.LogServiceDB;

import GUI.InfoReporting;


public class TableTree extends javax.swing.JTree {

	class RefreshTableThread extends Thread 
	{
		public RefreshTableThread() {
		}

		@Override
        public void run() {
			Thread progressBarThread = new Thread()
            {				
				@Override
                public void run() {					
					// Kick in the progress bar
					jFrameMain.getMessageListTableJPanel().getPaginationJPanel().getMiscInfo().setText("Retrieving Partitions from the " + jFrameMain.getDbSource() + " database.");
					jFrameMain.getMessageListTableJPanel().getPaginationJPanel().getProgressBar().setIndeterminate(true);
					jFrameMain.getMessageListTableJPanel().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				}
            }; 
            try {
            	SwingUtilities.invokeAndWait(progressBarThread);
            }
            catch (Exception e) {
            	InfoReporting.printError(e.getMessage());
            }            
			
            // Do not stop the progress bar if there is another thread retrieving messages. 
            //if (true == processRefreshPartitoinList()) {
            	processRefreshPartitoinList();
				// Stop the progress bar
				jFrameMain.getMessageListTableJPanel().getPaginationJPanel().getProgressBar().setIndeterminate(false);
				jFrameMain.getMessageListTableJPanel().getPaginationJPanel().getMiscInfo().setText("");		
				jFrameMain.getMessageListTableJPanel().setCursor(Cursor.getDefaultCursor());
			//}
		}		
	}		
	
	private static final long serialVersionUID = 7838644640611895489L;

	// The tree model of the configuration chooser tree
	private DefaultTreeModel treeModel;
	
	// The root node of the configuration chooser tree
	private DefaultMutableTreeNode rootNode;
	
	private boolean refreshingPartitionList = false; 
	
	TreeMap<String, Long> partitionMap = null;
	
	JFrameMain jFrameMain;
	
	/**
	 * Constructor.
	 *
	 */
	public TableTree(JFrameMain frameMain) {		
		super();
		
		jFrameMain = frameMain;
		
		// Create an empty root node
		rootNode = null;
		// Add this root node to the DefaultTreeModel
		treeModel = new DefaultTreeModel(rootNode);
		
		// Add the DefaultTreeModel to the Jtree
		this.setModel(treeModel);
	}
	
	
	/**
	 * Fills the tree with the content of the metadata table
	 */
	public void filljTreeTable()
	{
		refreshingPartitionList = true;
		// Create thread to handle the request and wait for it to finish	
		RefreshTableThread refresh = new RefreshTableThread();
		refresh.start();		
	}
	
	public void processRefreshPartitoinList()
	{	
	    ArrayList<String> userList      = null;
		ArrayList<String> runnumberList = null;		
        partitionMap                    = new TreeMap<String, Long>();
        LogServiceDB lsDB               = new LogServiceDB(jFrameMain.getDatabaseConnection(), jFrameMain.getDatabaseUser(), jFrameMain.getDatabasePass());
		
		try {			
			lsDB.openConnection();
			// Retrieve all the partitions			
			lsDB.SelectListPartitions(partitionMap, jFrameMain.getCurrentTdaqRelease());			
		}
		catch (DBConnectorException e) {			
			InfoReporting.printError(e);
			refreshingPartitionList = false;
			return;
		}		
			
		// Create the root node
		rootNode = null;
		treeModel = null;
		rootNode = new DefaultMutableTreeNode(jFrameMain.getCurrentTdaqRelease());
		treeModel = new DefaultTreeModel(rootNode);
		treeModel.setAsksAllowsChildren(true);
		
		DefaultMutableTreeNode partitionNode = null;
		DefaultMutableTreeNode usersNode = null;
		DefaultMutableTreeNode runnumberNode = null;
		
		String errorString = "";
		for (Iterator it = partitionMap.entrySet().iterator();	it.hasNext(); )			
		{
			// For each partition, create a node in the tree
			Map.Entry entry = (Map.Entry)it.next();
			String partitionName = entry.getKey().toString();
			Long partitionID     = (Long)entry.getValue();		
		
//         	partitionNode = new DefaultMutableTreeNode(partitionName);
//			rootNode.add(partitionNode);
			
			// Retrieve the Users for each Partition
			userList = null;
			userList = new ArrayList<String>();
			
			try {
				lsDB.SelectListUser(partitionID, userList, jFrameMain.getTableTreeStartTime());								
			}
			catch (DBConnectorException e) {	
				errorString += "Could not retrieve the Users for Partition: " + partitionName + 
                				". Error: " + e.getMessage();
				Throwable cause = e;
				while((cause = cause.getCause()) != null) {
					errorString += "\twas caused by: " + cause.getMessage();
				}	
				continue;
 			}
            
			// Backward compatibility when the runnumber search option is introduced. The metadata
			// table changed and therefore not all the tables and users produced by previous releases
			// are there.			
			if (userList.size() > 0) {
         	   partitionNode = new DefaultMutableTreeNode(partitionName);
			   rootNode.add(partitionNode);
			}
			
			for (int j = 0 ; j < userList.size(); j++ )
			{
				//for each user i create a node in the tree and add it to its parent
				String userName = userList.get(j);
				usersNode = new DefaultMutableTreeNode(userName);
				partitionNode.add(usersNode);
				
				// Get the Session for each User
                runnumberList = null;
                runnumberList =  new ArrayList<String>();				
				try {
					lsDB.SelectListRunNumbers(partitionID, userName, runnumberList, 
			                  jFrameMain.getTimeZoneGeneva(), 
			                  jFrameMain.getTableTreeStartTime());
				}
				catch (DBConnectorException e) {
					errorString += "Could not retrieve the Run Numbers for Partition: " + partitionName + 
		                           " and User: " + userName + ". Error: " + e.getMessage();
               		Throwable cause = e;
               		while((cause = cause.getCause()) != null) {
               			errorString += "\twas caused by: " + cause.getMessage();
               		}
					continue;
				}
				
				for (int k = 0 ; k < runnumberList.size(); k++)
				{			
					runnumberNode = new DefaultMutableTreeNode(runnumberList.get(k));
					usersNode.add(runnumberNode);									
    				treeModel.setAsksAllowsChildren(false);
				}			
			}
		}
				
		try {
			lsDB.closeConnection();
		}
		catch (DBConnectorException e) {			
			InfoReporting.printError(e);
			refreshingPartitionList = false;
			return;
		}	
		
		if (!errorString.equals("")) {
			InfoReporting.printError(errorString);
		}

		// Do not unset this flag pass setModel() or it will cause a warning to be shown
		refreshingPartitionList = false;
		
		this.setModel(treeModel);
		
		// Display the tree from the first node 
        this.setSelectionRow(0);
        this.scrollPathToVisible(this.getSelectionPath());
        this.setVisible(true);     
	}
	
	public DefaultMutableTreeNode searchNode(String nodeName, DefaultMutableTreeNode parent, boolean rn)
	{		
		DefaultMutableTreeNode node = null;
		Enumeration children = parent.children(); 
		
		while (children.hasMoreElements())
		{ 				
			node = (DefaultMutableTreeNode)children.nextElement();
			String parameter;
			if (false == rn) {
				parameter = node.getUserObject().toString();
			}
			else {
				// The run number is referenced in the tree as: rn (time) ->  81152  (11.08.2008 17:53:55)
				// Retrieve the run number only.
				int indx =  node.getUserObject().toString().indexOf("  (");
				if (-1 == indx) {
					InfoReporting.printError("Incorrect format in the Table tree");
					return node;							
				}                 										
				parameter = node.getUserObject().toString().substring(0, indx);				
			}
			if (0 == parameter.compareTo(nodeName)) {
				break; 				
			}
			node = null;
		}
		
		return node;
	}
	
	/**
	 * Return the selected Item.
	 * 
	 * @return the selected Directory object
	 */
	public String getSelectedItem() {
		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)this.getLastSelectedPathComponent();		
		if(selectedNode != null) {
			return (String)(selectedNode.getUserObject());
		}
		else {
			return null;
		}
	}
	
	/**
	 * Return the selected Item.
	 * 
	 * @return the selected Item object
	 */
	public String[] getSelectedItems() {		
		String[] items =  new String[this.getSelectionPaths().length];	
		TreePath[] treePath = this.getSelectionPaths();		
		
		for(int i=0; i < treePath.length; i++) {
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)treePath[i].getLastPathComponent();
			String item = (String)(selectedNode.getUserObject());
			items[i] = item;			
		}
		
		return items;
	}
	
    public void collapseAllNodes() {
        int row = getRowCount() - 1;
        while (row >= 1) {
            collapseRow(row);
            row--;
        }
    }
    
	/**
     * Delete the tree.
     */
    public void deleteTree() {    	
    	rootNode = null;
		treeModel = new DefaultTreeModel(rootNode);
        this.setModel(treeModel);        
	}


	public Map<String, Long> getPartitionMap() {
		return partitionMap;
	}


	public boolean getRefreshingPartitionList() {
		return refreshingPartitionList;
	}    
}
