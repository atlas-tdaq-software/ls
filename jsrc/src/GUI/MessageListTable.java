package GUI;

/**
 * this class is adding particularities to JTable
 * it defines how it will be filled
 */

import java.awt.Cursor;
import java.awt.Rectangle;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.DefaultTableModel;
import javax.swing.SwingUtilities;
import GUI.SearchCriteria;
import GUI.MessageListTableModel;

import common.db.DBConnectorException;
import common.db.LogServiceDB;

    

public class MessageListTable extends javax.swing.JTable
{		
	class RefreshTableThread extends Thread 
	{
		@Override
        public void run() {
			Thread progressBarThread = new Thread()
            {				
				@Override
                public void run() {					
					// Kick in the progress bar
					jFrameMain.getMessageListTableJPanel().getPaginationJPanel().getMiscInfo().setText("Retrieving logs from the " + jFrameMain.getDbSource() + " database...");
					jFrameMain.getMessageListTableJPanel().getPaginationJPanel().getProgressBar().setIndeterminate(true);
					jFrameMain.getMessageListTableJPanel().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				}
            }; 
            try {
            	SwingUtilities.invokeAndWait(progressBarThread);
            }
            catch (Exception e) {
            	InfoReporting.printError(e.getMessage());
            }            
            			
            // Do not stop the progress bar if there is another thread retrieving messages. 
            if (true == processRefreshTable()) {
				// Stop the progress bar
				jFrameMain.getMessageListTableJPanel().getPaginationJPanel().getProgressBar().setIndeterminate(false);
				jFrameMain.getMessageListTableJPanel().getPaginationJPanel().getMiscInfo().setText("");		
				jFrameMain.getMessageListTableJPanel().setCursor(Cursor.getDefaultCursor());
			}
		}		
	}		

	private static final long serialVersionUID = -2361900041002169939L;
	final static String[] columnNames = {"cm",        "Host",       "Application", "Issued", 
	                                     "Severity",  "Msg Id",     "Message",     "Param", 
	                                     "Opt Param", "Qualifiers", "ChainedMsgs"};
	private long numberOfRowsLocal;
	private long numberOfRowsDatabase;
	private int numberOfPages;
		
	private MessageListTableModel messageListTableModel = null;	
	//StoreTableContent storeTableContent;    
	private PaginationJPanel paginationJPanel;
	SearchCriteria searchCriteria;
	private JFrameMain jFrameMain;
	private String compiledMsg;
	
	
	/**
	 * Constructor.
	 *
	 */
	public MessageListTable(JFrameMain frameMain) 
	{		
		super();	
		
		jFrameMain = frameMain;
		
		compiledMsg = new String();

		// Set the search criteria 
		searchCriteria = new SearchCriteria();		
		searchCriteria.setLimitTo(jFrameMain.getMaxMsgsDisplayed());
		
		getSelectionModel().addListSelectionListener(new RowListener());
		
		messageListTableModel = new MessageListTableModel();
	
		int[] defColumnWidth = { 18, 100, 100, 130, 50, 100, 270, 150, 150, 150, 0 };
		boolean[] columnsEnabled = frameMain.getColumnsEnabled();
		for (int i = 0; i < columnNames.length; ++i) {		
			messageListTableModel.addColumn(columnNames[i]);
		}
		setModel(messageListTableModel);
		
		messageListTableModel.addTableModelListener(this);
		
		JTableHeader header = this.getTableHeader();
	    header.setUpdateTableInRealTime(true);
	    header.setReorderingAllowed(true);
				
	    // Only show the columns that are enabled. Never show the chained message text,  
	    // it is simply used to tore and later retrieve the parsed chained msgs.
		for (int i = 0; i < columnNames.length; ++i) {
			getColumn(columnNames[i]).setPreferredWidth(defColumnWidth[i]);
			if (false == columnsEnabled[i]) {
				removeColumn(getColumn(columnNames[i]));
			}			
		}
	}
	
	private class RowListener implements ListSelectionListener {
	        @Override
            public void valueChanged(ListSelectionEvent event) {
			if (event.getValueIsAdjusting()) {
				return;
			}
			outputSelection();
		}
	}        
	  
    
	private void outputSelection() 
	{
		int rowSelected = getSelectedRow();
		if (-1 == rowSelected) { return; } 
		jFrameMain.getContentDetailJPanel().fillContentDetails(this, rowSelected);
	}

	
	private String compiledMessage(int row)
	{		
		StringBuffer cmpMsg = new StringBuffer();
		// The row is guarnteed to be within boundaries by the calling functions.
  		int numColumns = messageListTableModel.getColumnCount();
      	for (int i = 0; i < numColumns; ++i) {
      		cmpMsg.append(messageListTableModel.getValueAt(row, i).toString());
      	}
		return cmpMsg.toString();
	}
    	
	
	/**
	 * Using the treePathSelected this function displays the messages of the database
	 * @param treePathSelected
	 */
	//@SuppressWarnings("deprecation")
	public void fillListTable(Long partitionID, String partitionName, String user, long[] runnumbers)
	{		
		// Reset the pagination values			
		getSearchCriteria().setLimitFrom(0);
		getSearchCriteria().setLimitTo(jFrameMain.getMaxMsgsDisplayed());
		paginationJPanel.setIndxPage(0);

		// Fill in the search criteria with the partition, table, user and session id
		searchCriteria.setPartitionID(partitionID);
		searchCriteria.setPartitionName(partitionName);
		searchCriteria.setUserName(user);
		searchCriteria.setRunNumbers(runnumbers);
		
		refreshTable();
	}
	
	
	public void refreshTable()
	{
		if (jFrameMain.getTableTreeJPanel().getTableTree().getRefreshingPartitionList() == true) {
			InfoReporting.printWarning("This action is not allowed whilst the Partition list is being refreshed.");
			return;
		}	
		
		// Load what is on the search criteria pannel.
		jFrameMain.getSearchJPanel().SearchjButtonActionPerformed(searchCriteria);
		// Set the search case sensitivness
		searchCriteria.setCaseInsensitiveSearch(jFrameMain.getCaseInsensitiveSearch());
		searchCriteria.composeQuery();

    
	    this.setVisible(false);
	    
	    // Clear the table.
	    while (0 != messageListTableModel.getRowCount()) {
	    	messageListTableModel.removeRow(0);
	    }
	    
	    // Remove columns if appropriate    
	    if (false == jFrameMain.getJMenuItemHost().isSelected()) {
	    	jFrameMain.jMenuItemColumnHostActionPerformed(false);
	    }
	    if (false == jFrameMain.getJMenuItemApplication().isSelected()) {
	    	jFrameMain.jMenuItemColumnApplicationActionPerformed(false);
	    }
	    if (false == jFrameMain.getJMenuItemIssued().isSelected()) {
	    	jFrameMain.jMenuItemColumnIssuedActionPerformed(false);
	    }
	    if (false == jFrameMain.getJMenuItemSeverity().isSelected()) {
	    	jFrameMain.jMenuItemColumnSeverityActionPerformed(false);
	    }
	    if (false == jFrameMain.getJMenuItemMsgID().isSelected()) {
	    	jFrameMain.jMenuItemColumnMsgIDActionPerformed(false);
	    }
	    if (false == jFrameMain.getJMenuItemMessage().isSelected()) {
	    	jFrameMain.jMenuItemColumnMsgActionPerformed(false);
	    }
	    if (false == jFrameMain.getJMenuItemParam().isSelected()) {
	    	jFrameMain.jMenuItemColumnParamActionPerformed(false);
	    }
	    if (false == jFrameMain.getJMenuItemOptParam().isSelected()) {
	    	jFrameMain.jMenuItemColumnOptParamActionPerformed(false);
	    }
	    if (false == jFrameMain.getJMenuItemQualifiers().isSelected()) {
	    	jFrameMain.jMenuItemColumnQualifiersActionPerformed(false);
	    }	    	
    		
	    this.setVisible(true);
	
		// Create thread to handle the request and wait for it to finish	
		RefreshTableThread refresh = new RefreshTableThread();
		refresh.start();
	}
	
	public boolean processRefreshTable()
	{		
		try {
			LogServiceDB lsDB = new LogServiceDB(jFrameMain.getDatabaseConnection(), jFrameMain.getDatabaseUser(), jFrameMain.getDatabasePass());	
			 
			lsDB.openConnection();
			// SelectMessages return false if there is a newest thread processing a select. 
			boolean retVal = lsDB.SelectMessages(searchCriteria, this, jFrameMain.getTimeZoneGeneva());				
			lsDB.closeConnection();
			if (!retVal) { return false; }									

			if (0 == numberOfRowsLocal) {
				InfoReporting.printError("No messages were found for partition " + searchCriteria.getPartitionName() + " given the search criteria.");
			}					    
			
	        // Compose the to-from textable
			long msgTo = searchCriteria.getLimitTo();
			if (msgTo > numberOfRowsDatabase) {
				msgTo = numberOfRowsDatabase;
			}
            paginationJPanel.getPageInfo().setText("(" + searchCriteria.getLimitFrom() + "-" + msgTo + ") of " + getNumberOfRowsDatabase());
			numberOfPages = (int)(numberOfRowsDatabase / jFrameMain.getMaxMsgsDisplayed());	    
			if (((int)(numberOfRowsDatabase / jFrameMain.getMaxMsgsDisplayed())) > 0 ) {
				++numberOfPages;
			}
					
			// If a message has been selected to be focused, try to find it and scroll to it.			
			if (0 != compiledMsg.length()) {
				int rowCount = getRowCount();
				int rowIndx = 0;
				for (; rowIndx < rowCount; ++rowIndx) {
					if (compiledMsg.equals(compiledMessage(rowIndx))) {
						break;
					}
				}
				if (rowCount != rowIndx) {
					compiledMsg = "";
					setRowSelectionInterval(rowIndx, rowIndx);
					scrollRectToVisible(new Rectangle(getCellRect(rowIndx, 0, true)));
				}
				else {
					// Check if the message is in the next page. If there are no more pages, we are done.
					if (!jFrameMain.getMessageListTableJPanel().getPaginationJPanel().goToNextPage()) {
						compiledMsg = "";
						InfoReporting.printError("Focused message has not been found in the new search.");	
					}
				}
			}
		}
		catch (DBConnectorException e) {			
			InfoReporting.printError(e);
			return false;				
		}

	    return true;
	}	
	
		
	public void refreshTableWithFocus()
	{
		int rowSelected = getSelectedRow();		
		if (-1 == rowSelected) {  
			InfoReporting.printError("Please select a row to focus.");
			return;
		}
		compiledMsg = compiledMessage(rowSelected);
		// Start searching from the first page.
		jFrameMain.getMessageListTableJPanel().getPaginationJPanel().goToFirstPage();
	}
	
    /**
     * Returns the name of a specific column.
     * 
     * @param col
     * 			is the index of hte column.
     * @return the name of a specific column.
     */
    public String getColumnHeadName(int col) {
        return columnNames[col];
    }	
	
	/**
	 * this function replaces null by "" 
	 * @param temp
	 * @return
	 */
	public String replaceNull(String temp)
	{
		if (temp == null)
			temp = "";
		
		return temp;
	}

	public SearchCriteria getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(SearchCriteria searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public PaginationJPanel getPaginationJPanel() {
		return paginationJPanel;
	}

	public void setPaginationJPanel(PaginationJPanel paginationJPanel) {
		this.paginationJPanel = paginationJPanel;
	}

	public DefaultTableModel getMessageListTableModel() {
		return messageListTableModel;
	}

	public long getNumberOfRowsDatabase() {
		return numberOfRowsDatabase;
	}

	public void setNumberOfRowsDatabase(long numberOfRowsDatabase) {
		this.numberOfRowsDatabase = numberOfRowsDatabase;
	}

	public long getNumberOfRowsLocal() {
		return numberOfRowsLocal;
	}

	public void setNumberOfRowsLocal(long numberOfRowsLocal) {
		this.numberOfRowsLocal = numberOfRowsLocal;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}
}


