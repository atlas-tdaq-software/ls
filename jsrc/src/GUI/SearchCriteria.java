package GUI;

/**
 * This class encapsulates all the fields that can form the search criteria
 * @author rmurillo
 *
 */
public class SearchCriteria 
{ 
	private Long partitionID;
	private String partitionName; 
	private String userName; 
	private long[] runNumbers;
	private String  host;
	private boolean hostRegExp;
	private String  application;
	private boolean applicatonRegExp;
	private String  msgID;
	private boolean msgIDRegExp;
	private String  msg;
	private boolean msgRegExp;
	private String  param;
	private boolean paramRegExp;
	private String  optParam;
	private boolean optParamRegExp;
	private String  qualifiers;
	private boolean qualifiersRegExp;
	private int[] severity;
	private long issuedFrom;
	private long issuedTo;
	private long limitFrom;
	private long limitTo;
	private String orderBy;
	private boolean orderAsc;	
	private String querySelect;
	private String queryCount;
	private boolean caseInsensitive;
	private static String listColumns = "PART_ID, USER_NAME,SESSION_ID,MSG_ID,MACHINE_NAME,APPLICATION_NAME,ISSUED_WHEN,SEVERITY,MSG_TEXT,PARAM,CHAINED_MSGS,LOGGED_WHEN,OPT_PARAM,QUALIFIERS";
	
	/**
	 * Constructor
	 */	
	public SearchCriteria()
	{
		partitionID      = -1L;
		partitionName    = "";
		userName         = ""; 
		runNumbers       = new long[0];		
		host             = "";
		application      = "";
		msgID            = "";
		msg              = "";
		param            = "";
		optParam         = "";
		qualifiers       = "";
		severity         = new int[0];		
		issuedFrom       = -1L;
		issuedTo         = -1L; 
		limitFrom        = -1L;
		limitTo          = -1L;				
		orderBy          = "";
		orderAsc         = true;		
		querySelect      = "";
		queryCount       = "";
		caseInsensitive  = false;		
		msgRegExp        = false;
		hostRegExp       = false;
		qualifiersRegExp = false;
		applicatonRegExp = false;
		paramRegExp      = false;				
		optParamRegExp   = false;
		msgIDRegExp      = false;		
	}

	
	private String getPartialQuery(String parameter, String type, boolean regExp)
	{
	
		String partialQuery = "";
		
		if (parameter.isEmpty()) {
			return partialQuery;
		}
			
		String caseQuery = "";

		if (true == caseInsensitive) {
			caseQuery = "UPPER";
			parameter = parameter.toUpperCase();
		}

		if (false == regExp) {
			String likeQuery;
			String logic;

			if (true == parameter.startsWith("\\!") ||
				true == parameter.startsWith("\\|") ||
				true == parameter.startsWith("\\|!")) {
					// Remove the '\' wildcard.
					parameter = parameter.substring(1);
					likeQuery = "like";
					logic     = "AND";					
			}
			else if (true == parameter.startsWith("|!")) {
				// Remove the wildcards '|!'
				parameter = parameter.substring(2);
				likeQuery = "not like";
				logic     = "OR ";
			}
			else if (true == parameter.startsWith("!")) {
				// Remove the negative wildcard '!'
				parameter = parameter.substring(1);
				likeQuery = "not like";
				logic     = "AND";
			}
			else if (true == parameter.startsWith("|")) {
				// Remove the OR wildcard '|'
				parameter = parameter.substring(1);
				likeQuery = "like";
				logic     = "OR ";
			}
			else {
				likeQuery = "like";
				logic     = "AND";				
			}

			partialQuery = " (" + caseQuery + "(" + type + ") " + likeQuery + " '%" + parameter + "%' OR " 
			             + caseQuery + "(CHAINED_MSGS) " + likeQuery + " '%" + parameter + "%') " + logic;
		}				

		else {
			String logic;
			
			if (true == parameter.startsWith("\\|")) {
				// Remove the '\' wildcard.
				parameter = parameter.substring(1);
				logic     = "AND";					
			}
			else if (true == parameter.startsWith("|")) {
				// Remove the OR  wildcard '|'
				parameter = parameter.substring(1);
				logic     = "OR ";
			}
			else {
				logic = "AND";
			}
						
			partialQuery = " (REGEXP_LIKE (" + type + ", '" + parameter + "') OR " + 
			               "(REGEXP_LIKE (CHAINED_MSGS, '" + parameter + "'))) " + logic;				
		}
	
		return partialQuery;		
	}
	
	public void composeQuery()
	{		
		String criteria;
		criteria = "";			
		
		if (-1 == partitionID) {		    	
			return;
		}		

		criteria = "PART_ID=" + partitionID;
		
		if (userName.length() > 0) {
			criteria += " AND USER_NAME = '" + userName + "'";
		}
		// Make sure there is at least one valid (different than -1L) run number. 
		if (runNumbers.length >= 0 && runNumbers[0] != -1L) {
			long rn;
			criteria += " AND (";
			for (int i = 0; i < (runNumbers.length - 1); ++i) {
				rn = runNumbers[i];
				if (rn != -1) {
					criteria += "RUN_NUMBER = '" + rn + "' OR ";		
				}				
			}
			rn = runNumbers[runNumbers.length - 1];			
			if (rn != -1) {
				criteria += "RUN_NUMBER = '" + rn + "')";
			}		
		}

		// First the parameters that have to be ANDed		
		String partialCriteria;
		partialCriteria =  getPartialQuery(msg,         "MSG_TEXT",         msgRegExp);
		partialCriteria += getPartialQuery(host,        "MACHINE_NAME",     hostRegExp); 
		partialCriteria += getPartialQuery(qualifiers,  "QUALIFIERS",       qualifiersRegExp);
		partialCriteria += getPartialQuery(application, "APPLICATION_NAME", applicatonRegExp);
		partialCriteria += getPartialQuery(param,       "PARAM",            paramRegExp);
		partialCriteria += getPartialQuery(optParam,    "OPT_PARAM",        optParamRegExp);
		partialCriteria += getPartialQuery(msgID,       "MSG_ID",           msgIDRegExp);
		
		// Wrap the criteria with 'AND ()' to
		if (!partialCriteria.isEmpty()) {
			// Trim out the last logical operator in partialQuery. This operator is 3 character 
			// long 'AND' or 'OR '. This is very ugly mais bon.
			partialCriteria = partialCriteria.substring(0, partialCriteria.length() - 3);
			criteria += " AND (" + partialCriteria + ")";
		}
		
		// If all severities are selected, no need to add them in the query.
		if (severity.length < 4) {	
			criteria += " AND (";
			for (int i = 0; i < severity.length; ++i) {
				// The severity order is inverted, hence abs(value - 5)
				if (i < (severity.length - 1)) {
					criteria += "SEVERITY=" + java.lang.Math.abs(severity[i] - 5) + " OR ";	
				}
				else { // The last severity has to caluse the parenthesis.
					criteria += "SEVERITY=" + java.lang.Math.abs(severity[severity.length - 1] - 5) + ")";
				}
			}			
		}
		if (issuedFrom > 0) {
			criteria += " AND ISSUED_WHEN > " + issuedFrom;
		}
		if (issuedTo > 0) {
			criteria += " AND ISSUED_WHEN < " + issuedTo;
		}
		
		queryCount  = "SELECT COUNT(*) AS cpt FROM  ATLAS_LOG_MESSG.LOG_MESSAGES WHERE " + criteria;
		
		querySelect = "SELECT " + listColumns + ", total_rows FROM (SELECT " + listColumns + ", total_rows, rownum row_counter" +
	                  " FROM (SELECT " + listColumns + ", count(*) OVER () total_rows " +
	                  " FROM   ATLAS_LOG_MESSG.LOG_MESSAGES " + " WHERE " + criteria +
		              " ORDER BY ";

		if (orderBy.length() > 0) {
			querySelect += orderBy;
			if (false == orderAsc) {
				querySelect +=  " DESC ";
			}
		}	
		else {
			querySelect += " ISSUED_WHEN ASC ";
		}
		
		querySelect += ")) ";
		
		if (limitFrom > 0 || limitTo > 0) {
			querySelect += " WHERE ";
			if (limitFrom > 0) {
				querySelect += " row_counter >= " + limitFrom + " AND "; 
			}
			if (limitTo > 0) {
				querySelect += " row_counter < " + limitTo;
			}
		}
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application, boolean regExp) {
		this.application      = application;
		this.applicatonRegExp = regExp;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host, boolean regExp) {
		this.host       = host;
		this.hostRegExp = regExp;
	}

	public long getIssuedFrom() {
		return issuedFrom;
	}

	public void setIssuedFrom(long issuedFrom) {
		this.issuedFrom = issuedFrom;
	}

	public long getIssuedTo() {
		return issuedTo;
	}

	public void setIssuedTo(long issuedTo) {
		this.issuedTo = issuedTo;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg, boolean regExp) {
		this.msg       = msg;	
		this.msgRegExp = regExp;
	}

	public String getMsgID() {
		return msgID;
	}

	public void setMsgID(String msgID, boolean regExp) {
		this.msgID       = msgID;
		this.msgIDRegExp = regExp;
	}

	public String getParam() {
		return param;
	}
	
	public void setParam(String param, boolean regExp) {
		this.param       = param;
		this.paramRegExp = regExp;
	}

	public String getOptParam() {
		return param;
	}

	public void setOptParam(String optParam, boolean regExp) {
		this.optParam       = optParam;
		this.optParamRegExp = regExp;
	}
	
	public String getQualifiers() {
		return qualifiers;
	}
	
	public void setQualifiers(String qualifiers, boolean regExp) {
		this.qualifiers       = qualifiers;
		this.qualifiersRegExp = regExp;
	}	
	
	public Long getPartitionID() {
		return partitionID;
	}	
	
	public void setPartitionID(Long partitionID) {
		this.partitionID = partitionID;
	}
	
	public void setPartitionName(String partitionName) {
		this.partitionName = partitionName;
	}
	
	public String getPartitionName() {
		return partitionName;
	}		

	public long[] getRunNumbers() {
		return runNumbers;
	}

	public void setRunNumbers(long[] runNumbers) {
		this.runNumbers = runNumbers;
	}

	public int[] getSev() {
		return severity;
	}

	public void setSev(int[] severity) {
		this.severity = severity;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getLimitFrom() {
		return limitFrom;
	}

	public void setLimitFrom(long limitFrom) {
		this.limitFrom = limitFrom;
	}

	public long getLimitTo() {
		return limitTo;
	}

	public void setLimitTo(long limitTo) {
		this.limitTo = limitTo;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public boolean getOrderDir() {
		return orderAsc;
	}

	public void setOrderDir(boolean orderAsc) {
		this.orderAsc = orderAsc;
	}

	public String getQueryCount() {
		return queryCount;
	}

	public void setQueryCount(String queryCount) {
		this.queryCount = queryCount;
	}

	public String getQuerySelect() {
		return querySelect;
	}

	public void setQuerySelect(String querySelect) {
		this.querySelect = querySelect;
	}	
	
	public void setCaseInsensitiveSearch(boolean caseInsensitive) {
		this.caseInsensitive = caseInsensitive;
	}	
	
}

