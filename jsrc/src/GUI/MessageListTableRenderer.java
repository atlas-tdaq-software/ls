package GUI;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;




/**
 * Class to define the renderer of the JTree for the configuration chooser.
 * @author Vincent Boyer
 *
 */
public class MessageListTableRenderer extends DefaultTableCellRenderer 
{    
	private static final long serialVersionUID = 2L;
	
	@Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
													boolean hasFocus, int row, int column) 
	{
		Component cell = super.getTableCellRendererComponent
        (table, value, isSelected, hasFocus, row, column);
     	
     	String strValue = (String)value;
         if( strValue.equalsIgnoreCase("0"))
         {
        	 cell.setBackground(new java.awt.Color(0, 255, 127));
             cell.setForeground(new java.awt.Color(0, 0, 0));
         }
         else if( strValue.equalsIgnoreCase("1") ) 
         {
         	cell.setBackground(new java.awt.Color(0, 206, 209));
         	cell.setForeground(new java.awt.Color(255, 255, 255));
         }
         else if( strValue.equalsIgnoreCase("2")) 
         {
         	cell.setBackground(new java.awt.Color(255, 255, 0));
         	cell.setForeground(new java.awt.Color(0, 0, 0));
         }
         else if( strValue.equalsIgnoreCase("3") ) 
         {
         	cell.setBackground(new java.awt.Color(255, 140, 0));
         	cell.setForeground(new java.awt.Color(255, 255, 255));
         }
         else if( strValue.equalsIgnoreCase("4") ) 
         {
         	cell.setBackground(new java.awt.Color(255, 69, 0));
         	cell.setForeground(new java.awt.Color(255, 255, 255));
         }
         else if( strValue.equalsIgnoreCase("5") ) 
         {
         	cell.setBackground(new java.awt.Color(206, 0, 0));
         	cell.setForeground(new java.awt.Color(255, 255, 255));
         }
    
     return cell;
 }
	
	
   
    

}