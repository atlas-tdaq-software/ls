package GUI;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.util.Scanner;
import java.util.Enumeration;
import java.util.Arrays;

import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import GUI.InfoReporting;


/**
 * this panel contains the tree of the partitions, users, sessions
 * it deals with the mouse listeners
 */

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class TableTreeJPanel extends javax.swing.JPanel 
{	
	private static final long serialVersionUID = -1457760718441117366L;
	// GUI components
	private JScrollPane jScrollPaneTableTree;
	private TableTree tableTree;
	
	// To store the selection listener of this class
	protected EventListenerList selectionListenerList;
	
	// To store the display listener of this class
	protected EventListenerList displayListenerList;
	
	private JFrameMain jFrameMain;
	private JPopupMenu popUpMenu;
	private JMenuItem  refreshItem;
	private JMenuItem  retrieveItem;
	private String     lastPartitonName;
	private Long       lastPartitionID;  
	private String     lastUserName;
	private long       lastRunNumber;  	

	private boolean goingToTableTreeObject;	
	
	/**
	 * Constructor.
	 *
	 */	
	public TableTreeJPanel(JFrameMain frameMain) {
		super();
		
		goingToTableTreeObject = false;
		jFrameMain = frameMain;
		this.selectionListenerList = new EventListenerList();
		this.displayListenerList = new EventListenerList();		
		
		lastPartitonName = "";
		lastPartitionID  = -1L;  
		lastUserName     = "";
		lastRunNumber    = -1L;
		
		try {
			this.setLayout(new BorderLayout());
	//		this.setFont(jFrameMain.defaultFont);
			{
				tableTree = new TableTree(jFrameMain);
				jScrollPaneTableTree = new JScrollPane();
				jScrollPaneTableTree.setViewportView(tableTree) ;
				jScrollPaneTableTree.setBorder(	new javax.swing.border.CompoundBorder(
												new javax.swing.border.CompoundBorder(
												new javax.swing.border.LineBorder(Color.gray),
												new javax.swing.border.EtchedBorder(javax.swing.border.EtchedBorder.RAISED, Color.white, Color.lightGray)
												),
												new javax.swing.border.LineBorder(Color.darkGray)));
							
				// Fill in the table
				this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				tableTree.filljTreeTable();
				this.setCursor(Cursor.getDefaultCursor());
				
				popUpMenu    = new JPopupMenu();
				refreshItem  = new JMenuItem("Refresh");	
				retrieveItem = new JMenuItem("Get msgs for");
				popUpMenu.add(refreshItem);
				popUpMenu.add(retrieveItem);
								
				refreshItem.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseReleased(MouseEvent evt) {
						if (tableTree.getRefreshingPartitionList() == true) {
							InfoReporting.printWarning("This action is not allowed whilst the Partition list is being refreshed.");
						}
						else {
							tableTree.filljTreeTable();
						}
					}					
				});	
				
				retrieveItem.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseReleased(MouseEvent evt) {
						if (tableTree.getRefreshingPartitionList() == true) {
							InfoReporting.printWarning("This action is not allowed whilst the Partition list is being refreshed.");
						}
						else {
							long[] runNumbers = new long[1];
							runNumbers[0] = lastRunNumber;
							displayMessages(lastPartitionID, lastPartitonName, lastUserName, runNumbers);							
						}
					}					
				});	
				
				tableTree.addMouseListener(new MouseAdapter() {
					@Override
                    public void mousePressed(MouseEvent evt) {	
						if (tableTree.getRefreshingPartitionList() == true) {
							InfoReporting.printWarning("This action is not allowed whilst the Partition list is being refreshed.");
							return;
						}							
						if (evt.getClickCount() == 1) {
							if(SwingUtilities.isLeftMouseButton(evt)) {
								jTreeLeftMousePressed(evt);	
							}
							else {
								jTreeRightMousePressed(evt);
							}
						}
					}
				});
			}
			this.add(jScrollPaneTableTree, BorderLayout.CENTER );
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}	
	
	public void goToObject(String partition, String user, Integer runNumber)
	{			
		goingToTableTreeObject = true;
		
		DefaultMutableTreeNode root          = (DefaultMutableTreeNode)tableTree.getModel().getRoot();
		DefaultMutableTreeNode partitionNode = null;
		DefaultMutableTreeNode userNode      = null;
		DefaultMutableTreeNode rnNode        = null;
		
		if (false == partition.isEmpty()) {
			partitionNode = tableTree.searchNode(partition, root, false);
			if (null != partitionNode) {
				tableTree.collapseAllNodes();	
				TreePath path = new TreePath( ((DefaultTreeModel)tableTree.getModel()).getPathToRoot(partitionNode) );
				int row = tableTree.getRowForPath(path);
				tableTree.expandRow(row);
				row += 20;
				if (row > tableTree.getRowCount()) {
					row = tableTree.getRowCount();
				}					
				tableTree.scrollRowToVisible(row);				
			}
			else {
				InfoReporting.printError("Partition '" + partition + "' not found.");
				goingToTableTreeObject = false;
				return;				
			}
		}
		
		if (false == user.isEmpty()){			
			userNode = tableTree.searchNode(user, partitionNode, false);
			if (null != userNode) {
				TreePath path = new TreePath( ((DefaultTreeModel)tableTree.getModel()).getPathToRoot(userNode) );
				int row = tableTree.getRowForPath(path);
				tableTree.expandRow(row);	
				row += 20;
				if (row > tableTree.getRowCount()) {
					row = tableTree.getRowCount();
				}				
				tableTree.scrollRowToVisible(row);
			}
			else {
				InfoReporting.printError("User '" + user + "' not found.");
				goingToTableTreeObject = false;
				return;				
			}
		}		
		
		if (-1 != runNumber) {
			tableTree.collapseAllNodes();	
			if (null != userNode) {
				rnNode = tableTree.searchNode(runNumber.toString(), userNode, true);
			}
			else if (null != partitionNode) {
				Enumeration users = partitionNode.children(); 				
				while (users.hasMoreElements())	{ 				
					userNode = (DefaultMutableTreeNode)users.nextElement();
					rnNode = tableTree.searchNode(runNumber.toString(), userNode, true);
					if (null != rnNode) {	
						break;
					}
				}
			}
			else {
				Enumeration partitions = root.children(); 	
				boolean found = false;
				while (partitions.hasMoreElements() && false == found)	{ 				
					partitionNode = (DefaultMutableTreeNode)partitions.nextElement();
					Enumeration users = partitionNode.children(); 
					while (users.hasMoreElements() && false == found)	{ 				
						userNode = (DefaultMutableTreeNode)users.nextElement();
						rnNode = tableTree.searchNode(runNumber.toString(), userNode, true);
						if (null != rnNode) {
						 	found = true;
						}
					}
				}				
			}
			
			if (null != rnNode) {
				// Expand partition. Needed to get the row of the runNumber.
				{
					TreePath tmpTree = new TreePath( ((DefaultTreeModel)tableTree.getModel()).getPathToRoot(partitionNode));
					tableTree.expandPath(tmpTree);	
				}
				// Expand user. Needed to get the row of the runNumber.
				{
					TreePath tmpTree = new TreePath( ((DefaultTreeModel)tableTree.getModel()).getPathToRoot(userNode));
					tableTree.expandPath(tmpTree);	
				}				
				
				TreePath path = new TreePath(((DefaultTreeModel)tableTree.getModel()).getPathToRoot(rnNode));

				int row = tableTree.getRowForPath(path);
				InfoReporting.printError("row '" + row); 
				tableTree.expandRow(row);	
				row += 20;
				if (row > tableTree.getRowCount()) {
					row = tableTree.getRowCount();
				}							
				tableTree.scrollRowToVisible(row);							
								
				String treePathSelected = path.toString();					
				// Retrieve the partition name, user and session.
				String tmp = "";
				// The String.replace() method does not work, dunno why. Use the classical style. 
				for (int i = 0; i < treePathSelected.length(); i ++) {
					if (treePathSelected.charAt(i) != '[' && treePathSelected.charAt(i) != ']')				
					{
						tmp += treePathSelected.charAt(i);
					}
				}
				
				String[] treePathSplit = tmp.split(",");		
				Long partitionID       = -1L;
				String partitionName   = "";
				String userName        = "";
				long[] runnumber       = new long[] { -1L };
				
				switch (treePathSplit.length)
				{
					case 4: // Partition, user and run number selected
						// Format of the runnumber node: 28466  (29.07.2008 16:10:30)
						//    the actual run number is the first integer.
						Scanner scan = new Scanner(treePathSplit[3].trim());
						runnumber[0] = scan.nextInt();
						//runnumber = Integer.decode().longValue();
					case 3: // Partition and user selected
						userName = treePathSplit[2].trim();
					case 2: // Partition selected
						partitionName = treePathSplit[1].trim();
						partitionID   = tableTree.getPartitionMap().get(partitionName);				
						break;
					case 1: // Top node selected ("Log Msgs"), do nothing
						break;
					default:
						break;
				}
				// Get table if a node is selected.
				jFrameMain.getSearchJPanel().getSearchjButton().setVisible(true);
				jFrameMain.getSearchTree().update(partitionName, userName, runnumber[0]);
				jFrameMain.getMessageListTableJPanel().getListMessageTable().fillListTable(partitionID, partitionName, userName, runnumber);
			}
			else {
				InfoReporting.printError("Run Number '" + runNumber + "' not found.");
			}
		}
		goingToTableTreeObject = false;
	}
	
	void displayMessages(Long partitionID, String partitionName, String userName, long[] runNumbers) 
	{
		// Get table if a node is selected.
		jFrameMain.getSearchJPanel().getSearchjButton().setVisible(true);
		jScrollPaneTableTree.setPreferredSize(this.tableTree.getSize());
		jFrameMain.getSearchTree().update(partitionName, userName, runNumbers[0]);
		jFrameMain.getMessageListTableJPanel().getListMessageTable().fillListTable(partitionID, partitionName, userName, runNumbers);		
	}
	
	/**
	 * Set the Table Tree
	 */
	public void setTableTree(TableTree tree) { tableTree = tree; }
	
	/**
	 * Get the Table Tree
	 */
	public TableTree getTableTree() { return tableTree; }
	
	/**
	 * Set DefaultTreeModel to the configuration chooser.
	 * 
	 * @param defaultTreeModel
	 * 			is the DefaultTreeModel to set.
	 */
	public void setDefaultTreeModel(DefaultTreeModel defaultTreeModel) 
	{		
		if(defaultTreeModel != null)
			
			// Set the model tree
			tableTree.setModel(defaultTreeModel);
		
			// Display the tree from the first node 
			tableTree.setSelectionRow(0);
			tableTree.scrollPathToVisible(tableTree.getSelectionPath());
	}
	
	/**
	 * Get DefaultTreeModel to the configuration chooser.
	 * 
	 * @return the DefaultTreeModel of the tree.
	 */
	public DefaultTreeModel getDefaultTreeModel() {		
		return (DefaultTreeModel)tableTree.getModel();
	}
	
	/**
	 * Return the selected Item.
	 * 
	 * @return the selected Item object
	 */
	public String getSelectedItem() {
		return tableTree.getSelectedItem();		
	}
	
	/**
	 * Return the selected Items.
	 * 
	 * @return the selected Items object
	 */
	public String[] getSelectedItems() {
		return tableTree.getSelectedItems();		
	}
	
	/**
	 * Actions to do when a node of a JTree is selected.
	 */
	private void jTreeLeftMousePressed(MouseEvent evt)
	{
		TreePath[] paths = tableTree.getSelectionPaths();	
		TreePath   path  = paths[0];
		
		if (4 == path.getPathCount()) 
		{			
			Long     partitionID   = -1L;
			String   partitionName = "";
			String   userName      = "";
			long[]   runNumbers    = new long[tableTree.getSelectionCount()];

			// Fill in wiht -1L
			Arrays.fill(runNumbers, -1L);	
			
			partitionName = path.getPathComponent(1).toString();
			partitionID   = tableTree.getPartitionMap().get(partitionName);				
			userName      = path.getPathComponent(2).toString();
			Scanner scan  = new Scanner(path.getPathComponent(3).toString());
			runNumbers[0] = scan.nextInt();

			// Iterate the remaining paths selected.
			for (int i = 1; i < tableTree.getSelectionCount() ; ++i) {
				path = paths[i];
				if (partitionName == path.getPathComponent(1).toString() &&
					userName      == path.getPathComponent(2).toString() &&
					runNumbers[0] != -1L)
				{
					scan          = new Scanner(path.getPathComponent(3).toString());
					runNumbers[i] = scan.nextInt();
				}
				else {
					InfoReporting.printWarning(path.toString() + " ignored.");	
				}			
			}
			
			displayMessages(partitionID, partitionName, userName, runNumbers);			
		}		
	}
	
	private void jTreeRightMousePressed(MouseEvent evt)
	{	
		TreePath path = tableTree.getPathForLocation(evt.getX(), evt.getY());
		
		if (null == path) {
			return;
		}
		String  label = "Get msgs";
			
		switch (path.getPathCount()) 
		{
		   case 1: // The root node was selected. Return.
			   retrieveItem.setEnabled(false);		  
			  break;		
		   case 2: // Partition selected
			   lastPartitonName = path.getPathComponent(1).toString();
			   lastPartitionID  = tableTree.getPartitionMap().get(lastPartitonName);
			   lastUserName     = "";
			   lastRunNumber    = -1L;
			   retrieveItem.setEnabled(true);
			   break;
		   case 3: // Partition and user selected
			   lastPartitonName = path.getPathComponent(1).toString();
			   lastPartitionID  = tableTree.getPartitionMap().get(lastPartitonName);
			   lastUserName     = path.getPathComponent(2).toString();
			   lastRunNumber    = -1L;
			   retrieveItem.setEnabled(true);			   
			   break;			  
		   case 4: // Partition, user and run number selected
			   lastPartitonName = path.getPathComponent(1).toString();
			   lastPartitionID  = tableTree.getPartitionMap().get(lastPartitonName);
			   lastUserName     = path.getPathComponent(2).toString();
			   Scanner scan = new Scanner(path.getPathComponent(3).toString());
			   lastRunNumber = scan.nextInt();
			   retrieveItem.setEnabled(true);				
			   break;
		   default:
			  break;
		}
		if (false == lastPartitonName.equals("")) {
			label += " for " + lastPartitonName;
			if (false == lastUserName.equals("")) {
				label += ":" + lastUserName;
				if (-1 != lastRunNumber) {
				   label += ":" + lastRunNumber;
				}
			}						
		}
		retrieveItem.setText(label);
				
		popUpMenu.show(evt.getComponent(), evt.getX(), evt.getY());				
	}	
	
	
	/**
	* Enables or disables this component, depending on the value of the parameter b. An enabled component can respond to user input and generate events. 
    * Components are enabled initially by default.
	* 
	* @param b
	* 		If true, this component is enabled, otherwise this component is disabled.
	* 
	*/
	@Override
    public void setEnabled(boolean b) {		
		tableTree.setEnabled(b);		
	}


	/**
	 * Return the scroll pane.
	 * 
	 * @return the scroll pane
	 */
	public JScrollPane getJScrollPane() {
		return jScrollPaneTableTree;
		
	}	
}
