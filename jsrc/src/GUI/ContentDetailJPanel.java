package GUI;

/**
 * this class contains all the element of the detail of a message
 * it displays them so they are more readable
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class ContentDetailJPanel extends javax.swing.JPanel {

	private static final long serialVersionUID = -6700182758817327174L;
	private JTextPane MessageContentjTextPane;
	private JScrollPane jScrollMessageContent;    
	
	private Style styleBoldTxt;
	private Style styleNormalTxt;	
	private StyledDocument doc;
	
	JPopupMenu clearMenu; 
	
	public static final String mainStyleName = "MainStyle";	
	
	public ContentDetailJPanel() {
		super();

		try {
			this.setLayout(new BorderLayout());
			{
				MessageContentjTextPane = new JTextPane();
				MessageContentjTextPane.setEditable(false);
				MessageContentjTextPane.addMouseListener(new MouseAdapter() {
					@Override
                    public void mousePressed(MouseEvent evt) {
						if(SwingUtilities.isRightMouseButton(evt)) {
							clearMenu.show(evt.getComponent(), evt.getX(), evt.getY());			
						}
					}
				});
				clearMenu = new JPopupMenu();
				JMenuItem clearItem = new JMenuItem("Clear");	
				clearMenu.add(clearItem);
				clearItem.addMouseListener(new MouseAdapter() {
					@Override
                    public void mouseReleased(MouseEvent evt) {
						try {
							doc.remove(0, doc.getLength());
						}
						catch(BadLocationException e) {	
						}
					}					
				});				
								
				jScrollMessageContent = new JScrollPane();
				jScrollMessageContent.setViewportView(MessageContentjTextPane) ;
				jScrollMessageContent.setBorder(new javax.swing.border.CompoundBorder(
												new javax.swing.border.CompoundBorder(
												new javax.swing.border.LineBorder(Color.gray),
												new javax.swing.border.EtchedBorder(javax.swing.border.EtchedBorder.RAISED, Color.white, Color.lightGray)
												),
												new javax.swing.border.LineBorder(Color.darkGray)));
				
				doc = (StyledDocument)MessageContentjTextPane.getDocument();
				
				styleNormalTxt = doc.addStyle("normalTxt", null);
				//StyleConstants.setForeground(styleNormalTxt, new java.awt.Color(0, 157, 0));
				
				styleBoldTxt = doc.addStyle("boldTxt", null);
				StyleConstants.setBold(styleBoldTxt, true);
				
			}
			this.add(jScrollMessageContent, BorderLayout.CENTER );						
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	/**
	 * Function called when the user clicks on a line of the JTable it
	 * displays all the information of a message given in the text zones
	 * @param msg
	 */
	public void fillContentDetails (MessageListTable listTable, int row)
	{		
		DefaultTableModel model = listTable.getMessageListTableModel();

		try {
			doc.remove(0, doc.getLength());
			doc.insertString(0, "Host: ", styleBoldTxt);
			doc.insertString(doc.getLength(), (String)model.getValueAt(row, 1) + "\n", styleNormalTxt);
			doc.insertString(doc.getLength(), "Application Name: ", styleBoldTxt);
			doc.insertString(doc.getLength(), (String)model.getValueAt(row, 2) + "\n", styleNormalTxt);
			doc.insertString(doc.getLength(), "Issued: ", styleBoldTxt);
			doc.insertString(doc.getLength(), (String)model.getValueAt(row, 3) + "\n", styleNormalTxt);
			doc.insertString(doc.getLength(), "Severity: ", styleBoldTxt);
			doc.insertString(doc.getLength(), (String)model.getValueAt(row, 4) + "\n", styleNormalTxt);
			doc.insertString(doc.getLength(), "Message ID: ", styleBoldTxt);
			doc.insertString(doc.getLength(), (String)model.getValueAt(row, 5) + "\n", styleNormalTxt);
			doc.insertString(doc.getLength(), "Message: ", styleBoldTxt);
			doc.insertString(doc.getLength(), (String)model.getValueAt(row, 6) + "\n", styleNormalTxt);
			doc.insertString(doc.getLength(), "Context: ", styleBoldTxt);
			doc.insertString(doc.getLength(), (String)model.getValueAt(row, 7) + "\n", styleNormalTxt);
			doc.insertString(doc.getLength(), "Parameters: ", styleBoldTxt);
			doc.insertString(doc.getLength(), (String)model.getValueAt(row, 8) + "\n", styleNormalTxt);			
			doc.insertString(doc.getLength(), "Qualifiers: ", styleBoldTxt);
			doc.insertString(doc.getLength(), (String)model.getValueAt(row, 9) + "\n", styleNormalTxt);			
		}
		
		catch(BadLocationException e) {	
		}
	}
	
	
	/**
	 * Return the scroll pane.
	 * 
	 * @return the scroll pane
	 */
	public JScrollPane getJScrollPane() {
		return jScrollMessageContent;
		
	}		

	public JTextPane getMessageContentjTextPane() {
		return MessageContentjTextPane;
	}


	public void setMessageContentjTextPane(JTextPane messageContentjTextPane) {
		MessageContentjTextPane = messageContentjTextPane;
	}	
}
